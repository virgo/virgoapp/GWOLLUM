stages:
  - build
  - docs
  - deploy

include:
  # https://computing.docs.ligo.org/gitlab-ci-templates/
  - project: computing/gitlab-ci-templates
    file:
      # https://computing.docs.ligo.org/gitlab-ci-templates/conda/
      - conda.yml

# cmake build template
.build:
  stage: build
  needs: []
  script:
    # build out-of-tree
    - rm -rf _build
    - mkdir -p _build
    - cd _build
    # configure
    # NOTE: jobs that extend this template can customise the cmake
    #       execution using the CMAKE_ARGS environment variable,
    #       using `xargs -t ... <<< ${CMAKE_ARGS}` allows CMAKE_ARGS
    #       to have arbitrary quotes, spaces, etc, and bash still handle
    #       it properly.
    - xargs -t cmake ${CI_PROJECT_DIR}
          -DCMAKE_INSTALL_PREFIX:PATH=$(pwd)/install
          -DCMAKE_VERBOSE_MAKEFILE:BOOL=yes
          <<< ${CMAKE_ARGS}
    # build
    - cmake --build . --parallel 4 --verbose
    # test
    - ctest --parallel 4 --verbose
    # install
    - cmake --build . --parallel 4 --verbose --target install

# build with conda
.build:conda:
  extends:
    # extend the .conda:base template to get default conda initialisation
    # and package caching, see
    # https://computing.docs.ligo.org/gitlab-ci-templates/conda/#.conda:base
    - .conda:base
    # extend .build to execute the cmake build as the `script`
    - .build
  before_script:
    # run the before_script section of the .conda:base template
    - !reference [".conda:base", before_script]
    # then prepare a conda environment in which to build
    - conda create
          --name build
          --strict-channel-priority
          --quiet
          `# build tools`
          cxx-compiler
          "cmake>=3.12.0"
          make
          pkg-config
          `# required libraries`
          fftw
          hdf5
          libframel
          root_base
    # print the packages in the environment
    - conda list --name build
    # activate the environment
    - conda activate build
    # NOTE: we get a populated CMAKE_ARGS variable from the conda-forge
    #       compiler feedstock, see
    #       https://github.com/conda-forge/ctng-compiler-activation-feedstock/blob/master/recipe/activate-gcc.sh

build:conda:linux:
  extends:
    - .build:conda

#build:conda:macos:
#  extends:
#    - .build:conda
#  tags:
#    - macos

docs:
  image: ubuntu
  stage: docs
  needs: []
  before_script:
  - apt-get update
  - apt-get upgrade -y
  - apt-get install --yes doxygen
  - apt-get install --yes graphviz
  - apt-get install --yes build-essential
  script:
  - bash ./doc/rundox.sh
  artifacts:
    paths:
    - doc/html

pages:
  stage: deploy
  needs:
  - docs
  script:
  - mv doc/html/ public/
  artifacts:
    paths:
    - public
  rules:
  - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
