/**
 * @file 
 * @brief Frame file access.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __ffl__
#define __ffl__

#include "Monitor.h"
#include "GwollumPlot.h"
#include "Segments.h"
#include <FrameL.h>
#include <FrVect.h>

using namespace std;

/**
 * @brief Load frame files and access data in frames.
 * @details This class is designed to load frame files and provide methods to access the data.
 * The native format for file registration is the so-called FFL format.
 * A FFL file is a text file where each line identifies a frame file.
 * Each line must contained exactly 5 columns:
 * - path to a frame file
 * - starting GPS time of the frame file
 * - duration of the frame file [s]
 * - unused column [0]
 * - unused column [0]
 *
 * @note The FFL format is the native format for this class. However the lalcache format (LCF) is also supported. In that case the LCF file is first converted to a FFL file before processing.
 *
 * This class is using the <a href="http://lappweb.in2p3.fr/virgo/FrameL/">Frame Library</a> functions.
 *
 * @author Florent Robinet
 */
class ffl{

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the ffl class.
   * @details The frame file list is provided.
   * Two formats are supported: the native Frame File List (FFL) and the lalcache format (LCF). For both cases, it is possible to list another file (LCF or FFL) with a line with the following format: file path - GPS start - duration.
   *
   * When working with LCF files or when working with secondary files, a working FFL file will be created. In that case, a tmp directory must be provided.
   *
   * @note It is possible to perform several attemps when accessing the frame file data. You can configure the number of attemps and the time distance between attempts.
   *
   * @param[in] aFrameFileList Path to frame file list
   * @param[in] aPlotStyle Plotting style
   * @param[in] aVerbosity Verbosity level
   * @param[in] aTmpDirPath Path to temporary directory. If needed, this path is used to build a working FFL file.
   * @param[in] aNTrials Number of trials to access the data.
   * @param[in] aNSleepSeconds Number of seconds between trials.
   */
  ffl(const string aFrameFileList,
      const string aPlotStyle = "GWOLLUM",
      const unsigned int aVerbosity = 0,
      const string aTmpDirPath = ".",
      const unsigned int aNTrials = 1,
      const unsigned int aNSleepSeconds = 0);

  /**
   * @brief Destructor of the ffl class.
   */
  virtual ~ffl(void);
  /**
     @}
  */

  /**
   * @brief Returns the class status.
   */
  inline bool GetStatus(void){ return mon->GetStatus(); };

  /**
   * @brief Sets a new name for the plotting structure.
   * @param[in] aName Plot name.
   */
  void SetPlotName(const string aName);

  /**
   * @brief Returns the path to the input frame file list.
   */
  inline string GetInputFfl(void){ return fInFrameFile; };

  /**
   * @brief Extracts channels at a given GPS time.
   * @details Only ADC, PROC, SIM and SER channels are extracted.
   * Only channels with a sampling rate above or equal to 1Hz are extracted.
   * @note The data is retrieved several times, as configured by the number of trials.
   *
   * @param[in] aGps GPS time where to scan the channel list.
   * @returns true if the channel list was correctly loaded, false otherwise.
   */
  bool ExtractChannels(const unsigned int aGps);

  /**
   * @brief Returns a pointer to the segments covered by the ffl.
   * @warning DO NOT DELETE OR MODIFY! This object is internally used by the class.
   */
  inline Segments* GetSegments(void){ return seg; }

  /**
   * @brief Returns the sampling rate of a given channel [Hz].
   * @returns 0 if the channel is not found.
   * @note To target a given GPS time, use ExtractChannels() first.
   * @param[in] aChannelName Channel name.
   * @param[out] aChannelIndex Channel index.
   */
  inline unsigned int GetChannelSampling(const string aChannelName, unsigned int &aChannelIndex){
    for(aChannelIndex=0; aChannelIndex<channels.size(); aChannelIndex++){
      if(!aChannelName.compare(channels[aChannelIndex]))
        return sampling[aChannelIndex];
    }
    return 0;
  };

  /**
   * @brief Returns the sampling rate of a given channel [Hz].
   * @returns 0 if the channel is not found.
   * @note To target a given GPS time, use ExtractChannels() first.
   * @param[in] aChannelName Channel name.
   */
  inline unsigned int GetChannelSampling(const string aChannelName){
    unsigned int dummy;
    return GetChannelSampling(aChannelName, dummy);
  };

  /**
   * @brief Prints the list of channels at a given GPS time.
   * @details Two columns are printed: the channel name and the sampling frequency [Hz].
   * @param[in] aGps GPS time. Use 0 to print the channels in the first frame file.  
   */
  inline bool PrintChannels(const unsigned int aGps=0){
    bool res = ExtractChannels(aGps);
    for(unsigned int l=0; l<channels.size(); l++)
      cout<<channels[l]<<" "<<sampling[l]<<endl;
    return res;
  };

  /**
   * @brief Returns the current channel list.
   * @details It is possible to return a subset of channels using a filter.
   * This filter is a string containing wildcards, e.g. "V1:DQ_*_FLAG_* V1:ENV_*".
   * @param[in] aFilter Channel name filter. Use "" for no filter.
   */  
  vector <string> GetChannelList(const string aFilter="");

  /**
   * @brief Tests whether a channel exists in the dataset pointed by the ffl.
   * @returns true if the channel exists, false otherwise.
   * @warning Make sure that the channel list was extracted at the correct GPS time. See ExtractChannels().
   * @param[in] aChannelName Channel name to test.
   */
  inline bool IsChannel(const string aChannelName){
    for(unsigned int l=0; l<channels.size(); l++){
      if(!aChannelName.compare(channels[l])) return true;
    }
    return false;
  };

 /**
   * @brief Returns a data vector for a given channel and a GPS range.
   * @details A pointer to the data vector is returned.
   * The size of the vector is returned.
   * If the data cannot be read, a pointer to NULL is returned and the data size is set to 0.
   * @note The data access is tried several times, as configured with the constructor.
   *
   * If there are missing data in the requested time stretch, no data vector is returned by default (`aMissing=999.0`). For any other value for `aMissing`, the data vector is returned and the missing sample are given the value `aMissing`.
   *
   * @note The user is in charge of deleting the returned vector.
   * @param[out] aSize Number of samples in the returned vector.
   * @param[in] aChannelName Channel name.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   * @param[in] aMissing Special value to flag missing samples. If =999.0, no flagging, the function returns NULL.
   */
  double* GetData(unsigned int &aSize, const string aChannelName,
                  const double aGpsStart, const double aGpsEnd,
                  const double aMissing=999.0);

  /**
   * @brief Prints a data vector in a text file.
   * @details This function prints the time series of a given channel between two GPS times in an text file. 
   * @param[in] aChannelName Channel name.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   * @param aFileName Output text file name.
   */
  bool PrintData(const string aChannelName, const double aGpsStart, const double aGpsEnd, string aFileName);

  /**
   * @brief Plots a data vector time series.
   * @details This function, when run interactively, plots the time series of a given channel between two GPS times.
   * @returns A pointer to the resulting graph is returned. Do not modify or delete this object as it is internally used by the class. NULL is returned if this function fails.
   * @param[in] aChannelName Channel name.
   * @param[in] aGpsStart Starting GPS time.
   * @param[in] aGpsEnd Ending GPS time.
   */
  TGraph* PlotData(const string aChannelName, const double aGpsStart, const double aGpsEnd);
  
 private:

  Monitor *mon;            ///< Class status.
  string fInFrameFile;     ///< Path to input frame file list.
  string fWoFrameFile;     ///< Path to working frame file (always FFL).
  string srandint;         ///< Random integer string.
  unsigned int ntrials;    ///< Number of trials when accessing the data.
  unsigned int nsleepsec;  ///< Number of seconds between trials.

  // DATA
  Segments *seg;           ///< Segments covered by the ffl file.
  FrFile *frfile;          ///< Fr file.

  // CHANNELS
  vector <string> channels;///< List of channels.
  vector <unsigned int> sampling;///< List of sampling frequencies.

  // PLOTS
  GwollumPlot *GP;         ///< Plotting structure.
  TGraph *G;               ///< Graph.

  // FRAME FILES
  vector <string> ffl_framefile; ///< Frame file path.
  vector <unsigned int> ffl_gps; ///< Frame file GPS start.
  vector <unsigned int> ffl_dur; ///< Frame file duration.
  bool ffl_pure;                 ///< Flags a pure FFL file --> no need for a working file.

  /**
   * @brief Reads an input file and extracts the list of frame files.
   * @param[in] aInFile Path to the input file.
   */
  void ReadInputFile(const string aInFile);

  ClassDef(ffl,0)  

};

#endif


