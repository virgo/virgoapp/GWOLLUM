# -- Streams library ---------

# list of headers
set(
  GWOLLUM_STREAMS_HEADERS
  DetConst.h
  ffl.h
  Sample.h
  Streams.h
  Spectrum.h
  )

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/../Time
  ${CMAKE_CURRENT_SOURCE_DIR}/../Utils
  ${CMAKE_CURRENT_SOURCE_DIR}/../Segments
  ${CMAKE_CURRENT_SOURCE_DIR}/../Streams
  ${FRAMEL_INCLUDE_DIRS}
  )

# build ROOT dictionary
root_generate_dictionary(
  StreamsDict
  LINKDEF LinkDef.h
  MODULE Streams
  OPTIONS ${GWOLLUM_STREAMS_HEADERS} -I${FFTW_INCLUDE_DIRS}
  )

# compile library
add_library(
  Streams
  SHARED
  ffl.cc
  DetUtils.cc
  Sample.cc
  Streams.cc
  Spectrum.cc
  StreamsDict.cxx
  )
target_include_directories(
  Streams
  PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  )
target_link_libraries(
  Streams
  Time
  RootUtils
  Segments
  ${FRAMEL_LIBRARIES}
  )
set_target_properties(
  Streams PROPERTIES
  PUBLIC_HEADER "${GWOLLUM_STREAMS_HEADERS}"
  )

# -- executables ------------

# gwl-print-channels
add_executable(
  gwl-print-channels
  gwl-print-channels.cc
  )
target_link_libraries(
  gwl-print-channels
  ${ROOT_Core_LIBRARY}
  Streams
  )
target_include_directories(
  gwl-print-channels
  PRIVATE
  ${FRAMEL_INCLUDE_DIRS}
  )

# gwl-print-psd
add_executable(
  gwl-print-psd
  gwl-print-psd.cc
  )
target_link_libraries(
  gwl-print-psd
  ${ROOT_Core_LIBRARY}
  ${ROOT_RIO_LIBRARY}
  Streams
  )
target_include_directories(
  gwl-print-psd
  PRIVATE
  ${FRAMEL_INCLUDE_DIRS}
  )

# gwl-print-detector-response
add_executable(
  gwl-print-detector-response
  gwl-print-detector-response.cc
  )
target_include_directories(
  gwl-print-detector-response
  PRIVATE
  ${CMAKE_CURRENT_SOURCE_DIR}/../Time
  ${CMAKE_CURRENT_SOURCE_DIR}/../Utils
  )
target_link_libraries(
  gwl-print-detector-response
  Streams
  )

# -- tests -----------
add_executable(
  gwl-test-streams
  gwl-test-streams.cc
  )
target_link_libraries(
  gwl-test-streams
  Streams
  )
add_test(
  NAME gwl-test-streams
  COMMAND gwl-test-streams
  )

add_executable(
  gwl-test-ffl
  gwl-test-ffl.cc
  )
target_link_libraries(
  gwl-test-ffl
  Streams
  )
add_test(
  NAME gwl-test-ffl
  COMMAND gwl-test-ffl ${CMAKE_CURRENT_SOURCE_DIR}/../../data
  )

# -- installation -----------

# install library
install(
  TARGETS
  Streams
  RUNTIME DESTINATION ${RUNTIME_DESTINATION}
  LIBRARY DESTINATION ${LIBRARY_DESTINATION}
  ARCHIVE DESTINATION ${LIBRARY_DESTINATION}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )

# install ROOT PCM
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/libStreams_rdict.pcm
  ${CMAKE_CURRENT_BINARY_DIR}/libStreams.rootmap
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  )

# install executable(s)
install(
  TARGETS
  gwl-print-channels
  gwl-print-psd
  gwl-print-detector-response
  DESTINATION ${CMAKE_INSTALL_BINDIR}
  )

