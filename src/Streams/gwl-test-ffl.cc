/**
 * @file 
 * @brief Program to test the ffl class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ffl.h"

using namespace std;

/**
 * @brief Test program.
 */
int main (int argc, char* argv[]){

  // test directory
  error_code ec;
  if(!CreateDirectory(ec, "./gwl-test-ffl-dir")){
    cerr<<"Cannot create test directory ("<<ec<<")"<<endl;
    return 1;
  }
  
  // getdata
  char *c_gwf = getenv("GWOLLUM_DATA");
  string gwf;
  if(argc==2) gwf = (string)argv[1];
  else if(c_gwf==NULL) gwf=".";
  else gwf = (string)c_gwf;

  vector<string> chanlist;
  double *data;
  unsigned dsize;
  ffl *F;
  ofstream of;
  
  // generatea PURE FFL file
  of.open("./gwl-test-ffl-dir/gwl-test-ffl.ffl");
  if(of.fail()) return 1;
  of<<gwf+"/H-H1_llhoft-1162473868-4.gwf"<<" 1162473868 4  0 0"<<endl;// seg[2]
  of<<"/it/does/not/matter/frame1.gwf  1200000000 100 0 0"<<endl;// seg[3]
  of<<"/it/does/not/matter/frame2.gwf  1200000300 50 0 0"<<endl;// seg[4] (II)
  of<<"/it/does/not/matter/frame3.gwf  1200000200 100 0 0"<<endl;// seg[4] (I)
  of<<gwf+"/V1-hrec-vsr2.gwf"<<" 941380000 10  0 0"<<endl; // seg[0]
  of<<gwf+"/H-H1_llhoft-1162473860-4.gwf"<<" 1162473860 4  0 0"<<endl;// seg[1]
  of.close();
  
  //*********************************************************************
  // ffl() [PURE FFL]
  //*********************************************************************
  cout<<"\nffl() [PURE FFL]"<<endl;
  F = new ffl("./gwl-test-ffl-dir/gwl-test-ffl.ffl", "GWOLLUM", 0, "./gwl-test-ffl-dir/", 1, 0);
  cout<<"\tGetStatus()"<<endl;
  if(F->GetStatus()==false) return 2;
  cout<<"\tGetInputFfl()"<<endl;
  if(F->GetInputFfl().compare("./gwl-test-ffl-dir/gwl-test-ffl.ffl")) return 2;
  cout<<"\tGetSegments()"<<endl;
  if(F->GetSegments()->GetLiveTime()!=268.0) return 2;
  if(F->GetSegments()->GetN()!=5) return 2;
  if(F->GetSegments()->GetStart(0)!=941380000.0) return 2;
  if(F->GetSegments()->GetStart(1)!=1162473860.0) return 2;
  if(F->GetSegments()->GetStart(2)!=1162473868.0) return 2;
  if(F->GetSegments()->GetStart(3)!=1200000000.0) return 2;
  if(F->GetSegments()->GetStart(4)!=1200000200.0) return 2;
  if(F->GetSegments()->GetEnd(0)!=941380010.0) return 2;
  if(F->GetSegments()->GetEnd(1)!=1162473864.0) return 2;
  if(F->GetSegments()->GetEnd(2)!=1162473872.0) return 2;
  if(F->GetSegments()->GetEnd(3)!=1200000100.0) return 2;
  if(F->GetSegments()->GetEnd(4)!=1200000350.0) return 2;
  cout<<"\tExtractChannels()"<<endl;
  if(F->ExtractChannels(941380000)==false) return 2;
  cout<<"\tGetChannelList()"<<endl;
  chanlist = F->GetChannelList("");
  if(chanlist.size()!=10) return 2;
  if(chanlist[0].compare("V1:Hrec_Flag_Channel")) return 2;
  if(chanlist[1].compare("V1:Hrec_Flag_Finesse")) return 2;
  if(chanlist[9].compare("V1:h_4096Hz")) return 2;
  chanlist = F->GetChannelList("*Flag*");
  if(chanlist.size()!=7) return 2;
  chanlist = F->GetChannelList("V1:Hrec_Flag_LineRmvl V1:h_16384Hz");
  if(chanlist.size()!=2) return 2;
  chanlist = F->GetChannelList("V1:h_?????Hz");
  if(chanlist.size()!=2) return 2;
  chanlist = F->GetChannelList("V1:h_?????Hz V1:Hrec_Flag_*");
  if(chanlist.size()!=9) return 2;
  chanlist = F->GetChannelList("*");
  if(chanlist.size()!=10) return 2;
  cout<<"\tIsChannel()"<<endl;
  if(F->IsChannel("V1:h_16384Hz")!=true) return 2;
  if(F->IsChannel("V1:h_Hz")!=false) return 2;
  cout<<"\tGetChannelSampling()"<<endl;
  if(F->GetChannelSampling("V1:h_16384Hz")!=16384) return 2;
  if(F->GetChannelSampling("V1:Hrec_Flag_LineRmvl")!=1) return 2;
  if(F->GetChannelSampling("V1:Hrec")!=0) return 2;
  cout<<"\tGetData() (FFL)"<<endl;
  data = F->GetData(dsize, "V1:DOES-NOT-EXIST", 941380000, 941380010);
  if(dsize!=0) return 2;
  if(data!=NULL) return 2;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380020);
  if(dsize!=0) return 2;
  if(data!=NULL) return 2;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380010);
  if(dsize!=16384*10) return 2;
  delete data;
  data=F->GetData(dsize, "V1:h_20000Hz", 941380000, 941380010);
  if(dsize!=20000*10) return 2;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380010);
  if(dsize!=1*10) return 2;
  if(data==NULL) return 2;
  if(data[0]!=1) return 2;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380001);
  if(dsize!=1*1) return 2;
  if(data==NULL) return 2;
  if(data[0]!=1) return 2;
  delete data;

  delete F;
  
  // generatea PURE LCF file
  of.open("./gwl-test-ffl-dir/gwl-test-ffl.lcf");
  if(of.fail()) return 1;
  of<<"H1 LL 1162473868 4 file://localhost"<<gwf+"/H-H1_llhoft-1162473868-4.gwf"<<endl;// seg[2]
  of<<"H1 LL 1200000000 100 file://localhost/it/does/not/matter/frame1.gwf"<<endl;// seg[3]
  of<<"H1 LL 1200000300 50 file://localhost/it/does/not/matter/frame2.gwf"<<endl;// seg[4] (II)
  of<<"H1 LL 1200000200 100 file://localhost/it/does/not/matter/frame3.gwf"<<endl;// seg[4] (I)
  of<<"V1 VSR2 941380000 10 file://localhost"<<gwf+"/V1-hrec-vsr2.gwf"<<endl; // seg[0]
  of<<"H1 LL 1162473860 4 file://localhost"<<gwf+"/H-H1_llhoft-1162473860-4.gwf"<<endl;// seg[1]
  of.close();
  
  //*********************************************************************
  // ffl() [PURE LCF]
  //*********************************************************************
  cout<<"\nffl() [PURE LCF]"<<endl;
  F = new ffl("./gwl-test-ffl-dir/gwl-test-ffl.lcf", "GWOLLUM", 0, "./gwl-test-ffl-dir/", 1, 0);
  cout<<"\tGetStatus()"<<endl;
  if(F->GetStatus()==false) return 3;
  cout<<"\tGetInputFfl()"<<endl;
  if(F->GetInputFfl().compare("./gwl-test-ffl-dir/gwl-test-ffl.lcf")) return 3;
  cout<<"\tGetSegments()"<<endl;
  if(F->GetSegments()->GetLiveTime()!=268.0) return 3;
  if(F->GetSegments()->GetN()!=5) return 3;
  if(F->GetSegments()->GetStart(0)!=941380000.0) return 3;
  if(F->GetSegments()->GetStart(1)!=1162473860.0) return 3;
  if(F->GetSegments()->GetStart(2)!=1162473868.0) return 3;
  if(F->GetSegments()->GetStart(3)!=1200000000.0) return 3;
  if(F->GetSegments()->GetStart(4)!=1200000200.0) return 3;
  if(F->GetSegments()->GetEnd(0)!=941380010.0) return 3;
  if(F->GetSegments()->GetEnd(1)!=1162473864.0) return 3;
  if(F->GetSegments()->GetEnd(2)!=1162473872.0) return 3;
  if(F->GetSegments()->GetEnd(3)!=1200000100.0) return 3;
  if(F->GetSegments()->GetEnd(4)!=1200000350.0) return 3;
  cout<<"\tExtractChannels()"<<endl;
  if(F->ExtractChannels(941380000)==false) return 3;
  cout<<"\tGetChannelList()"<<endl;
  chanlist = F->GetChannelList("");
  if(chanlist.size()!=10) return 3;
  if(chanlist[0].compare("V1:Hrec_Flag_Channel")) return 3;
  if(chanlist[1].compare("V1:Hrec_Flag_Finesse")) return 3;
  if(chanlist[9].compare("V1:h_4096Hz")) return 3;
  chanlist = F->GetChannelList("*Flag*");
  if(chanlist.size()!=7) return 3;
  chanlist = F->GetChannelList("V1:Hrec_Flag_LineRmvl V1:h_16384Hz");
  if(chanlist.size()!=2) return 3;
  chanlist = F->GetChannelList("V1:h_?????Hz");
  if(chanlist.size()!=2) return 3;
  chanlist = F->GetChannelList("V1:h_?????Hz V1:Hrec_Flag_*");
  if(chanlist.size()!=9) return 3;
  chanlist = F->GetChannelList("*");
  if(chanlist.size()!=10) return 3;
  cout<<"\tIsChannel()"<<endl;
  if(F->IsChannel("V1:h_16384Hz")!=true) return 3;
  if(F->IsChannel("V1:h_Hz")!=false) return 3;
  cout<<"\tGetChannelSampling()"<<endl;
  if(F->GetChannelSampling("V1:h_16384Hz")!=16384) return 3;
  if(F->GetChannelSampling("V1:Hrec_Flag_LineRmvl")!=1) return 3;
  if(F->GetChannelSampling("V1:Hrec")!=0) return 3;
  cout<<"\tGetData() (FFL)"<<endl;
  data = F->GetData(dsize, "V1:DOES-NOT-EXIST", 941380000, 941380010);
  if(dsize!=0) return 3;
  if(data!=NULL) return 3;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380020);
  if(dsize!=0) return 3;
  if(data!=NULL) return 3;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380010);
  if(dsize!=16384*10) return 3;
  delete data;
  data=F->GetData(dsize, "V1:h_20000Hz", 941380000, 941380010);
  if(dsize!=20000*10) return 3;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380010);
  if(dsize!=1*10) return 3;
  if(data==NULL) return 3;
  if(data[0]!=1) return 3;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380001);
  if(dsize!=1*1) return 3;
  if(data==NULL) return 3;
  if(data[0]!=1) return 3;
  delete data;

  delete F;
  
  // generate a MIX FFL/LCF file
  of.open("./gwl-test-ffl-dir/gwl-test-ffl.mix");
  if(of.fail()) return 1;
  of<<gwf+"/H-H1_llhoft-1162473868-4.gwf"<<" 1162473868 4  0 0"<<endl;// seg[2] (FFL)
  of<<"H1 LL 1200000000 100 file://localhost/it/does/not/matter/frame1.gwf"<<endl;// seg[3] (LCF)
  of<<"./gwl-test-ffl-dir/gwl-test-ffl.ffl 1200000300 100"<<endl; // FFL FILE
  of<<"/it/does/not/matter/frame3.gwf  1200000200 100 0 0"<<endl;// seg[4] (I)
  of<<"./gwl-test-ffl-dir/gwl-test-ffl.lcf 1162473860 4"<<endl;// LCF FILE
  of.close();
  of.open("./gwl-test-ffl-dir/gwl-test-ffl.ffl");
  if(of.fail()) return 1;
  of<<"/it/does/not/matter/frame2.gwf  1200000300 50 0 0"<<endl;// seg[4] (II)
  of<<gwf+"/V1-hrec-vsr2.gwf"<<" 941380000 10  0 0"<<endl; // seg[0]
  of.close();
  of.open("./gwl-test-ffl-dir/gwl-test-ffl.lcf");
  if(of.fail()) return 1;
  of<<"H1 LL 1162473860 4 file://localhost"<<gwf+"/H-H1_llhoft-1162473860-4.gwf"<<endl;// seg[1]
  of.close();
  
  //*********************************************************************
  // ffl() [MIX FFL/LCF]
  //*********************************************************************
  cout<<"\nffl() [MIX FFL/LCF]"<<endl;
  F = new ffl("./gwl-test-ffl-dir/gwl-test-ffl.mix", "GWOLLUM", 0, "./gwl-test-ffl-dir/", 1, 0);
  cout<<"\tGetStatus()"<<endl;
  if(F->GetStatus()==false) return 4;
  cout<<"\tGetInputFfl()"<<endl;
  if(F->GetInputFfl().compare("./gwl-test-ffl-dir/gwl-test-ffl.mix")) return 4;
  cout<<"\tGetSegments()"<<endl;
  if(F->GetSegments()->GetLiveTime()!=268.0) return 4;
  if(F->GetSegments()->GetN()!=5) return 4;
  if(F->GetSegments()->GetStart(0)!=941380000.0) return 4;
  if(F->GetSegments()->GetStart(1)!=1162473860.0) return 4;
  if(F->GetSegments()->GetStart(2)!=1162473868.0) return 4;
  if(F->GetSegments()->GetStart(3)!=1200000000.0) return 4;
  if(F->GetSegments()->GetStart(4)!=1200000200.0) return 4;
  if(F->GetSegments()->GetEnd(0)!=941380010.0) return 4;
  if(F->GetSegments()->GetEnd(1)!=1162473864.0) return 4;
  if(F->GetSegments()->GetEnd(2)!=1162473872.0) return 4;
  if(F->GetSegments()->GetEnd(3)!=1200000100.0) return 4;
  if(F->GetSegments()->GetEnd(4)!=1200000350.0) return 4;
  cout<<"\tExtractChannels()"<<endl;
  if(F->ExtractChannels(941380000)==false) return 4;
  cout<<"\tGetChannelList()"<<endl;
  chanlist = F->GetChannelList("");
  if(chanlist.size()!=10) return 4;
  if(chanlist[0].compare("V1:Hrec_Flag_Channel")) return 4;
  if(chanlist[1].compare("V1:Hrec_Flag_Finesse")) return 4;
  if(chanlist[9].compare("V1:h_4096Hz")) return 4;
  chanlist = F->GetChannelList("*Flag*");
  if(chanlist.size()!=7) return 4;
  chanlist = F->GetChannelList("V1:Hrec_Flag_LineRmvl V1:h_16384Hz");
  if(chanlist.size()!=2) return 4;
  chanlist = F->GetChannelList("V1:h_?????Hz");
  if(chanlist.size()!=2) return 4;
  chanlist = F->GetChannelList("V1:h_?????Hz V1:Hrec_Flag_*");
  if(chanlist.size()!=9) return 4;
  chanlist = F->GetChannelList("*");
  if(chanlist.size()!=10) return 4;
  cout<<"\tIsChannel()"<<endl;
  if(F->IsChannel("V1:h_16384Hz")!=true) return 4;
  if(F->IsChannel("V1:h_Hz")!=false) return 4;
  cout<<"\tGetChannelSampling()"<<endl;
  if(F->GetChannelSampling("V1:h_16384Hz")!=16384) return 4;
  if(F->GetChannelSampling("V1:Hrec_Flag_LineRmvl")!=1) return 4;
  if(F->GetChannelSampling("V1:Hrec")!=0) return 4;
  cout<<"\tGetData() (FFL)"<<endl;
  data = F->GetData(dsize, "V1:DOES-NOT-EXIST", 941380000, 941380010);
  if(dsize!=0) return 4;
  if(data!=NULL) return 4;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380020);
  if(dsize!=0) return 4;
  if(data!=NULL) return 4;
  data=F->GetData(dsize, "V1:h_16384Hz", 941380000, 941380010);
  if(dsize!=16384*10) return 4;
  delete data;
  data=F->GetData(dsize, "V1:h_20000Hz", 941380000, 941380010);
  if(dsize!=20000*10) return 4;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380010);
  if(dsize!=1*10) return 4;
  if(data==NULL) return 4;
  if(data[0]!=1) return 4;
  delete data;
  data=F->GetData(dsize, "V1:Hrec_Flag_Channel", 941380000, 941380001);
  if(dsize!=1*1) return 4;
  if(data==NULL) return 4;
  if(data[0]!=1) return 4;
  delete data;

  delete F;
  
  if(RemoveFileOrDirectory(ec, "./gwl-test-ffl-dir")==0){
    cerr<<"clean-up failed ("<<ec<<")"<<endl;
    return 255;
  }
    
  return 0;
}

