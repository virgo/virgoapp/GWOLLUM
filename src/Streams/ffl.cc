/**
 * @file 
 * @brief See ffl.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ffl.h"

ClassImp(ffl)


////////////////////////////////////////////////////////////////////////////////////
ffl::ffl(const string aFrameFileList,
         const string aPlotStyle,
         const unsigned int aVerbosity,
         const string aTmpDirPath,
         const unsigned int aNTrials,
         const unsigned int aNSleepSeconds){
////////////////////////////////////////////////////////////////////////////////////

  // init
  fInFrameFile = aFrameFileList;
  frfile = NULL;

  // monitoring
  mon = new Monitor("ffl");
  mon->SetVerbosityLevel(aVerbosity);
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;

  // read input file
  ffl_pure = true;
  ReadInputFile(fInFrameFile);

  // random id
  int randint = rand();
  gwl_ss<<randint<<"_"<<getpid();
  srandint = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");

  // test tmp dir
  if(filesystem::is_directory(aTmpDirPath)==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": the tmp directory "<<aTmpDirPath<<" does not exist"<<endl;
  }

  // working FFL file
  if(ffl_pure) fWoFrameFile = fInFrameFile;
  else{
    fWoFrameFile = aTmpDirPath + "/ffconvert."+srandint+".ffl";
    ofstream ofs;
    ofs.open(fWoFrameFile);
    if(ofs.is_open()==true){
      for(unsigned int f=0; f<ffl_framefile.size(); f++){
        ofs<<ffl_framefile[f]<<" "<<ffl_gps[f]<<" "<<ffl_dur[f]<<" 0 0"<<endl;
      }
      ofs.close();
    }
  }
  mon->SetMessageType(monitor_info_4);
  *mon<<"ffl::"<<__func__<<": working ffl file = "<<fWoFrameFile<<endl;

  // FFL segments
  seg = new Segments();
  for(unsigned int s=0; s<ffl_gps.size(); s++){
    seg->AddSegment((double)ffl_gps[s], (double)(ffl_gps[s]+ffl_dur[s]), 1);
  }
  mon->SetMessageType(monitor_info_4);
  *mon<<"ffl::"<<__func__<<": segment livetime = "<<seg->GetLiveTime()<<" s"<<endl;
  mon->UpdateStatus(seg->GetStatus());
  
  // multiple trials when reading the data
  ntrials = aNTrials;
  nsleepsec = aNSleepSeconds;

  // make Fr file
  for(unsigned int n=0; n<ntrials; n++){
    frfile = FrFileINew((char*)fWoFrameFile.c_str());
    if(frfile!=NULL) break;
    sleep(nsleepsec);
  }
  
  // check frfile
  if(frfile==NULL){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": FFL file "<<fWoFrameFile<<" cannot be loaded"<<endl;
  }

  // list of channels
  channels.clear();
  sampling.clear();

  // plotting
  GP = new GwollumPlot("ffl_graph_"+srandint, aPlotStyle);
  G = new TGraph();
  G->SetName(("ffl_graph_"+srandint).c_str());
}

////////////////////////////////////////////////////////////////////////////////////
ffl::~ffl(){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;
  delete seg;
  delete G;
  delete GP;
  ffl_framefile.clear();
  ffl_gps.clear();
  ffl_dur.clear();
  channels.clear();
  sampling.clear();
  delete mon;
}


////////////////////////////////////////////////////////////////////////////////////
void ffl::SetPlotName(const string aName){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;
  string p = GP->GetCurrentStyle();
  delete GP;
  GP = new GwollumPlot(aName, p);
}
  
////////////////////////////////////////////////////////////////////////////////////
bool ffl::ExtractChannels(const unsigned int aGps){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  if(aGps>0&&!seg->IsInsideSegment(aGps)){
    mon->SetMessageType(monitor_error);
    *mon<<"ffl::"<<__func__<<": the requested GPS time ("<<aGps<<") is not covered by the ffl"<<endl;
    return false;
  }

  // get channel list
  mon->SetMessageType(monitor_info_3);
  *mon<<"extract channel list"<<endl;
  char* clist = NULL;
  for(unsigned int n=0; n<ntrials; n++){
    clist = FrFileIGetChannelList(frfile, aGps);
    if(clist!=NULL) break;
    sleep(nsleepsec);
  }
  if(clist==NULL){
    mon->SetMessageType(monitor_error);
    *mon<<"ffl::"<<__func__<<": no channel returned"<<endl;
    return false;
  }
  string slist = (string)clist;
  delete clist;

  // reset channel list
  channels.clear();
  sampling.clear();

  // split into lines
  vector <string> lines = SplitString(slist, '\n');
  
  // read each line
  mon->SetMessageType(monitor_info_3);
  *mon<<"ffl::"<<__func__<<": filter and format channel list"<<endl;
  vector <string> words;
  double tmp;
  for(unsigned int l=0; l<lines.size(); l++){
    words=SplitString(lines[l], ' ');

    // there should be 3 words: TYPE - NAME - SAMPLING
    if(words.size()<3){
      words.clear();
      continue;
    }

    // search for ADC/PROC/SIM
    if((!words[0].compare("ADC")) ||
       (!words[0].compare("PROC")) ||
       (!words[0].compare("SIM"))){
      tmp=atof(words[2].c_str());
      if(tmp&&tmp==floor(tmp)){
	channels.push_back(words[1]);
	sampling.push_back((unsigned int)tmp);
      }
    }

    // search for SER
    else if(!words[0].compare("SER")){
      tmp=atof(words[2].c_str());
      if(tmp&&tmp==floor(tmp)){
	for(unsigned int w=3; w<words.size(); w+=2){
	  channels.push_back(words[1]+"_"+words[w]);
	  sampling.push_back((unsigned int)tmp);
	}
      }
    }
    
    words.clear();
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
vector <string> ffl::GetChannelList(const string aFilter){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;

  // no filter
  if(!aFilter.compare("")) return channels;

  vector <string> subchannels;

  // get sub-strings
  vector <string> substring = SplitString(aFilter, ' ');

  // loop over channels
  for(unsigned int c=0; c<channels.size(); c++){

    // check pattern
    for(unsigned int s=0; s<substring.size(); s++){
      if(!fnmatch(substring[s].c_str(), channels[c].c_str(), FNM_NOESCAPE)){
	subchannels.push_back(channels[c]);
	break;
      }
    }
  }

  return subchannels;
}

////////////////////////////////////////////////////////////////////////////////////
double* ffl::GetData(unsigned int &aSize, const string aChannelName,
                     const double aGpsStart, const double aGpsEnd,
                     const double aMissing){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": corrupted object"<<endl;
    aSize=0;
    return NULL;
  }
  if(aMissing != -999.0){
    if(seg->GetLiveTime(aGpsStart, aGpsEnd) != aGpsEnd-aGpsStart){
      mon->SetMessageType(monitor_error);
      *mon<<"ffl::"<<__func__<<": the requested GPS range is not entirely covered by the ffl"<<endl;
      aSize=0;
      return NULL;
    }
  }
  if(GetChannelSampling(aChannelName)==0){
    mon->SetMessageType(monitor_error);
    *mon<<"ffl::"<<__func__<<": "<<aChannelName<<" was not found in the data"<<endl;
    aSize=0;
    return NULL;
  }

  // output vector
  double *outvector;
  FrVect *chanvect = NULL; // main vector

  // fill Frame vector
  mon->SetMessageType(monitor_info_3);
  *mon<<"ffl::"<<__func__<<": read data for "<<aChannelName<<" between "<<aGpsStart<<" and "<<aGpsEnd<<endl;
  for(unsigned int n=0; n<ntrials; n++){
    chanvect = FrFileIGetVectDN(frfile, (char*)(aChannelName.c_str()),
                                aGpsStart, aGpsEnd-aGpsStart);
    if(chanvect!=NULL) break;
    sleep(nsleepsec);
  }
  
  // test output
  if(chanvect==NULL||!chanvect->nData){
    mon->SetMessageType(monitor_error);
    *mon<<"ffl::"<<__func__<<": data could not be read for "<<aChannelName<<" between "<<aGpsStart<<" and "<<aGpsEnd<<endl;
    if(chanvect!=NULL) FrVectFree(chanvect);
    aSize=0;
    return NULL;
  }

  // get size
  aSize=chanvect->nData;
  mon->SetMessageType(monitor_info_4);
  *mon<<"ffl::"<<__func__<<": "<<aSize<<" samples"<<endl;

  // copy output somewhere else
  mon->SetMessageType(monitor_info_3);
  *mon<<"ffl::"<<__func__<<": copy data vector"<<endl;
  outvector = new double [aSize];
  for(unsigned int i=0; i<aSize; i++){

    // test for nan (corrupted data)
    if(chanvect->dataD[i]==chanvect->dataD[i]) outvector[i]=chanvect->dataD[i];
    else{
      mon->SetMessageType(monitor_error);
      *mon<<"ffl::"<<__func__<<": corrupted data for "<<aChannelName<<" between "<<aGpsStart<<" and "<<aGpsEnd<<endl;
      FrVectFree(chanvect);
      delete [] outvector;
      aSize=0;
      return NULL;
    }
  }

  // missing data
  mon->SetMessageType(monitor_info_3);
  *mon<<"ffl::"<<__func__<<": check for missing data"<<endl;
  if(chanvect->next!=NULL){

    // return
    if(aMissing==999.0){
      mon->SetMessageType(monitor_error);
      *mon<<"ffl::"<<__func__<<": missing data for "<<aChannelName<<" between "<<aGpsStart<<" and "<<aGpsEnd<<endl;
      FrVectFree(chanvect);
      delete [] outvector;
      aSize=0;
      return NULL;
    }
 
    // flag missing samples
    for(unsigned int i=0; i<aSize; i++){
      if(chanvect->next->dataU[i]==0) outvector[i]=aMissing;
    }
  }
  FrVectFree(chanvect);

  return outvector;
}

////////////////////////////////////////////////////////////////////////////////////
bool ffl::PrintData(const string aChannelName, const double aGpsStart, const double aGpsEnd, string aFileName){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // get data
  unsigned int dvsize;
  double *dv = GetData(dvsize, aChannelName, aGpsStart, aGpsEnd);
  if(dvsize==0) return false;

  // fill outputfile
  mon->SetMessageType(monitor_info_3);
  *mon<<"ffl::"<<__func__<<": save in output file"<<endl;
  ofstream outputfile(aFileName.c_str());
  if(outputfile.fail()){
    mon->SetMessageType(monitor_error);
    *mon<<"ffl::"<<__func__<<": cannot open output file "<<aFileName<<endl;
    delete [] dv;
    return false;
  }

  for(unsigned int i=0; i<dvsize; i++) 
    outputfile<<setprecision(16)<<aGpsStart+((double)i)*(aGpsEnd-aGpsStart)/(double)dvsize<<" "<<setprecision(6)<<dv[i]<<endl;
  delete [] dv;

  outputfile.close();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* ffl::PlotData(const string aChannelName, const double aGpsStart, const double aGpsEnd){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"ffl::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  // get data
  unsigned int dvsize;
  double *dv = GetData(dvsize, aChannelName, aGpsStart, aGpsEnd);
  if(dvsize==0) return NULL;

  // fill graph
  mon->SetMessageType(monitor_info_3);
  *mon<<"ffl::"<<__func__<<": make plot"<<endl;
  delete G;
  G = new TGraph(dvsize);
  G->SetName(("ffl_graph_"+srandint).c_str());
  for(unsigned int i=0; i<dvsize; i++) G->SetPoint(i, aGpsStart+(double)i*(aGpsEnd-aGpsStart)/(double)dvsize, dv[i]);
  delete [] dv;

  // plot
  G->GetHistogram()->GetXaxis()->SetNoExponent();
  G->GetHistogram()->GetXaxis()->SetNdivisions(5,5,0);
  G->GetHistogram()->GetXaxis()->SetTitle("Time [s]");
  G->GetHistogram()->SetTitle((aChannelName+" time series").c_str());
  G->GetHistogram()->GetYaxis()->SetTitle("Amplitude");
  GP->Draw(G,"APL");
  GP->SetGridx();  GP->SetGridy();

  return G;
}

////////////////////////////////////////////////////////////////////////////////////
void ffl::ReadInputFile(const string aInFile){
////////////////////////////////////////////////////////////////////////////////////

  // check input file
  if(filesystem::is_regular_file(aInFile)==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": ffl file "<<aInFile<<" cannot be found/read"<<endl;
    return;
  }

  // open input file
  ifstream infile;
  infile.open(aInFile);
  if(infile.is_open()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"ffl::"<<__func__<<": ffl file "<<aInFile<<" is corrupted"<<endl;
    return;
  }

  // read the file line by line to get the file format
  string line;
  vector<string> words;
  while(getline(infile, line)){

    // read words
    words = SplitString(line);

    // 5 words --> frame file
    if(words.size()==5){

      // LCF format
      if(atoi(words[1].c_str())==0){

        // remove "file:" and "//localhost" in the file path
        if((words[4].substr(0,5).compare("file:"))==false) words[4] = words[4].substr(5);
        if((words[4].substr(0,11).compare("//localhost"))==false) words[4] = words[4].substr(11);

        // save frame file
        ffl_framefile.push_back(words[4]);
        ffl_gps.push_back(stoul(words[2]));
        ffl_dur.push_back(stoul(words[3]));

        // not pure FFL
        ffl_pure = false;
      }

      // FFL format
      else{
        ffl_framefile.push_back(words[0]);
        ffl_gps.push_back(stoul(words[1]));
        ffl_dur.push_back(stoul(words[2]));
      }
    }

    // 3 words --> points to another ffl/lcf
    else if(words.size()==3){
      ReadInputFile(words[0]);
      // not pure FFL
      ffl_pure = false;
    }

    // unknown format --> skip line
    else{};
  }
   
  infile.close();
  return;
}

