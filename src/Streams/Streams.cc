/**
 * @file 
 * @brief See Streams.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Streams.h"

ClassImp(Streams)

/////////////////////////////////////////////////////////////////////////
Streams::Streams(const string aName, const unsigned int aVerbose): Sample(aVerbose) { 
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Streams::"<<__func__<<endl;

  // set name
  SetName(aName);
}

/////////////////////////////////////////////////////////////////////////
Streams::~Streams(void){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Streams::"<<__func__<<endl;
}

/////////////////////////////////////////////////////////////////////////
void Streams::SetName(const string aName){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Streams::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Streams::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  Name=aName;

  if(Name.length()==0)
    Name="00:none";
  else if(aName.length()<4)
    Name="00:"+Name;
  else{};
  
  // 00 prefix
  if((Name.substr(2,1)).compare(":")) Name="00:"+Name;

  // prefix/suffix
  NamePrefix=Name.substr(0,2);
  NameSuffix=Name.substr(3,Name.length()-3);
  Name=NamePrefix+":"+NameSuffix;
  NameSuffixUnderScore=ReplaceAll(NameSuffix,"-","_");
  
  // make a detector
  MakeDetector();
  mon->SetMessageType(monitor_info_4);
  *mon<<"Streams::"<<__func__<<": this stream ("<<Name<<") is associated to detector: "<<DET_NAME[DetIndex]<<" ("<<DET_PREFIX[DetIndex]<<")"<<endl;

  return;
}

/////////////////////////////////////////////////////////////////////////
void Streams::GetDetectorAMResponse(double &aFplus, double &aFcross,
                               const double aRa, const double aDec,
                               const double aPsi, const double aGmst){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Streams::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Streams::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  double X[3];
  double Y[3];

  // Greenwich hour angle of source (radians)
  const double gha = aGmst - aRa;

  // pre-compute trig functions
  const double cosgha = cos(gha);
  const double singha = sin(gha);
  const double cosdec = cos(aDec);
  const double sindec = sin(aDec);
  const double cospsi = cos(aPsi);
  const double sinpsi = sin(aPsi);

  // Note that dec = pi/2 - theta, and gha =
  // -phi where theta and phi are the standard spherical coordinates
  // used in that paper
  X[0] = -cospsi * singha - sinpsi * cosgha * sindec;
  X[1] = -cospsi * cosgha + sinpsi * singha * sindec;
  X[2] =  sinpsi * cosdec;
  Y[0] =  sinpsi * singha - cospsi * cosgha * sindec;
  Y[1] =  sinpsi * cosgha + cospsi * singha * sindec;
  Y[2] =  cospsi * cosdec;

  aFplus = aFcross = 0.0;
  double DX, DY;
  for(unsigned int i = 0; i < 3; i++){
    DX = Response[i][0] * X[0] + Response[i][1] * X[1] + Response[i][2] * X[2];
    DY = Response[i][0] * Y[0] + Response[i][1] * Y[1] + Response[i][2] * Y[2];
    aFplus  += X[i] * DX - Y[i] * DY;
    aFcross += X[i] * DY + Y[i] * DX;
  }

  return;
}

/////////////////////////////////////////////////////////////////////////
bool Streams::GetSkyRingPoint(double &aThetaS, double &aPhiS, const double aGmst, const double aDeltaT, const double aOmegaS, Streams *aDprime){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Streams::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Streams::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // DD' vector
  double dx = DET_VERTEX_LOCATION_X_SI[aDprime->Streams::GetDetectorIndex()] - DET_VERTEX_LOCATION_X_SI[Streams::GetDetectorIndex()];
  double dy = DET_VERTEX_LOCATION_Y_SI[aDprime->Streams::GetDetectorIndex()] - DET_VERTEX_LOCATION_Y_SI[Streams::GetDetectorIndex()];
  double dz = DET_VERTEX_LOCATION_Z_SI[aDprime->Streams::GetDetectorIndex()] - DET_VERTEX_LOCATION_Z_SI[Streams::GetDetectorIndex()];

  // DD' norm
  double d = TMath::Sqrt(dx*dx + dy*dy + dz*dz);

  // same detector!
  if(d==0){
    mon->SetMessageType(monitor_error);
    *mon<<"Streams::"<<__func__<<": this function cannot be used for a single detector"<<endl;
    return false;
  }

  // cone opening angle
  double c_gamma_s = -TMath::C()*aDeltaT/d;
  if(c_gamma_s>1.0) c_gamma_s = 1.0;   // ~force dt to its maximal value 
  if(c_gamma_s<-1.0) c_gamma_s = -1.0; // ~force dt to its minimal value 
  double s_gamma_s = TMath::Sin(TMath::ACos(c_gamma_s));
  // sin/cos omega
  double c_omega_s = TMath::Cos(aOmegaS);
  double s_omega_s = TMath::Sin(aOmegaS);

  // cos and sin coefficients
  double dxy = TMath::Sqrt(dx*dx + dy*dy);
  double c_theta = dz / d;
  double s_theta = dxy / d;
  double c_phi = 1.0;
  double s_phi = 0.0;
  if(dxy>0.0){
    c_phi = dx / dxy;
    s_phi = dy / dxy;
  }
  
  // rotation matrix R' -> R (index: row/column)
  // (this is the transpose of R -> R')
  // https://eng.libretexts.org/Bookshelves/Mechanical_Engineering/System_Design_for_Uncertainty_(Hover_and_Triantafyllou)/09%3A_Kinematics_of_Moving_Frames/9.02%3A_Rotation_of_Reference_Frames
  double R_xx = c_phi*c_theta;
  double R_xy = -s_phi;
  double R_xz = c_phi*s_theta;
  double R_yx = s_phi*c_theta;
  double R_yy = c_phi;
  double R_yz = s_phi*s_theta;
  double R_zx = -s_theta;
  double R_zy = 0.0;
  double R_zz = c_theta;

  // apply the rotation to get theta_s first
  double c_theta_s = R_zx*s_gamma_s*c_omega_s + R_zy*s_gamma_s*s_omega_s + R_zz*c_gamma_s;
  aThetaS = TMath::ACos(c_theta_s);
  double s_theta_s = TMath::Sin(aThetaS);

  // then phi_s
  aPhiS = 0.0;
  if(s_theta_s!=0.0){
    double c_phi_s = (R_xx*s_gamma_s*c_omega_s + R_xy*s_gamma_s*s_omega_s + R_xz*c_gamma_s) / s_theta_s;
    double s_phi_s = (R_yx*s_gamma_s*c_omega_s + R_yy*s_gamma_s*s_omega_s + R_yz*c_gamma_s) / s_theta_s;
    aPhiS = TMath::ACos(c_phi_s);
    if(s_phi_s<0) aPhiS = 2.0*TMath::Pi() - aPhiS;
  }

  // Greenwich hour angle correction (radians) (mod 2pi)
  aPhiS += aGmst;
  aPhiS = fmod(aPhiS, 2.0*TMath::Pi());

  return true;
}

/////////////////////////////////////////////////////////////////////////
TH2D* Streams::GetSkyAntennaFactor(const unsigned int aGpsTime){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Streams::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Streams::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  // create histogram
  gwl_ss<<"af_"<<NamePrefix<<"_"<<aGpsTime;
  TH2D *h2 = new TH2D(gwl_ss.str().c_str(), ("Antenna factor ("+NamePrefix+")").c_str(), 200, 0.0, 2.0*TMath::Pi(), 200, -TMath::Pi()/2, TMath::Pi()/2);
  gwl_ss.clear(); gwl_ss.str("");
  h2->GetXaxis()->SetTitle("Right ascension [rad]");
  h2->GetYaxis()->SetTitle("Declination [rad]");
  h2->GetZaxis()->SetTitle("#sqrt{F_{+}^{2} + F_{x}^{2}}");

  //
  double Gmst = GreenwichMeanSiderealTime(aGpsTime);

  // fill histogram
  double Fp, Fc;
  for(int bx=1; bx<=h2->GetNbinsX(); bx++){
    for(int by=1; by<=h2->GetNbinsX(); by++){
      GetDetectorAMResponse(Fp, Fc, h2->GetXaxis()->GetBinCenter(bx), h2->GetYaxis()->GetBinCenter(by), 1.0, Gmst);
      h2->SetBinContent(bx, by, TMath::Sqrt(Fp*Fp + Fc*Fc));
    }
  }
  
  return h2;
}

/////////////////////////////////////////////////////////////////////////
void Streams::MakeDetector(void){
/////////////////////////////////////////////////////////////////////////

  // get det index based on the prefix
  DetIndex=::GetDetectorIndex(NamePrefix);

  // compute detector response
  ComputeDetectorResponse();

  // detector location
  DetLoc[0] = DET_VERTEX_LOCATION_X_SI[DetIndex];
  DetLoc[1] = DET_VERTEX_LOCATION_Y_SI[DetIndex];
  DetLoc[2] = DET_VERTEX_LOCATION_Z_SI[DetIndex];

  return;
}

/////////////////////////////////////////////////////////////////////////
void Streams::ComputeDetectorResponse(void){
/////////////////////////////////////////////////////////////////////////
  Response[0][0]=0.5*(DET_ARM_X_DIRECTION_X[DetIndex]*DET_ARM_X_DIRECTION_X[DetIndex]-DET_ARM_Y_DIRECTION_X[DetIndex]*DET_ARM_Y_DIRECTION_X[DetIndex]);
  Response[0][1]=0.5*(DET_ARM_X_DIRECTION_X[DetIndex]*DET_ARM_X_DIRECTION_Y[DetIndex]-DET_ARM_Y_DIRECTION_X[DetIndex]*DET_ARM_Y_DIRECTION_Y[DetIndex]);
  Response[0][2]=0.5*(DET_ARM_X_DIRECTION_X[DetIndex]*DET_ARM_X_DIRECTION_Z[DetIndex]-DET_ARM_Y_DIRECTION_X[DetIndex]*DET_ARM_Y_DIRECTION_Z[DetIndex]);
  Response[1][0]=0.5*(DET_ARM_X_DIRECTION_Y[DetIndex]*DET_ARM_X_DIRECTION_X[DetIndex]-DET_ARM_Y_DIRECTION_Y[DetIndex]*DET_ARM_Y_DIRECTION_X[DetIndex]);
  Response[1][1]=0.5*(DET_ARM_X_DIRECTION_Y[DetIndex]*DET_ARM_X_DIRECTION_Y[DetIndex]-DET_ARM_Y_DIRECTION_Y[DetIndex]*DET_ARM_Y_DIRECTION_Y[DetIndex]);
  Response[1][2]=0.5*(DET_ARM_X_DIRECTION_Y[DetIndex]*DET_ARM_X_DIRECTION_Z[DetIndex]-DET_ARM_Y_DIRECTION_Y[DetIndex]*DET_ARM_Y_DIRECTION_Z[DetIndex]);
  Response[2][0]=0.5*(DET_ARM_X_DIRECTION_Z[DetIndex]*DET_ARM_X_DIRECTION_X[DetIndex]-DET_ARM_Y_DIRECTION_Z[DetIndex]*DET_ARM_Y_DIRECTION_X[DetIndex]);
  Response[2][1]=0.5*(DET_ARM_X_DIRECTION_Z[DetIndex]*DET_ARM_X_DIRECTION_Y[DetIndex]-DET_ARM_Y_DIRECTION_Z[DetIndex]*DET_ARM_Y_DIRECTION_Y[DetIndex]);
  Response[2][2]=0.5*(DET_ARM_X_DIRECTION_Z[DetIndex]*DET_ARM_X_DIRECTION_Z[DetIndex]-DET_ARM_Y_DIRECTION_Z[DetIndex]*DET_ARM_Y_DIRECTION_Z[DetIndex]);

  return;
}
