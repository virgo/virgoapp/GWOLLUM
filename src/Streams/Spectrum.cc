/**
 * @file 
 * @brief See Spectrum.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Spectrum.h"

ClassImp(Spectrum)

////////////////////////////////////////////////////////////////////////////////////
Spectrum::Spectrum(const unsigned int aSpectrumSize,
                   const unsigned int aDataDuration,
                   const unsigned int aDataSamplingFrequency,
                   const unsigned int aVerbosity){ 
////////////////////////////////////////////////////////////////////////////////////

  // save parameters
  sampling = aDataSamplingFrequency;
  unsigned int datasize = aDataDuration*sampling;

  // monitoring
  mon = new Monitor("spectrum");
  mon->SetVerbosityLevel(aVerbosity);
  mon->SetMessageType(monitor_info_2);
  *mon<<"Spectrum::"<<__func__<<endl;

  // FFT
  mon->SetMessageType(monitor_info_3);
  *mon<<"Spectrum::"<<__func__<<": create fft plans"<<endl;
  unsigned int fft_size = TMath::Max(2*aSpectrumSize,(unsigned int)2);
  fft_psd = new fft(fft_size, "FFTW_MEASURE", "r2c");

  // the data size must be large enough
  if(datasize<fft_psd->GetSize_t()/2) datasize = fft_psd->GetSize_t()/2;
     
  // nominal number of subsegments
  nSubSegments[0] = datasize/fft_psd->GetSize_t();
  nSubSegments[1] = (datasize-fft_psd->GetSize_t()/2)/fft_psd->GetSize_t();
  if(nSubSegments[0]+nSubSegments[1] == 0){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": the data duration ("<<datasize<<" points) is too short to estimate the PSD with this resolution ("<<fft_psd->GetSize_f()<<" points)"<<endl;
    nSubSegments[0] = 0;
    nSubSegments[1] = 0;
  }
  
  // periodogram buffer
  mon->SetMessageType(monitor_info_3);
  *mon<<"Spectrum::"<<__func__<<": create periodogram buffers"<<endl;
  for(unsigned int t=0; t<2; t++){
    periodogram[t] = new double* [nSubSegments[t]];
    for(unsigned int s=0; s<nSubSegments[t]; s++){
      periodogram[t][s] = new double [fft_psd->GetSize_f()];
      for(unsigned int i=0; i<fft_psd->GetSize_f(); i++)
        periodogram[t][s][i] = -1.0;
    }
  }

  // normalized hann window
  // to enforce unity RMS
  mon->SetMessageType(monitor_info_3);
  *mon<<"Spectrum::"<<__func__<<": create hann windows"<<endl;
  HannWindow = GetTukeyWindow(fft_psd->GetSize_t(), 1.0);
  double norm=0.0;
  for(unsigned int i=0; i<fft_psd->GetSize_t(); i++)
    norm += HannWindow[i]*HannWindow[i];
  norm = TMath::Sqrt(norm/(double)fft_psd->GetSize_t());
  for(unsigned int i=0; i<fft_psd->GetSize_t(); i++)
    HannWindow[i] /= norm;

  // set PSD to 0
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
    fft_psd->SetRe_f(i, 0.0);
    fft_psd->SetIm_f(i, 0.0);
  }

  // init all
  Reset();

  // print info
  mon->SetMessageType(monitor_info_4);
  *mon<<"Spectrum::"<<__func__<<": input data sampling frequency = "<<sampling<<" Hz"<<endl;
  mon->SetMessageType(monitor_info_4);
  *mon<<"Spectrum::"<<__func__<<": PSD size                      = "<<GetSpectrumSize()<<" points"<<endl;
  mon->SetMessageType(monitor_info_4);
  *mon<<"Spectrum::"<<__func__<<": PSD resolution                = "<<GetSpectrumResolution()<<" Hz"<<endl;
  mon->SetMessageType(monitor_info_4);
  *mon<<"Spectrum::"<<__func__<<": number of even samples        = "<<GetNSubSegmentsMax(0)<<endl;
  mon->SetMessageType(monitor_info_4);
  *mon<<"Spectrum::"<<__func__<<": number of odd samples         = "<<GetNSubSegmentsMax(1)<<endl;
}

////////////////////////////////////////////////////////////////////////////////////
Spectrum::~Spectrum(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Spectrum::"<<__func__<<endl;
  delete [] HannWindow;
  delete fft_psd;
  for(unsigned int t=0; t<2; t++){
    for(unsigned int s=0; s<nSubSegments[t]; s++) delete [] periodogram[t][s];
    delete [] periodogram[t];
  }
  delete mon;
}

////////////////////////////////////////////////////////////////////////////////////
void Spectrum::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Spectrum::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // reset buffer
  for(unsigned int t=0; t<2; t++){
    write_index[t]=0;
    for(unsigned int s=0; s<nSubSegments[t]; s++)
      periodogram[t][s][0] = -1.0;
  }

  // reset PSD
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
    fft_psd->SetRe_f(i, 0.0);
    fft_psd->SetIm_f(i, 0.0);
  }
}

////////////////////////////////////////////////////////////////////////////////////
double Spectrum::GetPower(const double aFrequency){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return 0.0;
  }

  if(GetSpectrumSize()==0) return 0.0;
  if(GetSpectrumSize()==1) return fft_psd->GetRe_f(0U);

  // return DC power
  if(aFrequency<=0) return fft_psd->GetRe_f(0U);

  // frequency index (below)
  unsigned int findex = (unsigned int)(aFrequency/GetSpectrumResolution());

  // evaluate power
  double psd;
  double logpsd;
  if(findex>=GetSpectrumSize()){ // linear extrapolation (log)
    psd = fft_psd->GetRe_f(GetSpectrumSize()-1)
      + (fft_psd->GetRe_f(GetSpectrumSize()-1)-fft_psd->GetRe_f(GetSpectrumSize()-2))/(GetSpectrumFrequency(GetSpectrumSize()-1)-GetSpectrumFrequency(GetSpectrumSize()-2)) * (aFrequency-GetSpectrumFrequency(GetSpectrumSize()-1));
    logpsd = fft_psd->GetIm_f(GetSpectrumSize()-1)
      + (fft_psd->GetIm_f(GetSpectrumSize()-1)-fft_psd->GetIm_f(GetSpectrumSize()-2))/(GetSpectrumFrequency(GetSpectrumSize()-1)-GetSpectrumFrequency(GetSpectrumSize()-2)) * (aFrequency-GetSpectrumFrequency(GetSpectrumSize()-1));
  }
  else{ // linear interpolation (log)
    psd = fft_psd->GetRe_f(findex)
      + (fft_psd->GetRe_f(findex+1)-fft_psd->GetRe_f(findex))/(GetSpectrumFrequency(findex+1)-GetSpectrumFrequency(findex)) * (aFrequency-GetSpectrumFrequency(findex));
    logpsd = fft_psd->GetIm_f(findex)
      + (fft_psd->GetIm_f(findex+1)-fft_psd->GetIm_f(findex))/(GetSpectrumFrequency(findex+1)-GetSpectrumFrequency(findex)) * (aFrequency-GetSpectrumFrequency(findex));
  }
  
  // no power
  if(psd<=0.0) return 0.0;
  
  return TMath::Exp(logpsd);
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::AddData(const unsigned int aDataSize,
                       const double *aData,
                       const unsigned int aDataStart,
                       const bool aMute){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Spectrum::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // number of new PSD subsegments
  unsigned int n_sub[2];
  n_sub[0] = aDataSize/fft_psd->GetSize_t();
  n_sub[1] = (aDataSize-fft_psd->GetSize_t()/2)/fft_psd->GetSize_t();
  if(n_sub[0]==0){
    mon->SetMessageType(monitor_error);
    *mon<<"Spectrum::"<<__func__<<": the data duration ("<<aDataSize<<" points) is too short to estimate the PSD with this resolution ("<<fft_psd->GetSize_f()<<" points)"<<endl;
    return false;
  }

  // loop over even and odd sub-segments
  mon->SetMessageType(monitor_info_3);
  *mon<<"Spectrum::"<<__func__<<": make periodograms"<<endl;
  unsigned int index_start;
  for(unsigned int t=0; t<2; t++){

    if(!nSubSegments[t]) continue; // not to divide by 0 (%nSubSegments[t])

    for(unsigned int s=0; s<n_sub[t]; s++){

      index_start = aDataStart + (2*s+t)*fft_psd->GetSize_t()/2;

      // fill subsegment data vector
      for(unsigned int i=0; i<fft_psd->GetSize_t(); i++)
        fft_psd->SetRe_t(i,aData[index_start+i]*HannWindow[i]);

      // fft-forward it
      fft_psd->Forward();

      // get power for this sub-segment
      // normalize periodogram to give correct units:
      // 2/Tspec = 2f_s/N (periodogram)
      // /f_s / f_s (FFT squared)
      for(unsigned int i=0; i<fft_psd->GetSize_f(); i++)
	periodogram[t][write_index[t]][i] = 2.0*fft_psd->GetNorm2_f(i)/(double)fft_psd->GetSize_t()/sampling;

      // increment write index
      write_index[t]=(write_index[t]+1)%nSubSegments[t];
    }
  }

  // compute PSD
  mon->SetMessageType(monitor_info_3);
  *mon<<"Spectrum::"<<__func__<<": compute PSD (median-mean)"<<endl;
  ComputeMedianMean(aMute);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::LoadData(const unsigned int aDataSize,
                        const double *aData,
                        const unsigned int aDataStart,
                        const bool aMute){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Spectrum::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return false;
  }
  Reset();
  return AddData(aDataSize, aData, aDataStart, aMute);
}

////////////////////////////////////////////////////////////////////////////////////
void Spectrum::ComputeMedianMean(const bool aMute){
////////////////////////////////////////////////////////////////////////////////////

  // count number of active buffers
  unsigned int size_odd=0, size_evn=0;
  for(unsigned int s=0; s<nSubSegments[0]; s++) if(periodogram[0][s][0]>=0.0) size_evn++;
  for(unsigned int s=0; s<nSubSegments[1]; s++) if(periodogram[1][s][0]>=0.0) size_odd++;

  // buffer is empty
  if(size_evn+size_odd==0) return;
  
  // warning: non-nominal PSD estimation
  if(aMute==false){
    if(size_evn!=nSubSegments[0] || size_odd!=nSubSegments[1]){
      mon->SetMessageType(monitor_warning);
      *mon<<"Spectrum::"<<__func__<<": the PSD estimation is not nominal: "<<size_evn<<"/"<<size_odd<<" subPSDs are used instead of "<<nSubSegments[0]<<"/"<<nSubSegments[1]<<endl;
    }
  }
  
  // allocate memory
  double *sorted_odd = new double [size_odd];
  double *sorted_evn = new double [size_evn];
  double median_odd=0, median_evn=0;
  unsigned int sa;
  
  // loop over frequency bins
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){

    // loop over even buffers
    sa=0;
    for(unsigned int s=0; s<nSubSegments[0]; s++){
      if(periodogram[0][s][0]>=0){
	sorted_evn[sa]=periodogram[0][s][i];
	sa++;
      }
    }

    // loop over odd buffers
    sa=0;
    for(unsigned int s=0; s<nSubSegments[1]; s++){
      if(periodogram[1][s][0]>=0){
	sorted_odd[sa]=periodogram[1][s][i];
	sa++;
      }
    }

    // take median and correct median bias
    if(size_odd)
      median_odd = GetMedian(size_odd, sorted_odd);
    else
      median_odd=0.0;
    if(size_evn)
      median_evn= GetMedian(size_evn, sorted_evn);
    else
      median_evn=0.0;
    median_odd/=medianbiasfactor(size_odd);
    median_evn/=medianbiasfactor(size_evn);
    
    // average odd and even medians --> PSD
    // Re --> PSD
    // Im --> log(PSD)
    fft_psd->SetRe_f(i, ((double)size_odd*median_odd+(double)size_evn*median_evn) / (double)(size_odd+size_evn));
    if(fft_psd->GetRe_f(i)>0) fft_psd->SetIm_f(i, TMath::Log(fft_psd->GetRe_f(i)));
    else fft_psd->SetIm_f(i, 0.0);
  }
  
  // clean memory
  delete [] sorted_odd;
  delete [] sorted_evn;
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::IsBufferFull(void){
////////////////////////////////////////////////////////////////////////////////////

  // count number of active buffers
  unsigned int size_odd=0, size_evn=0;
  for(unsigned int s=0; s<nSubSegments[0]; s++) if(periodogram[0][s][0]>=0.0) size_evn++;
  for(unsigned int s=0; s<nSubSegments[1]; s++) if(periodogram[1][s][0]>=0.0) size_odd++;

  // even buffer is not full
  if(size_evn<nSubSegments[0]) return false;

  // odd buffer is not full
  if(size_odd<nSubSegments[1]) return false;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetPSD(void){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }
  TGraph *GPSD = new TGraph(fft_psd->GetSize_f());
  GPSD->SetName("PSD");
  GPSD->SetTitle("Power spectrum density");
  GPSD->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++)
    GPSD->SetPoint(GPSD->GetN(), GetSpectrumFrequency(i), GetPower(i));
  return GPSD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetPSD(const double aFrequencyMin, const double aFrequencyMax){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }
  TGraph *GPSD = new TGraph();
  GPSD->SetName("PSD");
  GPSD->SetTitle("Power spectrum density");
  GPSD->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");
  double freq;
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
    freq = GetSpectrumFrequency(i);
    if(freq<aFrequencyMin) continue;
    if(freq>aFrequencyMax) break;
    GPSD->SetPoint(GPSD->GetN(), freq, GetPower(i));
  }
  return GPSD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetASD(void){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }
  TGraph *GASD = new TGraph(fft_psd->GetSize_f());
  GASD->SetName("ASD");
  GASD->SetTitle("Amplitude spectrum density");
  GASD->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++)
    GASD->SetPoint(i, GetSpectrumFrequency(i), TMath::Sqrt(GetPower(i))); 
  return GASD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetASD(const double aFrequencyMin, const double aFrequencyMax){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }
  TGraph *GASD = new TGraph();
  GASD->SetName("ASD");
  GASD->SetTitle("Amplitude spectrum density");
  GASD->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");
  double freq;
  for(unsigned int i=0; i<fft_psd->GetSize_f(); i++){
    freq = GetSpectrumFrequency(i);
    if(freq<aFrequencyMin) continue;
    if(freq>aFrequencyMax) break;
    GASD->SetPoint(GASD->GetN(), freq, TMath::Sqrt(GetPower(i)));
  }
  return GASD;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetPeriodogram(const unsigned int aParity, const unsigned int aIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }
  TGraph *G = new TGraph(fft_psd->GetSize_f());
  gwl_ss<<"periodogram_"<<aParity%2<<"_"<<aIndex;
  G->SetName((gwl_ss.str()).c_str());
  gwl_ss.clear(); gwl_ss.str("");
  G->SetTitle("Periodogram");
  G->GetHistogram()->GetXaxis()->SetTitle("Frequency [Hz]");
  for(unsigned int i=0; i<(unsigned int)G->GetN(); i++)
    G->SetPoint(i, GetSpectrumFrequency(i), periodogram[aParity%2][aIndex][i]); 
  return G;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* Spectrum::GetAmplitudePeriodogram(const unsigned int aParity, const unsigned int aIndex){
////////////////////////////////////////////////////////////////////////////////////
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }
  TGraph *G = GetPeriodogram(aParity, aIndex);
  for(unsigned int i=0; i<(unsigned int)G->GetN(); i++)
    G->GetY()[i]=TMath::Sqrt(G->GetY()[i]);
  return G;
}

////////////////////////////////////////////////////////////////////////////////////
bool Spectrum::WritePeriodogram(const string aOutFileName){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Spectrum::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Spectrum::"<<__func__<<": corrupted object"<<endl;
    return false;
  }
  
  // output file
  TFile *fout = new TFile(aOutFileName.c_str(),"RECREATE");
  if(fout->IsZombie()) return false;
  fout->cd();

  // write periodograms
  TGraph *G;
  for(unsigned int t=0; t<2; t++){
    for(unsigned int s=0; s<nSubSegments[t]; s++){
      if(periodogram[t][s][0]>=0){
        G = GetPeriodogram(t, s);
        G->Write(G->GetName());
        delete G;
      }
    }
  }

  // write PSD
  G = GetPSD();
  G->Write("PSD");
  delete G;

  fout->Close();
  return true;
}

