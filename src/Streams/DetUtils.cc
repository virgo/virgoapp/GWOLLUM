/**
 * @file 
 * @brief See DetConst.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "DetConst.h"

double GetLightTravelTime(const string aDetectorPrefix1, const string aDetectorPrefix2){

  // detector index
  unsigned int det_index1 = GetDetectorIndex(aDetectorPrefix1);
  unsigned int det_index2 = GetDetectorIndex(aDetectorPrefix2);

  // cartesian distance
  double dx = DET_VERTEX_LOCATION_X_SI[det_index1] - DET_VERTEX_LOCATION_X_SI[det_index2];
  double dy = DET_VERTEX_LOCATION_Y_SI[det_index1] - DET_VERTEX_LOCATION_Y_SI[det_index2];
  double dz = DET_VERTEX_LOCATION_Z_SI[det_index1] - DET_VERTEX_LOCATION_Z_SI[det_index2];

  // light travel time
  return TMath::Sqrt(dx*dx+dy*dy+dz*dz) / TMath::C();
}
