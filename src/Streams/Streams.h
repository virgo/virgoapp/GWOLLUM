/**
 * @file 
 * @brief Data stream identifier.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Streams__
#define __Streams__

#include <TH2.h>
#include "TimeDelay.h"
#include "Sample.h"
#include "DetConst.h"

using namespace std;

/**
 * @brief Manage streams.
 * @details This class is designed to identify data streams.
 * In particular, the Stream object can be converted into one detector of the LIGO-virgo-KAGRA global network if the stream name is recognized as such.
 * In that case many specific functions are provided to access the detector properties.
 * @author Florent Robinet
 */
class Streams: public Sample{
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Streams class.
   * @details A stream is identified by its name which shall follow the stream name convention.
   * @sa SetName()
   * @param[in] aName Stream name.
   * @param[in] aVerbose Verbosity level.
   */
  Streams(const string aName, const unsigned int aVerbose=0);
  
  /**
   * @brief Destructor of the Streams class.
   */
  virtual ~Streams(void);
  /**
     @}
  */

  /**
   * @brief Sets a new name to the stream.
   * @details A stream is identified by its name which shall follow the stream name convention:
   * [prefix]:[suffix], where [prefix] is 2-characters string exactly.
   * @param[in] aName Stream name.
   */
  void SetName(const string aName);

  /**
   * @brief Returns the stream full name.
   */
  inline string GetName(void){ return Name; };

  /**
   * @brief Returns the stream name prefix.
   */
  inline string GetNamePrefix(void){ return NamePrefix; };

  /**
   * @brief Returns the stream name suffix.
   */
  inline string GetNameSuffix(void){ return NameSuffix; };

  /**
   * @brief Returns the stream name suffix with only underscores.
   * @details Dash characters are turned into underscore characters.
   */
  inline string GetNameSuffixUnderScore(void){ return NameSuffixUnderScore; };

  /**
   * @brief Returns the stream name using the LIGO-Virgo file name convention.
   */
  inline string GetNameConv(void){ return NamePrefix+"-"+NameSuffixUnderScore; };

  /**
   * @brief Returns the stream directory.
   * @details Convention: ./[name]
   *
   * Optionally, a root directory can be added: [aOutDir]/[name]
   * @param[in] aOutDir Root directory.
   */
  inline string GetDirectory(const string aOutDir="."){
    return aOutDir+"/"+Name;
  };
    
  /**
   * @brief Returns the stream trigger file name.
   * @param[in] aGpsStart GPS start [s].
   * @param[in] aDuration Duration [s].
   * @param[in] aFileFormat File format (extension).
   * @param[in] aProcessName Process name.
   * @param[in] aOutDir Output directory.
   */
  inline string GetTriggerFileName(const unsigned int aGpsStart, const unsigned int aDuration, const string aFileFormat="root", const string aProcessName="PROC", const string aOutDir="."){
    gwl_ss<<aOutDir<<"/"<<GetNameConv()<<"_"<<aProcessName<<"-"<<aGpsStart<<"-"<<aDuration<<"."<<aFileFormat;
    string ss_ret = gwl_ss.str();
    gwl_ss.clear(); gwl_ss.str("");
    return ss_ret;
  };
    
  /**
   * @brief Returns the detector index for this stream.
   */
  inline unsigned int GetDetectorIndex(void){ return DetIndex; };

  /**
   * @brief Returns the detector prefix for this stream.
   */
  inline string GetDetectorPrefix(void){ return DET_PREFIX[DetIndex]; };

  /**
   * @brief Computes antenna factors \f$F_+\f$ and \f$F_\times\f$.
   * @details Antenna factors are calculated for a source at a specified sky position, polarization angle, and sidereal time.
   * The implementation follows the formulae given in Anderson et al., PRD 63 042003 (2001)
   * @param[out] aFplus Returned \f$F_+\f$ value.
   * @param[out] aFcross Returned \f$F_\times\f$ value.
   * @param[in] aRa Right ascension [rad].
   * @param[in] aDec Declination [rad].
   * @param[in] aPsi Polarisation angle [rad].
   * @param[in] aGmst Sideral time [rad].
   */
  void GetDetectorAMResponse(double &aFplus, double &aFcross,
                             const double aRa, const double aDec,
                             const double aPsi, const double aGmst);
  
  /**
   * @brief Converts the geocentric time to the detector local time [s].
   * @details The wave propagation direction must be given by the right ascension and declination.
   * @param[in] aRa Right ascension [rad].
   * @param[in] aDec Declination [rad].
   * @param[in] aGeocentricTime Geocentric time [s].
   */
  inline double GetLocalTime(const double aRa, const double aDec, const double aGeocentricTime){
    return aGeocentricTime+TimeDelayFromEarthCenter(DetLoc, aRa, aDec, aGeocentricTime);
  };

  /**
   * @brief Returns the sky position of a GW signal when detected by two detectors.
   * @details When detected by two detectors, a gravitational-wave source can be located along a ring in the sky. The GW event is detected at time \f$t\f$ in this detector and at \f$t^\prime\f$ in another detector. We note \f$\delta t = t^\prime-t\f$ the gravitational-wave travel time between the two detectors. We parameterize the position on the ring using the angle \f$\omega\f$ which can take values between \f$0\f$ and \f$2\pi\f$.
   *
   * We define the reference frame \f$\cal{R}\f$ with an origin positioned at the vertex of this detector, \f$D\f$, and the axes \f$(x, y, z)\f$ oriented along the celestial axes. In this reference frame, the vertex of the second detector, \f$D^\prime\f$, is located at \f$D^\prime = (d_x,\, d_y,\, d_z)_{\cal{R}}\f$, or, in spherical coordinates \f$D^\prime = (d\sin\theta\cos\varphi,\, d\sin\theta\sin\varphi,\, d\cos\theta)_{\cal{R}}\f$, where \f$d\f$ is the distance between the two detectors.
   *
   * We define a second reference frame \f$\cal{R}^\prime\f$ with an origin positioned at the vertex of the other detector (\f$D^\prime\f$). The reference frame \f$\cal{R}^\prime\f$ is obtained after rotating \f$\cal{R}\f$ with the rotation matrix \f$R\f$. The rotation \f$R\f$ is a sequence of two extrinsic rotations: first a rotation about the \f$y\f$ axis with a \f$\theta\f$ angle, then a rotation about the \f$z\f$ axis with a \f$\varphi\f$ angle. The rotation matrix can be written as:
   * \f[
   R = 
   \left[ {\begin{array}{ccc}
   \cos\varphi\cos\theta  & \sin\varphi\cos\theta & -\sin\theta \\ 
   -\sin\varphi           & \cos\theta            & 0 \\ 
   \cos\varphi\sin\theta  & \sin\varphi\sin\theta & \cos\theta
   \end{array} } \right]
   \f]
   * The two reference frames are represented in the following figure. Solid lines (resp. dashed lines) are used to represent the axis of \f$\cal{R}\f$ (resp. \f$\cal{R}^\prime\f$).
   * \image html Streams/sky_ring.png "Sky ring associated to a two-detector position." width=400
   *
   * The unit vector pointing in the direction of the GW source is noted \f$\vec{u}_s=(\sin\theta_s\cos\varphi_s,\, \sin\theta_s\sin\varphi_s,\, \cos\theta_s)_{\cal{R}}\f$. The position of the source must therefore verify:
   * \f[
   \vec{u}_s\cdot\overrightarrow{DD^\prime} 
   = -c\delta t 
   = d\cos\gamma_s
   \f]
   * where \f$c\f$ is the speed of light in vacuum and \f$\gamma_s\f$ is the angle between the \f$\overrightarrow{DD^\prime}\f$ axis and the source vector \f$\vec{u}_s\f$. The source position is degenerated and can be anywhere along the ring defined by the cone with an opening angle \f$\gamma_s\f$. The angle \f$\gamma_s\f$ is completely determined by the previous equation, assuming the time delay \f$\delta t\f$ is known: \f$\gamma_s = \arccos(-c\delta t / d)\f$. In \f$\cal{R}^\prime\f$, the source vector \f$\vec{u}_s\f$ coordinates are \f$\vec{u}_s=(\sin\gamma_s\cos\omega_s,\, \sin\gamma_s\sin\omega_s,\, \cos\gamma_s)_{\cal{R}^\prime}\f$. The \f$\gamma_s\f$ angle is derived from the time delay and \f$\omega_s\f$ must be provided by the user.
   *
   * The rotation to move from \f$\cal{R}^\prime\f$ to \f$\cal{R}\f$ is described by the inverse of \f$R\f$. For a rotation matrix, we simply use the transpose matrix: \f$R^{-1} = R^T\f$:
   * \f[
   \left( {\begin{array}{c}
   \sin\theta_s\cos\varphi_s \\ 
   \sin\theta_s\sin\varphi_s \\ 
   \cos\theta_s
   \end{array} } \right)_{\cal{R}}
   = R^T \times
   \left( {\begin{array}{c}
   \sin\gamma_s\cos\omega_s \\ 
   \sin\gamma_s\sin\omega_s \\ 
   \cos\gamma_s
   \end{array} } \right)_{\cal{R}^\prime}
   \f]
   * Finally, the sideral time must be added to the \f$\varphi_s\f$ angle to account for the Earth roatation.
   * @param[out] aThetaS Source position in the Earth reference frame (celestial coordinates): polar angle \f$\theta_s\f$ [rad].
   * @param[out] aPhiS Source position in the Earth reference frame (celestial coordinates): azimuthal angle \f$\varphi_s\f$ [rad].
   * @param[in] aGmst Sideral time for this stream [rad].
   * @param[in] aDeltaT Time delay between detectors: \f$\delta t = t^\prime-t\f$ [s].
   * @param[in] aOmegaS Ring angle as defined in \f$\cal{R}^\prime\f$: \f$\omega_s\f$ [rad].
   * @param[in] aDprime Pointer to the other detector \f$D^\prime\f$.
   */
  bool GetSkyRingPoint(double &aThetaS, double &aPhiS, const double aGmst, const double aDeltaT, const double aOmegaS, Streams *aDprime);

  /**
   * @brief Returns the sky position of a GW signal when detected by two detectors (radec).
   * @sa GetSkyRingPoint() for a full description.
   * @param[out] aDecS Source position in the Earth reference frame (celestial coordinates): declination [rad].
   * @param[out] aRaS Source position in the Earth reference frame (celestial coordinates): right ascension [rad].
   * @param[in] aGmst Sideral time for this stream [rad].
   * @param[in] aDeltaT Time delay between detectors: \f$\delta t = t^\prime-t\f$ [s].
   * @param[in] aOmegaS Ring angle as defined in \f$\cal{R}^\prime\f$: \f$\omega_s\f$ [rad].
   * @param[in] aDprime Pointer to the other detector \f$D^\prime\f$.
   */
  inline bool GetSkyRingPointRaDec(double &aDecS, double &aRaS, const double aGmst, const double aDeltaT, const double aOmegaS, Streams *aDprime){
    if(GetSkyRingPoint(aDecS, aRaS, aGmst, aDeltaT, aOmegaS, aDprime)==false) return false;
    aDecS = TMath::Pi()/2.0 - aDecS;
    return true;
  };
  
  /**
   * @brief Returns the sky map of the detector antenna factor for a given time.
   * @details The returned histogram (declination vs right ascension) is filled with the antenna factor values: \f$\sqrt{F_{+}^2 + F_{\times}^2}\f$.
   * @note The user is in charge of deleting the returned histogram.
   * @param[in] aGpsTime GPS time [s].
   */
  TH2D* GetSkyAntennaFactor(const unsigned int aGpsTime);

protected:
  
  string Name;                 ///< Stream name.
  
private:
  
  unsigned int verbose;        ///< Verbosity level.
  string NamePrefix;           ///< Stream prefix.
  string NameSuffix;           ///< Stream suffix.
  string NameSuffixUnderScore; ///< Stream suffix with only underscores.
  unsigned int DetIndex;       ///< Detector index.
  double Response[3][3];       ///< Detector response.
  double DetLoc[3];            ///< Detector location (vertex) on Earth.

  /**
   * @brief Associates detector's properties to the stream (based on prefix).
   */
  void MakeDetector(void);

  /**
   * @brief Computes the detector spatial response matrix.
   */
  void ComputeDetectorResponse(void);

  ClassDef(Streams,0)
};

#endif


