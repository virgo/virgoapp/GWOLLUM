/**
 * @file 
 * @brief Program to print the segments of trigger files.
 * @snippet this gwl-trigger-merge-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */

#include "ReadTriggers.h"
#include "MakeTriggers.h"

using namespace std;

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return GwlPrintVersion();
  }

  // check the argument
  if(argc!=3){
    //! [gwl-trigger-merge-usage]
    cerr<<"This program merges multiple trigger files (ROOT) into one."<<endl;
    cerr<<endl;
    cerr<<"usage:"<<endl;
    cerr<<argv[0]<<" [outdir] [ROOT trigger files]"<<endl; 
    cerr<<endl;
    cerr<<"  [outdir]: output directory"<<endl; 
    cerr<<"  [ROOT trigger files]: list of ROOT trigger files (pattern)"<<endl; 
    cerr<<""<<endl;
    //! [gwl-trigger-merge-usage]
    return 1;
  }

  // outdir
  string outdir = (string)argv[1];
  error_code ec;
  if(!CreateDirectory(ec, outdir)){
    cout<<"Cannot create output directory ("<<ec<<")"<<endl;
    return 2;
  }
  
  // input files
  string infiles = (string)argv[2];

  // extract segments
  ReadTriggerMetaData *meta = new ReadTriggerMetaData(infiles, "", 4);
  if(!meta->Merge(outdir).compare("")) return 1;
  
  delete meta;
  
  return 0;
}

