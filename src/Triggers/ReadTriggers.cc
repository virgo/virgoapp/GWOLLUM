/**
 * @file 
 * @brief See ReadTriggers.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "ReadTriggers.h"

ClassImp(ReadTriggers)

////////////////////////////////////////////////////////////////////////////////////
ReadTriggers::ReadTriggers(const string aPattern, const string aDirectory,
                           const unsigned int aVerbose):
ReadTriggerMetaData(aPattern, aDirectory, aVerbose), Triggers(aVerbose) { 
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"ReadTriggers::"<<__func__<<endl;
  Triggers::ReadTriggerFiles(aPattern, aDirectory);
}

////////////////////////////////////////////////////////////////////////////////////
ReadTriggers::~ReadTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"ReadTriggers::"<<__func__<<endl;
}
