/**
 * @file 
 * @brief See TriggerSelect.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "TriggerPlot.h"

ClassImp(TriggerPlot)

////////////////////////////////////////////////////////////////////////////////////
TriggerPlot::TriggerPlot(const unsigned int aNcoll, const string aPattern, const string aDirectory,
                         const string aStyleName, const unsigned int aVerbose):
GwollumPlot("TP",aStyleName),
  ReadTriggers(aPattern, aDirectory, aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;

  Ncoll = aNcoll;
  plotstar = true;
  rate_min = 0.0;
  rate_max = 0.0;

  // allocate memory (selection)
  Collection = new TriggerSelect* [Ncoll];
  legend = new string [Ncoll];
  Hfreqtime = new TH2D* [Ncoll];
  Gfreqtime = new TGraph* [Ncoll];
  Hsnrtime = new TH2D* [Ncoll];
  Gsnrtime = new TGraph* [Ncoll];
  Hsnrfreq = new TH2D* [Ncoll];
  Gsnrfreq = new TGraph* [Ncoll];
  Htime = new TH1D* [Ncoll];
  Hfreq = new TH1D* [Ncoll];
  Hsnr = new TH1D* [Ncoll];
  Gfreqtimeloud = new TGraph(1);
  Gfreqtimeloud2 = new TGraph(1);

  // init
  for(unsigned int p=0; p<Ncoll; p++){
    Collection[p] = new TriggerSelect(this, Triggers::mon->GetVerbosityLevel());
    Hfreqtime[p] = NULL;
    Gfreqtime[p] = NULL;
    Hsnrtime[p] = NULL;
    Gsnrtime[p] = NULL;
    Hsnrfreq[p] = NULL;
    Gsnrfreq[p] = NULL;
    Htime[p] = NULL;
    Hfreq[p] = NULL;
    Hsnr[p]  = NULL;
    ResetParameters(p);
  }

  // loudest event (star)
  Gfreqtimeloud->SetMarkerStyle(29);
  Gfreqtimeloud->SetMarkerSize(2);
  Gfreqtimeloud->SetMarkerColor(5);
  Gfreqtimeloud2->SetMarkerStyle(30);
  Gfreqtimeloud2->SetMarkerSize(2);
  Gfreqtimeloud2->SetMarkerColor(1);

  // segment boxes
  //SegBox = new TPave(0.1,0.905,0.84,0.925,0, "NDC");
  SegBox = new TPave(0.12,0.905,0.842,0.925,0, "NDC");
  SegBox->SetFillColor(2);
  nonbox=0;
  OnBox = NULL;
  nselbox=0;
  SelBox = NULL;

  // selection segments
  selseg = NULL;
  
}

////////////////////////////////////////////////////////////////////////////////////
TriggerPlot::~TriggerPlot(void){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  delete [] legend;
  for(unsigned int p=0; p<Ncoll; p++){
    delete Hfreqtime[p];
    delete Gfreqtime[p];
    delete Hsnrtime[p];
    delete Gsnrtime[p];
    delete Hsnrfreq[p];
    delete Gsnrfreq[p];
    delete Htime[p];
    delete Hfreq[p];
    delete Hsnr[p];
    delete Collection[p];
  }
  delete [] Hfreqtime;
  delete [] Gfreqtime;
  delete Gfreqtimeloud;
  delete Gfreqtimeloud2;
  delete [] Hsnrtime;
  delete [] Gsnrtime;
  delete [] Hsnrfreq;
  delete [] Gsnrfreq;
  delete [] Htime;
  delete [] Hfreq;
  delete [] Hsnr;
  delete [] Collection;
  delete SegBox;
  for(unsigned int i=0; i<nonbox; i++) delete OnBox[i];
  delete [] OnBox;
  for(unsigned int i=0; i<nselbox; i++) delete SelBox[i];
  delete [] SelBox;
}


////////////////////////////////////////////////////////////////////////////////////
void TriggerPlot::MakeCollection(const int aCollIndex, Segments *aInSeg){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  double collectiontmin;
  double collectiontmax;
  double collectionfmin;
  double collectionfmax;
  double collectionsmin;
  double collectionsmax;
  double tmax=-1.0;
  double fmax=-1.0;
  double smax=-1.0;
  unsigned int istart;
  unsigned int istop;

  // range of collections
  if((aCollIndex<0) || (aCollIndex>=(int)Ncoll)){
    istart=0;
    istop=Ncoll;
  }
  else{
    istart=(unsigned int)aCollIndex;
    istop=(unsigned int)aCollIndex + 1;
  }

  // loop over collections
  for(unsigned int i=istart; i<istop; i++){

    // reset
    ResetCollection(i);
    Triggers::mon->SetMessageType(monitor_info_3);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": make collection #"<<i<<endl;

    // selection
    collectiontmin=Collection[i]->GetTimeMin();
    collectiontmax=Collection[i]->GetTimeMax();
    collectionfmin=Collection[i]->GetFrequencyMin();
    collectionfmax=Collection[i]->GetFrequencyMax();
    collectionsmin=Collection[i]->GetSnrMin();
    collectionsmax=Collection[i]->GetSnrMax();
    
    // use clusters
    if(Collection[i]->GetClusterTag()>=0){

      for(unsigned int c=0; c<Triggers::GetClusterN(); c++){

	// apply cluster selection
        if(Triggers::GetClusterTag(c)!=Collection[i]->GetClusterTag()) continue;
	if(Triggers::GetClusterTime(c)<collectiontmin) continue;
	if(Triggers::GetClusterTime(c)>=collectiontmax) continue;
	if(Triggers::GetClusterFrequency(c)<collectionfmin) continue;
	if(Triggers::GetClusterFrequency(c)>=collectionfmax) continue;
	if(Triggers::GetClusterSnr(c)<collectionsmin) continue;
	if(Triggers::GetClusterSnr(c)>=collectionsmax) continue;
	if(Triggers::GetClusterTimeStart(c)>=collectiontmax) break;
	if(selseg!=NULL){
	  if(!selseg->IsInsideSegment(Triggers::GetClusterTime(c))) continue;
	}

        // fill plots
	Gfreqtime[i]->SetPoint(Gfreqtime[i]->GetN(), Triggers::GetClusterTime(c), Triggers::GetClusterFrequency(c));
	Gsnrtime[i]->SetPoint(Gsnrtime[i]->GetN(), Triggers::GetClusterTime(c), Triggers::GetClusterSnr(c));
	Gsnrfreq[i]->SetPoint(Gsnrfreq[i]->GetN(), Triggers::GetClusterFrequency(c), Triggers::GetClusterSnr(c));
	Htime[i]->Fill(Triggers::GetClusterTime(c));
	Hfreq[i]->Fill(Triggers::GetClusterFrequency(c));
	Hsnr[i]->Fill(Triggers::GetClusterSnr(c));

        // loudest event
	if(Triggers::GetClusterSnr(c)>smax){
          tmax=Triggers::GetClusterTime(c);
          fmax=Triggers::GetClusterFrequency(c);
          smax=Triggers::GetClusterSnr(c);
        }
      }
    }
    
    // triggers
    else{
      for(Long64_t t=0; t<Ttree->GetEntries(); t++){
	Ttree->GetEntry(t);
	
	// apply selection
	if(Ttime<collectiontmin) continue;
	if(Ttime>=collectiontmax) continue;
	if(Tfreq<collectionfmin) continue;
	if(Tfreq>=collectionfmax) continue;
	if(Tsnr<collectionsmin) continue;
	if(Tsnr>=collectionsmax) continue;
	if(Ttstart>=collectiontmax) break;
	if(selseg!=NULL){
	  if(!selseg->IsInsideSegment(Ttime)) continue;
	}

        // fill plots
	Gfreqtime[i]->SetPoint(Gfreqtime[i]->GetN(), Ttime, Tfreq);
	Gsnrtime[i]->SetPoint(Gsnrtime[i]->GetN(), Ttime, Tsnr);
	Gsnrfreq[i]->SetPoint(Gsnrfreq[i]->GetN(), Tfreq, Tsnr);
	Htime[i]->Fill(Ttime);
	Hfreq[i]->Fill(Tfreq);
	Hsnr[i]->Fill(Tsnr);

        // loudest event
	if(Tsnr>smax){
          tmax=Ttime;
          fmax=Tfreq;
          smax=Tsnr;
        }
      }
    }
  
    // time -> rate
    double bintime;
    for(int b=1; b<=Htime[i]->GetNbinsX(); b++){
      bintime=ReadTriggers::GetLiveTime(Htime[i]->GetXaxis()->GetBinLowEdge(b),
                                        Htime[i]->GetXaxis()->GetBinUpEdge(b));
      if(bintime) Htime[i]->SetBinContent(b, Htime[i]->GetBinContent(b)/bintime);
      else Htime[i]->SetBinContent(b, 0.0);
    }
    
  }

  // time box (ON)
  SegBox->ConvertNDCtoPad();
  for(unsigned int j=0; j<nonbox; j++) delete OnBox[j];
  delete [] OnBox;
  Segments *OnSeg = new Segments(Collection[istart]->GetTimeMin(), Collection[istart]->GetTimeMax());
  if(aInSeg!=NULL&&aInSeg->GetStatus()) OnSeg->Intersect(aInSeg);
  OnSeg->Intersect(this);
  nonbox = OnSeg->GetN();
  OnBox = new TPave* [nonbox];
  SegBox->ConvertNDCtoPad();
  for(unsigned int s=0; s<nonbox; s++){
    OnBox[s] = new TPave(SegBox->GetX1()+(SegBox->GetX2()-SegBox->GetX1())/(Collection[istart]->GetTimeMax()-Collection[istart]->GetTimeMin())*(OnSeg->GetStart(s)-Collection[istart]->GetTimeMin()),
                         SegBox->GetY1(),
                         SegBox->GetX1()+(SegBox->GetX2()-SegBox->GetX1())/(Collection[istart]->GetTimeMax()-Collection[istart]->GetTimeMin())*(OnSeg->GetEnd(s)-Collection[istart]->GetTimeMin()),
                         SegBox->GetY2(),
                         0, "NDC");
    OnBox[s]->SetFillColor(3);// green
  }
  delete OnSeg;
  
  // time box (SEL)
  for(unsigned int j=0; j<nselbox; j++) delete SelBox[j];
  delete [] SelBox;
  nselbox = 0; SelBox = new TPave* [nselbox];
  if(selseg!=NULL&&selseg->GetStatus()){
    OnSeg = new Segments(Collection[istart]->GetTimeMin(), Collection[istart]->GetTimeMax());
    if(aInSeg!=NULL&&aInSeg->GetStatus()) OnSeg->Intersect(aInSeg);
    OnSeg->Intersect(selseg);
    OnSeg->Intersect(this);
    nselbox=OnSeg->GetN();
    delete [] SelBox;
    SelBox = new TPave* [nselbox];
    for(unsigned int s=0; s<nselbox; s++){
      SelBox[s]= new TPave(SegBox->GetX1()+(SegBox->GetX2()-SegBox->GetX1())/(Collection[istart]->GetTimeMax()-Collection[istart]->GetTimeMin())*(OnSeg->GetStart(s)-Collection[istart]->GetTimeMin()),
                           SegBox->GetY1(),
                           SegBox->GetX1()+(SegBox->GetX2()-SegBox->GetX1())/(Collection[istart]->GetTimeMax()-Collection[istart]->GetTimeMin())*(OnSeg->GetEnd(s)-Collection[istart]->GetTimeMin()),
                           SegBox->GetY2(),
                           0, "NDC");
      SelBox[s]->SetFillColor(kGreen+3);// dark green
    }
    delete OnSeg;
  }
  
  // loudest plot
  Gfreqtimeloud->RemovePoint(0);
  Gfreqtimeloud2->RemovePoint(0);
  if(smax>0.0){
    Gfreqtimeloud->SetPoint(0, tmax, fmax);
    Gfreqtimeloud2->SetPoint(0, tmax, fmax);
  }
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerPlot::PrintPlot(const string aPlotType,
                            const vector<unsigned int> aCollIndex,
                            const bool aFirst){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // reset pad
  if(aFirst){
    GwollumPlot::ResetLegend();
    GwollumPlot::Wpad->Clear();
  }

  for(unsigned int c=0; c<aCollIndex.size(); c++){
    if(aCollIndex[c]>=Ncoll) continue;

    GwollumPlot::AddText("",0.01,0.01,0.04);

    //******* FREQTIME
    if(!aPlotType.compare("freqtime")){
      Triggers::mon->SetMessageType(monitor_info_3);
      *Triggers::mon<<"TriggerPlot::"<<__func__<<": make freqtime for collection #"<<c<<endl;

      // first
      if(c==0){
        // plot container
	if(aFirst) GwollumPlot::Draw(Hfreqtime[aCollIndex[c]], "");
	GwollumPlot::SetGridx(1); GwollumPlot::SetGridy(1);
        GwollumPlot::SetLogx(0); GwollumPlot::SetLogy(1);

        // draw segment boxes
        GwollumPlot::Draw(SegBox,"same");
        for(unsigned int i=0; i<nonbox; i++) GwollumPlot::Draw(OnBox[i],"same");
        for(unsigned int i=0; i<nselbox; i++) GwollumPlot::Draw(SelBox[i],"same");
      }
      
      if(Gfreqtime[aCollIndex[c]]->GetN()) GwollumPlot::Draw(Gfreqtime[aCollIndex[c]],"Psame");
      if(plotstar&&c==aCollIndex.size()-1){
	GwollumPlot::Draw(Gfreqtimeloud,"Psame");
	GwollumPlot::Draw(Gfreqtimeloud2,"Psame");
      }
      GwollumPlot::AddLegendEntry(Gfreqtime[aCollIndex[c]], legend[aCollIndex[c]], "P");
      GwollumPlot::UpdateText("Start: "+Collection[aCollIndex[c]]->GetTimeStamp());
    }

    //******* SNRTIME
    else if(!aPlotType.compare("snrtime")){
      Triggers::mon->SetMessageType(monitor_info_3);
      *Triggers::mon<<"TriggerPlot::"<<__func__<<": make snrtime for collection #"<<c<<endl;

      // first
      if(c==0){
        // plot container
	if(aFirst) GwollumPlot::Draw(Hsnrtime[aCollIndex[c]], "");
	GwollumPlot::SetGridx(1); GwollumPlot::SetGridy(1);
        GwollumPlot::SetLogx(0); GwollumPlot::SetLogy(1);

        // draw segment boxes
        GwollumPlot::Draw(SegBox,"same");
        for(unsigned int i=0; i<nonbox; i++) GwollumPlot::Draw(OnBox[i],"same");
        for(unsigned int i=0; i<nselbox; i++) GwollumPlot::Draw(SelBox[i],"same");
      }
      
      if(Gsnrtime[aCollIndex[c]]->GetN()) GwollumPlot::Draw(Gsnrtime[aCollIndex[c]],"Psame");
      GwollumPlot::AddLegendEntry(Gsnrtime[aCollIndex[c]], legend[aCollIndex[c]], "P");
      GwollumPlot::UpdateText("Start: "+Collection[aCollIndex[c]]->GetTimeStamp());
    }

    //******* SNRFREQ
    else if(!aPlotType.compare("snrfreq")){
      Triggers::mon->SetMessageType(monitor_info_3);
      *Triggers::mon<<"TriggerPlot::"<<__func__<<": make snrfreq for collection #"<<c<<endl;

      // first
      if(c==0){
        // plot container
	if(aFirst) GwollumPlot::Draw(Hsnrfreq[aCollIndex[c]], "");
	GwollumPlot::SetGridx(1); GwollumPlot::SetGridy(1);
        GwollumPlot::SetLogx(1); GwollumPlot::SetLogy(1);
      }
      
      if(Gsnrfreq[aCollIndex[c]]->GetN()) GwollumPlot::Draw(Gsnrfreq[aCollIndex[c]],"Psame");
      GwollumPlot::AddLegendEntry(Gsnrfreq[aCollIndex[c]], legend[aCollIndex[c]], "P");
      GwollumPlot::UpdateText("");
    }

    //******* RATE
    else if(!aPlotType.compare("rate")){
      Triggers::mon->SetMessageType(monitor_info_3);
      *Triggers::mon<<"TriggerPlot::"<<__func__<<": make rate for collection #"<<c<<endl;

      // set rate range
      if((rate_min>0.0)&&(rate_max>rate_min))
        Htime[aCollIndex[0]]->GetYaxis()->SetRangeUser(rate_min, rate_max);
      else if(rate_min>0.0)
        Htime[aCollIndex[0]]->GetYaxis()->SetRangeUser(rate_min, TMath::Max(2.0*rate_min, TMath::Max(Htime[aCollIndex[0]]->GetMaximum(), 1.1*Htime[aCollIndex[c]]->GetMaximum())));
      else if(rate_max>0.0)
        Htime[aCollIndex[0]]->GetYaxis()->SetRangeUser(TMath::Min(rate_max/2.0, TMath::Min(Htime[aCollIndex[0]]->GetMinimum(0), 0.9*Htime[aCollIndex[c]]->GetMinimum(0))), rate_max);
      else{
        if(c>0) Htime[aCollIndex[0]]->GetYaxis()->SetRangeUser(TMath::Min(Htime[aCollIndex[0]]->GetMinimum(0), 0.9*Htime[aCollIndex[c]]->GetMinimum(0)),
                                                               TMath::Max(Htime[aCollIndex[0]]->GetMaximum(), 1.1*Htime[aCollIndex[c]]->GetMaximum()));
        else Htime[aCollIndex[0]]->GetYaxis()->SetRangeUser(0.9*Htime[aCollIndex[c]]->GetMinimum(0),
                                                            1.1*Htime[aCollIndex[c]]->GetMaximum());
      }
      // first
      if(c==0){
        // plot container
	if(aFirst)
          GwollumPlot::Draw(Htime[aCollIndex[c]], "P");
	else
          GwollumPlot::Draw(Htime[aCollIndex[c]],"Psame");
        GwollumPlot::SetGridx(1); GwollumPlot::SetGridy(1);
        GwollumPlot::SetLogx(0);
        if(Htime[aCollIndex[0]]->GetMinimum()>0.0) GwollumPlot::SetLogy(1);
        else GwollumPlot::SetLogy(0);

        // draw segment boxes
        GwollumPlot::Draw(SegBox,"same");
        for(unsigned int i=0; i<nonbox; i++) GwollumPlot::Draw(OnBox[i],"same");
        for(unsigned int i=0; i<nselbox; i++) GwollumPlot::Draw(SelBox[i],"same");
      }
      else
        GwollumPlot::Draw(Htime[aCollIndex[c]],"Psame");
      GwollumPlot::AddLegendEntry(Htime[aCollIndex[c]], legend[aCollIndex[c]], "P");
      GwollumPlot::UpdateText("Start: "+Collection[aCollIndex[c]]->GetTimeStamp());
    }

    //******* FREQUENCY
    else if(!aPlotType.compare("frequency")){
      Triggers::mon->SetMessageType(monitor_info_3);
      *Triggers::mon<<"TriggerPlot::"<<__func__<<": make frequency for collection #"<<c<<endl;

      // first
      if(c==0){
        // plot container
	if(aFirst) GwollumPlot::Draw(Hfreq[aCollIndex[c]], "");
	else GwollumPlot::Draw(Hfreq[aCollIndex[c]],"same");
	GwollumPlot::SetGridx(1); GwollumPlot::SetGridy(1);
        GwollumPlot::SetLogx(1); GwollumPlot::SetLogy(1);
      }
      else
        GwollumPlot::Draw(Hfreq[aCollIndex[c]],"same");
      GwollumPlot::AddLegendEntry(Hfreq[aCollIndex[c]], legend[aCollIndex[c]], "L");
      GwollumPlot::UpdateText("");
    }

    //******* SNR
    else if(!aPlotType.compare("snr")){
      Triggers::mon->SetMessageType(monitor_info_3);
      *Triggers::mon<<"TriggerPlot::"<<__func__<<": make snr for collection #"<<c<<endl;

      // first
      if(c==0){
        // plot container
	if(aFirst) GwollumPlot::Draw(Hsnr[aCollIndex[c]], "");
	else GwollumPlot::Draw(Hsnr[aCollIndex[c]],"same");
	GwollumPlot::SetGridx(1); GwollumPlot::SetGridy(1);
        GwollumPlot::SetLogx(1); GwollumPlot::SetLogy(1);
      }
      
      GwollumPlot::Draw(Hsnr[aCollIndex[c]],"same");
      GwollumPlot::AddLegendEntry(Hsnr[aCollIndex[c]], legend[aCollIndex[c]], "L");
      GwollumPlot::UpdateText("");
    }
    else{};
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerPlot::SetSelectionSegments(Segments *SelectionSegments){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // no selection
  if(SelectionSegments==NULL || !SelectionSegments->GetStatus()){
    selseg = NULL;
    return;
  }

  // new segments
  selseg = SelectionSegments;
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
TH1D* TriggerPlot::GetTH1D(const string aPlotType, const unsigned int aCollIndex){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  if(!aPlotType.compare("rate")) return (TH1D*)(Htime[aCollIndex]->Clone());
  if(!aPlotType.compare("snr")) return (TH1D*)(Hsnr[aCollIndex]->Clone());
  if(!aPlotType.compare("frequency")) return (TH1D*)(Hfreq[aCollIndex]->Clone());
  return (TH1D*)(Htime[aCollIndex]->Clone());
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* TriggerPlot::GetTGraph(TH1D** aFrame, const string aPlotType, const unsigned int aCollIndex){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  if(!aPlotType.compare("freqtime")){
    *aFrame = (TH1D*)(Hfreqtime[aCollIndex]->Clone());
    return (TGraph*)(Gfreqtime[aCollIndex]->Clone());
  }
  if(!aPlotType.compare("snrtime")){
    *aFrame = (TH1D*)(Hsnrtime[aCollIndex]->Clone());
    return (TGraph*)(Gsnrtime[aCollIndex]->Clone());
  }
  if(!aPlotType.compare("snrfreq")){
    *aFrame = (TH1D*)(Hsnrfreq[aCollIndex]->Clone());
    return (TGraph*)(Gsnrfreq[aCollIndex]->Clone());
  }

  *aFrame = (TH1D*)(Hfreqtime[aCollIndex]->Clone());
  return (TGraph*)(Gfreqtime[aCollIndex]->Clone());
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerPlot::ResetCollection(const unsigned int aCollIndex){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // get current style
  Color_t color = gStyle->GetMarkerColor();
  Size_t msize = gStyle->GetMarkerSize();
  Width_t lsize = gStyle->GetLineWidth();
  Style_t mstyle = gStyle->GetMarkerStyle();
  if(Hfreqtime[aCollIndex]!=NULL){
    color= Hfreqtime[aCollIndex]->GetMarkerColor();
    msize = Hfreqtime[aCollIndex]->GetMarkerSize();
    lsize = Hfreqtime[aCollIndex]->GetLineWidth();
    mstyle = Hfreqtime[aCollIndex]->GetMarkerStyle();
  }
  //********************************************************************************
  //                                 FREQTIME
  //********************************************************************************
  Triggers::mon->SetMessageType(monitor_info_3);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<": reset freqtime plot"<<endl;
  delete Hfreqtime[aCollIndex];
  Hfreqtime[aCollIndex] = Collection[aCollIndex]->GetTH2D("Time", "Frequency");
  delete Gfreqtime[aCollIndex];
  Gfreqtime[aCollIndex] = new TGraph();

  gwl_ss<<"Hfreqtime_"<<aCollIndex<<"_"<<Triggers::srandid;
  Hfreqtime[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
  gwl_ss<<"Gfreqtime_"<<aCollIndex<<"_"<<Triggers::srandid;
  Gfreqtime[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();

  // style
  Hfreqtime[aCollIndex]->SetMarkerStyle(mstyle);
  Hfreqtime[aCollIndex]->SetMarkerColor(color);
  Hfreqtime[aCollIndex]->SetMarkerSize(msize);
  Hfreqtime[aCollIndex]->SetLineColor(color);
  Hfreqtime[aCollIndex]->SetLineWidth(lsize);
  Gfreqtime[aCollIndex]->SetMarkerStyle(mstyle);
  Gfreqtime[aCollIndex]->SetMarkerColor(color);
  Gfreqtime[aCollIndex]->SetMarkerSize(msize);
  Gfreqtime[aCollIndex]->SetLineColor(color);
  Gfreqtime[aCollIndex]->SetLineWidth(lsize);

  //********************************************************************************
  //                                  SNRTIME
  //********************************************************************************
  Triggers::mon->SetMessageType(monitor_info_3);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<": reset snrtime plot"<<endl;
  delete Hsnrtime[aCollIndex];
  Hsnrtime[aCollIndex] = Collection[aCollIndex]->GetTH2D("Time", "SNR");
  delete Gsnrtime[aCollIndex];
  Gsnrtime[aCollIndex]=new TGraph();

  gwl_ss<<"Hsnrtime_"<<aCollIndex<<"_"<<Triggers::srandid;
  Hsnrtime[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
  gwl_ss<<"Gsnrtime_"<<aCollIndex<<"_"<<Triggers::srandid;
  Gsnrtime[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();

  // style
  Hsnrtime[aCollIndex]->SetMarkerStyle(mstyle);
  Hsnrtime[aCollIndex]->SetMarkerColor(color);
  Hsnrtime[aCollIndex]->SetMarkerSize(msize);
  Hsnrtime[aCollIndex]->SetLineColor(color);
  Hsnrtime[aCollIndex]->SetLineWidth(lsize);
  Gsnrtime[aCollIndex]->SetMarkerStyle(mstyle);
  Gsnrtime[aCollIndex]->SetMarkerColor(color);
  Gsnrtime[aCollIndex]->SetMarkerSize(msize);
  Gsnrtime[aCollIndex]->SetLineColor(color);
  Gsnrtime[aCollIndex]->SetLineWidth(lsize);

  //********************************************************************************
  //                                  SNRFREQ
  //********************************************************************************
  Triggers::mon->SetMessageType(monitor_info_3);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<": reset snrfreq plot"<<endl;
  delete Hsnrfreq[aCollIndex];
  Hsnrfreq[aCollIndex] = Collection[aCollIndex]->GetTH2D("Frequency", "SNR");
  delete Gsnrfreq[aCollIndex];
  Gsnrfreq[aCollIndex]=new TGraph();

  gwl_ss<<"Hsnrfreq_"<<aCollIndex<<"_"<<Triggers::srandid;
  Hsnrfreq[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
  gwl_ss<<"Gsnrfreq_"<<aCollIndex<<"_"<<Triggers::srandid;
  Gsnrfreq[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();

  // style
  Hsnrfreq[aCollIndex]->SetMarkerStyle(mstyle);
  Hsnrfreq[aCollIndex]->SetMarkerColor(color);
  Hsnrfreq[aCollIndex]->SetMarkerSize(msize);
  Hsnrfreq[aCollIndex]->SetLineColor(color);
  Hsnrfreq[aCollIndex]->SetLineWidth(lsize);
  Gsnrfreq[aCollIndex]->SetMarkerStyle(mstyle);
  Gsnrfreq[aCollIndex]->SetMarkerColor(color);
  Gsnrfreq[aCollIndex]->SetMarkerSize(msize);
  Gsnrfreq[aCollIndex]->SetLineColor(color);
  Gsnrfreq[aCollIndex]->SetLineWidth(lsize);
  
  //********************************************************************************
  //                                    TIME
  //********************************************************************************
  Triggers::mon->SetMessageType(monitor_info_3);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<": reset time plot"<<endl;
  delete Htime[aCollIndex];
  Htime[aCollIndex] = Collection[aCollIndex]->GetTH1D("Time");

  gwl_ss<<"Htime_"<<aCollIndex<<"_"<<Triggers::srandid;
  Htime[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
  
  // style
  gwl_ss<<Htime[aCollIndex]->GetXaxis()->GetBinWidth(1);
  Htime[aCollIndex]->GetYaxis()->SetTitle(("Rate [Hz] (avg over "+gwl_ss.str()+" s)").c_str());
  gwl_ss.str(""); gwl_ss.clear();
  Htime[aCollIndex]->SetMarkerStyle(mstyle);
  Htime[aCollIndex]->SetMarkerColor(color);
  Htime[aCollIndex]->SetMarkerSize(msize);
  Htime[aCollIndex]->SetLineColor(color);
  Htime[aCollIndex]->SetLineWidth(lsize);

  //********************************************************************************
  //                                    FREQUENCY
  //********************************************************************************
  Triggers::mon->SetMessageType(monitor_info_3);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<": reset frequency plot"<<endl;
  delete Hfreq[aCollIndex];
  Hfreq[aCollIndex] = Collection[aCollIndex]->GetTH1D("Frequency");

  gwl_ss<<"Hfreq_"<<aCollIndex<<"_"<<Triggers::srandid;
  Hfreq[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();

  // style
  Hfreq[aCollIndex]->SetMarkerStyle(mstyle);
  Hfreq[aCollIndex]->SetMarkerColor(color);
  Hfreq[aCollIndex]->SetMarkerSize(msize);
  Hfreq[aCollIndex]->SetLineColor(color);
  Hfreq[aCollIndex]->SetLineWidth(lsize);
  
  //********************************************************************************
  //                                    SNR
  //********************************************************************************
  Triggers::mon->SetMessageType(monitor_info_3);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<": reset snr plot"<<endl;
  delete Hsnr[aCollIndex];
  Hsnr[aCollIndex] = Collection[aCollIndex]->GetTH1D("SNR");

  gwl_ss<<"Hsnr_"<<aCollIndex<<"_"<<Triggers::srandid;
  Hsnr[aCollIndex]->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
  
  // style
  Hsnr[aCollIndex]->SetMarkerStyle(mstyle);
  Hsnr[aCollIndex]->SetMarkerColor(color);
  Hsnr[aCollIndex]->SetMarkerSize(msize);
  Hsnr[aCollIndex]->SetLineColor(color);
  Hsnr[aCollIndex]->SetLineWidth(lsize);
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void TriggerPlot::ResetParameters(const unsigned int aCollIndex){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"TriggerPlot::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"TriggerPlot::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // back to default trigger selection
  Collection[aCollIndex]->ResetSelection();

  // set resolution
  Collection[aCollIndex]->SetTimeResolution(TMath::Min(TMath::Max((Long64_t)1,GetTriggerN()/(Long64_t)500),(Long64_t)100));
  Collection[aCollIndex]->SetFrequencyResolution(TMath::Min(TMath::Max((Long64_t)1,GetTriggerN()/(Long64_t)500),(Long64_t)100));
  Collection[aCollIndex]->SetSnrResolution(TMath::Min(TMath::Max((Long64_t)1,GetTriggerN()/(Long64_t)500),(Long64_t)100));

  plotstar = false;

  // style
  gwl_ss<<aCollIndex;
  legend[aCollIndex] = "Collection #"+gwl_ss.str();
  gwl_ss.str(""); gwl_ss.clear();

  return ResetCollection(aCollIndex);
}
