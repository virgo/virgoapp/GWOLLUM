/**
 * @file 
 * @brief Write triggers to disk.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">fl
orent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __MakeTriggers__
#define __MakeTriggers__

#include "GwollumOptions.h"
#include "Streams.h"
#include "Triggers.h"

using namespace std;

/**
 * @brief Write triggers to disk.
 * @details This class is designed to write triggers in a ROOT tree (other supported formats: hdf5, txt).
 * When triggers are produced by some external algorithms, the MakeTriggers class can be used to manage output triggers and to save them to disk.
 *
 * The ROOT files contains multiple TTree:
 * - "triggers": this is the TTree with the trigger parameters (Triggers)
 * - "metadata": this tree contains a single entry with trigger metadata.
 * - "segments": this tree saves the list of segments.
 * - "gwloptions": list of GWOLLUM options recorded with GwollumOptions.
 * 
 * The user is responsible to fill both the Segments and the Triggers structure from which this class inherits.
 * @author Florent Robinet
 */
class MakeTriggers: public GwollumOptions, public Segments, public Streams, public Triggers {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the MakeTriggers class.
   * @details The different structures are initialized:
   * - Streams object
   * - Triggers object
   * - Segments object
   * - GwollumOptions object
   *
   * @param[in] aStreamName Stream name.
   * @param[in] aVerbose Verbosity level.
   */
  MakeTriggers(const string aStreamName, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the MakeTriggers class.
   */
  virtual ~MakeTriggers(void);
  /**
     @}
  */
  
  /**
   * @brief Returns the object status.
   * @details Combines Triggers::GetStatus(), Segments::GetStatus(), and Streams::GetStatus(). 
   */
  inline bool GetStatus(void){
    return Triggers::GetStatus()&&Segments::GetStatus()&&Streams::GetStatus();
  };
  
  /**
   * @brief Resets all.
   * @details The memory is cleaned up:
   * - triggers/clusters (Triggers::Reset())
   * - segments (Segments::Reset())
   * - metadata
   *
   * @note GwollumOptions are preserved.
   */
  void Reset(void);
   
  /**
   * @brief Writes triggers to disk.
   * @details This function writes all the triggers added until now in an output file as well as the segments and metadata.
   * The MakeTriggers object is then completely reset (see MakeTriggers::Reset()).
   *
   * @returns The output file path is returned (last file). An empty string is returned if this function fails in some way.
   *
   * Several output formats are supported:
   * - 'root': standard GWOLLUM root trigger file
   * - 'hdf5': hdf5 format
   * - 'txt': text file with columns
   *
   * Multiple output formats can be requested at once. Use a single string to list the requested formats.
   * @note For the hdf5 and txt formats, if triggers were clustered, only clusters are saved. For the root format, only triggers are saved.
   * @warning If options were added with GwollumOptions, the GwollumOptions::FillTree() function must be called first (and only once)! Otherwise options will not be saved (empty tree).
   *
   * The optional argument 'aFileName' is used to bypass the file naming convention of GWOLLUM. With this option, the trigger file will be given a new name: [aFileName].[format]
   *
   * For ROOT files, it is possible to write the trees in a ROOT sub-directory. Use the optional argument 'aSubDir'. Only in this case, the trigger file can be updated when adding several directories. To do this, use aWriteMode="UPDATE".
   * @param[in] aOutDir Output directory (must exist).
   * @param[in] aFileFormat List of file formats.
   * @param[in] aFileName New name for ouput file. Use "" for GWOLLUM convention.
   * @param[in] aSubDir ROOT sub-directory name to write the triggers (only for ROOT format).
   * @param[in] aWriteMode Write mode: "RECREATE" or "UPDATE" (only with sub-directories).
   * @param[in] aWriteMeta Flag to write (or not) the metadata tree.
   */
  string Write(const string aOutDir=".",
	       const string aFileFormat="",
	       const string aFileName="",
	       const string aSubDir="",
	       const string aWriteMode="RECREATE",
	       const bool aWriteMeta=true);

  /**
   * @brief Writes the metadata in a ROOT file.
   * @details Three trees are saved: the trigger "metadata" tree, the "segments" tree, and the "gwloptions" tree (if any).
   * @warning If options were added with GwollumOptions, the GwollumOptions::FillTree() function must be called first (and only once)! Otherwise options will not be saved (empty tree).
   * @pre A ROOT file must be open first and set to the directory where to write the TTree.
   */
  bool WriteMetaData(void);

  /**
   * @brief Set the process name.
   * @param[in] aProcessName Process name.
   */
  inline void SetProcessName(const string aProcessName){
    Mprocessname = aProcessName;
  };

  /**
   * @brief Sets the version for this process.
   * @param[in] aProcessVersion Process version.
   */
  inline void SetProcessVersion(const string aProcessVersion){
    Mprocessversion = aProcessVersion;
  };

 protected:

  // METADATA
  TTree *Mtree;               ///< Metadata tree.
  string Mprocessname;        ///< Meta process name.
  string Mprocessversion;     ///< Meta process version.
  string Mprocessuser;        ///< Meta user name.
  double Mstart;              ///< Meta start time.
  double Mend;                ///< Meta end time.
  double Mfmin;               ///< Meta minimum fstart.
  double Mfmax;               ///< Meta maximum fend.
  double Mqmin;               ///< Meta minimum Q.
  double Mqmax;               ///< Meta maximum Q.
  double Msnrmin;             ///< Meta minimum SNR.
  double Msnrmax;             ///< Meta maximum SNR.
 
 private:
  
  /**
   * @brief Trigger structure for hdf5.
   */
  typedef struct h5_triggers{
    double h5_time;
    double h5_freq;
    double h5_snr;
    double h5_q;
    double h5_amp;
    double h5_ph;
    double h5_tstart;
    double h5_tend;
    double h5_fstart;
    double h5_fend;
  } h5_triggers; ///< trigger structure for hdf5

  /**
   * @brief Segment structure for hdf5.
   */
  typedef struct h5_segments{
    double h5_start;
    double h5_end;
  } h5_segments;

  /**
   * @brief Datatype for hdf5 (triggers).
   */
  H5::CompType *h5_type;

  /**
   * @brief Datatype for hdf5 (segments).
   */
  H5::CompType *h5_type_s;

  /**
   * @brief Write triggers in a HDF5 file.
   * @param[in] aFileName File name.
   */
  void WriteHDF5(const string aFileName);

  /**
   * @brief Write triggers in a text file.
   * @param[in] aFileName File name.
   */
  void WriteTXT(const string aFileName);

  /**
   * @brief Returns the start/stop metadata.
   * @param[out] aStart GPS start time.
   * @param[out] aStop GPS stop time.
   */
  void GetMetaSegment(double &aStart, double &aStop);

  ClassDef(MakeTriggers,0)  
};

#endif


