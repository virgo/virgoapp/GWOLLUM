/**
 * @file 
 * @brief Program to test the Triggers class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "MakeTriggers.h"
#include "ReadTriggers.h"

using namespace std;

/**
 * @brief Test program.
 */
int main(int argc, char* argv[]){

  //*********************************************************************
  // Triggers(const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nTriggers(const unsigned int aVerbose=0) (write-mode)"<<endl;
  Triggers *T = new Triggers(0);
  cout<<"\tReset()"<<endl;
  T->Reset();
  cout<<"\tSortTriggers()"<<endl;
  if(T->SortTriggers()!=true) return 1;
  cout<<"\tGetClusterizeDt()"<<endl;
  if(T->GetClusterizeDt()!=0.1) return 1;
  cout<<"\tGetClusterizeSizeMin()"<<endl;
  if(T->GetClusterizeSizeMin()!=0) return 1;
  cout<<"\tGetClusterizeSnrThr()"<<endl;
  if(T->GetClusterizeSnrThr()!=0.0) return 1;
  cout<<"\tResetClusters()"<<endl;
  T->ResetClusters();
  cout<<"\tClusterize()"<<endl;
  if(T->Clusterize()!=true) return 1;
  cout<<"\tGetTriggerN()"<<endl;
  if(T->GetTriggerN()!=0) return 1;
  cout<<"\tGetClusterN()"<<endl;
  if(T->GetClusterN()!=0) return 1;

  //*********************************************************************
  // AddTrigger(...)
  //*********************************************************************
  cout<<"\nAddTrigger(...)"<<endl;
  if(!T->AddTrigger(1200000001.040, 100.0,
                    10.0, 1.0,
                    1200000001.020, 1200000001.100,
                    100.0, 100.0,
                    1e-21, 0.1)) return 2;
  if(!T->AddTrigger(1200000001.000, 200.0,
                    20.0, 2.0,
                    1200000001.000, 1200000002.0,
                    200.0, 300.0,
                    1e-21, 0.1)) return 2;
  if(!T->AddTrigger(1200000020.0, 300.0,
                    30.0, 3.0,
                    1200000010.0, 1200000020.0,
                    200.0, 300.0,
                    1e-21, 0.1)) return 2;
  cout<<"\tGetTriggerN()"<<endl;
  if(T->GetTriggerN()!=3) return 2;
  cout<<"\tGetTriggerTime()"<<endl;
  if(T->GetTriggerTime(0)!=1200000001.040) return 2;
  if(T->GetTriggerTime(1)!=1200000001.000) return 2;
  if(T->GetTriggerTime(2)!=1200000020.000) return 2;
  if(T->GetTriggerTime(3)!=0.0) return 2;
  cout<<"\tGetTriggerFrequency()"<<endl;
  if(T->GetTriggerFrequency(0)!=100.0) return 2;
  if(T->GetTriggerFrequency(1)!=200.0) return 2;
  if(T->GetTriggerFrequency(2)!=300.0) return 2;
  if(T->GetTriggerFrequency(3)!=0.0) return 2;
  cout<<"\tGetTriggerQ()"<<endl;
  if(T->GetTriggerQ(0)!=1.0) return 2;
  if(T->GetTriggerQ(1)!=2.0) return 2;
  if(T->GetTriggerQ(2)!=3.0) return 2;
  if(T->GetTriggerQ(3)!=0.0) return 2;
  cout<<"\tGetTriggerSnr()"<<endl;
  if(T->GetTriggerSnr(0)!=10.0) return 2;
  if(T->GetTriggerSnr(1)!=20.0) return 2;
  if(T->GetTriggerSnr(2)!=30.0) return 2;
  if(T->GetTriggerSnr(3)!=0.0) return 2;
  cout<<"\tGetTriggerTimeStart()"<<endl;
  if(T->GetTriggerTimeStart(0)!=1200000001.020) return 2;
  if(T->GetTriggerTimeStart(1)!=1200000001.000) return 2;
  if(T->GetTriggerTimeStart(2)!=1200000010.0) return 2;
  if(T->GetTriggerTimeStart(3)!=0.0) return 2;
  cout<<"\tGetTriggerTimeEnd()"<<endl;
  if(T->GetTriggerTimeEnd(0)!=1200000001.100) return 2;
  if(T->GetTriggerTimeEnd(1)!=1200000002.000) return 2;
  if(T->GetTriggerTimeEnd(2)!=1200000020.0) return 2;
  if(T->GetTriggerTimeEnd(3)!=0.0) return 2;
  cout<<"\tGetTriggerFrequencyStart()"<<endl;
  if(T->GetTriggerFrequencyStart(0)!=100.0) return 2;
  if(T->GetTriggerFrequencyStart(1)!=200.0) return 2;
  if(T->GetTriggerFrequencyStart(2)!=200.0) return 2;
  if(T->GetTriggerFrequencyStart(3)!=0.0) return 2;
  cout<<"\tGetTriggerFrequencyEnd()"<<endl;
  if(T->GetTriggerFrequencyEnd(0)!=100.0) return 2;
  if(T->GetTriggerFrequencyEnd(1)!=300.0) return 2;
  if(T->GetTriggerFrequencyEnd(2)!=300.0) return 2;
  if(T->GetTriggerFrequencyEnd(3)!=0.0) return 2;
  cout<<"\tGetTriggerAmplitude()"<<endl;
  if(T->GetTriggerAmplitude(0)!=1e-21) return 2;
  if(T->GetTriggerAmplitude(1)!=1e-21) return 2;
  if(T->GetTriggerAmplitude(2)!=1e-21) return 2;
  if(T->GetTriggerAmplitude(3)!=0.0) return 2;
  cout<<"\tGetTriggerPhase()"<<endl;
  if(T->GetTriggerPhase(0)!=0.1) return 2;
  if(T->GetTriggerPhase(1)!=0.1) return 2;
  if(T->GetTriggerPhase(2)!=0.1) return 2;
  if(T->GetTriggerPhase(3)!=-99.0) return 2;

  //*********************************************************************
  // SortTriggers(void)
  //*********************************************************************
  cout<<"\nSortTriggers(void)"<<endl;
  if(T->SortTriggers()!=true) return 3;
  cout<<"\tGetTriggerTimeStart()"<<endl;
  if(T->GetTriggerTimeStart(0)!=1200000001.000) return 3;
  if(T->GetTriggerTimeStart(1)!=1200000001.020) return 3;
  if(T->GetTriggerTimeStart(2)!=1200000010.0) return 3;
  if(T->GetTriggerTimeStart(3)!=0.0) return 3;
  cout<<"\tGetTriggerSnr()"<<endl;
  if(T->GetTriggerSnr(0)!=20.0) return 3;
  if(T->GetTriggerSnr(1)!=10.0) return 3;
  if(T->GetTriggerSnr(2)!=30.0) return 3;
  if(T->GetTriggerSnr(3)!=0.0) return 3;
  cout<<"\tGetTriggerTime()"<<endl;
  if(T->GetTriggerTime(0)!=1200000001.000) return 3;
  if(T->GetTriggerTime(1)!=1200000001.040) return 3;
  if(T->GetTriggerTime(2)!=1200000020.000) return 3;
  if(T->GetTriggerTime(3)!=0.0) return 3;
  cout<<"\tGetTriggerFrequency()"<<endl;
  if(T->GetTriggerFrequency(0)!=200.0) return 3;
  if(T->GetTriggerFrequency(1)!=100.0) return 3;
  if(T->GetTriggerFrequency(2)!=300.0) return 3;
  if(T->GetTriggerFrequency(3)!=0.0) return 3;

  //*********************************************************************
  // Clusterize(const int aTag=1)
  //*********************************************************************
  cout<<"\nClusterize(const int aTag=1)"<<endl;
  if(!T->AddTrigger(1200000001.040, 120.0,
                    102.0, 80.0,
                    1200000001.020, 1200000001.200,
                    30.0, 150.0,
                    1e-23, 0.5)) return 4;
  cout<<"\tSortTriggers()"<<endl;
  if(T->SortTriggers()!=true) return 4;
  cout<<"\tClusterize(()"<<endl;
  if(T->Clusterize(-1)!=true) return 4;
  cout<<"\tGetTriggerN()"<<endl;
  if(T->GetTriggerN()!=4) return 4;
  cout<<"\tGetClusterN()"<<endl;
  if(T->GetClusterN()!=2) return 4;
  cout<<"\tGetClusterTime()"<<endl;
  if(T->GetClusterTime(0)!=1200000001.040) return 4;
  if(T->GetClusterTime(1)!=1200000020.000) return 4;
  cout<<"\tGetClusterFrequency()"<<endl;
  if(T->GetClusterFrequency(0)!=120.0) return 4;
  if(T->GetClusterFrequency(1)!=300.0) return 4;
  cout<<"\tGetClusterQ()"<<endl;
  if(T->GetClusterQ(0)!=80.0) return 4;
  if(T->GetClusterQ(1)!=3.0) return 4;
  cout<<"\tGetClusterSnr()"<<endl;
  if(T->GetClusterSnr(0)!=102.0) return 4;
  if(T->GetClusterSnr(1)!=30.0) return 4;
  cout<<"\tGetClusterTimeStart()"<<endl;
  if(T->GetClusterTimeStart(0)!=1200000001.000) return 4;
  if(T->GetClusterTimeStart(1)!=1200000010.0) return 4;
  cout<<"\tGetClusterTimeEnd()"<<endl;
  if(T->GetClusterTimeEnd(0)!=1200000002.000) return 4;
  if(T->GetClusterTimeEnd(1)!=1200000020.0) return 4;
  cout<<"\tGetClusterFrequencyStart()"<<endl;
  if(T->GetClusterFrequencyStart(0)!=30.0) return 4;
  if(T->GetClusterFrequencyStart(1)!=200.0) return 4;
  cout<<"\tGetClusterFrequencyEnd()"<<endl;
  if(T->GetClusterFrequencyEnd(0)!=300.0) return 4;
  if(T->GetClusterFrequencyEnd(1)!=300.0) return 4;
  cout<<"\tGetClusterAmplitude()"<<endl;
  if(T->GetClusterAmplitude(0)!=1.0e-23) return 4;
  if(T->GetClusterAmplitude(1)!=1.0e-21) return 4;
  cout<<"\tGetClusterPhase()"<<endl;
  if(T->GetClusterPhase(0)!=0.5) return 4;
  if(T->GetClusterPhase(1)!=0.1) return 4;
  cout<<"\tGetClusterSize()"<<endl;
  if(T->GetClusterSize(0)!=3) return 4;
  if(T->GetClusterSize(1)!=1) return 4;
  cout<<"\tGetClusterTag()"<<endl;
  if(T->GetClusterTag(0)!=-1) return 4;
  if(T->GetClusterTag(1)!=-1) return 4;
  cout<<"\tGetClusterFirstTrigger()"<<endl;
  if(T->GetClusterFirstTrigger(0)!=0) return 4;
  if(T->GetClusterFirstTrigger(1)!=3) return 4;
  cout<<"\tGetClusterIndex()"<<endl;
  if(T->GetClusterIndex(1200000000.000)!=-1) return 4;
  if(T->GetClusterIndex(1200000001.000)!=0) return 4;
  if(T->GetClusterIndex(1200000001.200)!=0) return 4;
  if(T->GetClusterIndex(1200000002.000)!=-1) return 4;
  if(T->GetClusterIndex(1200000005.000)!=-1) return 4;
  if(T->GetClusterIndex(1200000010.000)!=1) return 4;
  if(T->GetClusterIndex(1200000015.000)!=1) return 4;
  if(T->GetClusterIndex(1200000020.000)!=-1) return 4;
  if(T->GetClusterIndex(1200000021.000)!=-1) return 4;
  delete T;

  //*********************************************************************
  // MakeTriggers(const string aStreamName, const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nMakeTriggers(const string aStreamName, const unsigned int aVerbose=0)"<<endl;
  error_code errcode;
  RemoveFileOrDirectory(errcode, "./V1-CTEST_GWL-1200000000-100.root");
  RemoveFileOrDirectory(errcode, "./V1-CTEST_GWL-1200000200-100.root");
  RemoveFileOrDirectory(errcode, "./V1-CTEST_GWL-1200000000-300.root");
  MakeTriggers *M = new MakeTriggers("V1:CTEST", 0);
  M->SetProcessName("GWL");
  M->SetProcessVersion((string)GWL_PROJECT_VERSION);

  // add a set of options
  M->GwollumOptions::Add("DATA", "TESTI", (int)0, 3);
  M->GwollumOptions::Add("DATA", "TESTU", (unsigned int)0, 1);
  M->GwollumOptions::Add("DATA", "TESTD", 0.0, 1);
  M->GwollumOptions::Add("DATA", "TESTS", "zero", 1);
  M->GwollumOptions::Add("DATA", "TESTVI", (int)0, 0);
  M->GwollumOptions::Add("DATA", "TESTVU", (unsigned int)0, 0);
  M->GwollumOptions::Add("DATA", "TESTVD", 0.0, 0);
  M->GwollumOptions::Add("DATA", "TESTVS", "zero", 0);
  char *c_optfile = getenv("GWOLLUM_DATA");
  string optfile;
  if(argc==2) optfile = (string)argv[1];
  else if(c_optfile==NULL) optfile=".";
  else optfile = (string)c_optfile;
  optfile+="/gwollumoptions.opt";
  if(M->GwollumOptions::ReadTextFile(optfile)!=8) return 5;
  M->GwollumOptions::FillTree();

  // add triggers
  if(M->Segments::AddSegment(1200000000.000, 1200000100.000)==false) return 5;
  if(M->Triggers::AddTrigger(1200000000.500, 100.0, 5.0, 4.0, 1200000000.000, 1200000001.000, 99.0, 101.0, 1.0e-21, 1.0)==false) return 5;
  if(M->Triggers::AddTrigger(1200000002.500, 100.0, 5.0, 4.0, 1200000002.000, 1200000003.000, 99.0, 101.0, 1.0e-21, 1.0)==false) return 5;

  cout<<"\tWrite()"<<endl;
  if(M->Write(".", "root", "", "", "RECREATE", true).compare("./V1-CTEST_GWL-1200000000-100.root")) return 5;

  // write a second file
  if(M->Segments::AddSegment(1200000200.000, 1200000300.000)==false) return 5;
  if(M->Triggers::AddTrigger(1200000200.500, 100.0, 5.0, 4.0, 1200000200.000, 1200000201.000, 99.0, 101.0, 1.0e-21, 1.0)==false) return 5;
  if(M->Triggers::AddTrigger(1200000202.500, 100.0, 5.0, 4.0, 1200000202.000, 1200000203.000, 99.0, 101.0, 1.0e-21, 1.0)==false) return 5;
  cout<<"\tWrite()"<<endl;
  if(M->Write(".", "root", "", "", "RECREATE", true).compare("./V1-CTEST_GWL-1200000200-100.root")) return 5;

  delete M;

  //*********************************************************************
  // ReadTriggers(const string aPattern, const string aDirectory="", const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nReadTriggers(const string aPattern, const string aDirectory="", const unsigned int aVerbose=0)"<<endl;
  ReadTriggers *R = new ReadTriggers("./V1-CTEST_GWL-1200000*-100.root", "", 0);

  cout<<"\tSegments"<<endl;
  if(R->Segments::GetStatus()==false) return 6;
  if(R->Segments::GetLiveTime()!=200) return 6;
  if(R->Segments::GetN()!=2) return 6;
  if(R->Segments::GetFirst()!=1200000000.000) return 6;
  if(R->Segments::GetLast()!=1200000300.000) return 6;

  cout<<"\tStreams"<<endl;
  if(R->Streams::GetName().compare("V1:CTEST")) return 6;

  cout<<"\tGwollumOptions"<<endl;
  if(R->GwollumOptions::GetOptionsN()!=8) return 6;
  if(R->GwollumOptions::GetOptionSize("DATA", "TESTI")!=3) return 6;
  if(R->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTI")!=3) return 6;
  if(R->GwollumOptions::GetOptionValueI("DATA", "TESTI", 0)!=-1) return 6;
  if(R->GwollumOptions::GetOptionValueI("DATA", "TESTI", 1)!=-2) return 6;
  if(R->GwollumOptions::GetOptionValueI("DATA", "TESTI", 2)!=-3) return 6;
  if(R->GwollumOptions::GetOptionSize("DATA", "TESTVI")!=0) return 6;
  if(R->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTVI")!=2) return 6;
  if(R->GwollumOptions::GetOptionValueI("DATA", "TESTVI", 0)!=-1) return 6;
  if(R->GwollumOptions::GetOptionValueI("DATA", "TESTVI", 1)!=-2) return 6;
  if(R->GwollumOptions::GetOptionValueI("DATA", "TESTVI", 2)!=0) return 6;
  if(R->GwollumOptions::GetOptionSize("DATA", "TESTS")!=1) return 6;
  if(R->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTS")!=1) return 6;
  if(R->GwollumOptions::GetOptionValueS("DATA", "TESTS", 0).compare("four")) return 6;
  if(R->GwollumOptions::GetOptionSize("DATA", "TESTVS")!=0) return 6;
  if(R->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTVS")!=5) return 6;
  if(R->GwollumOptions::GetOptionValueS("DATA", "TESTVS", 0).compare("one")) return 6;
  if(R->GwollumOptions::GetOptionValueS("DATA", "TESTVS", 1).compare("two")) return 6;
  if(R->GwollumOptions::GetOptionValueS("DATA", "TESTVS", 2).compare("three")) return 6;
  if(R->GwollumOptions::GetOptionValueS("DATA", "TESTVS", 3).compare("four")) return 6;
  if(R->GwollumOptions::GetOptionValueS("DATA", "TESTVS", 4).compare("five")) return 6;

  cout<<"\tTriggers"<<endl;
  if(R->Triggers::GetTriggerN()!=4) return 6;
  if(R->Triggers::GetTriggerTime(0)!=1200000000.500) return 6;
  if(R->Triggers::GetTriggerTime(1)!=1200000002.500) return 6;
  if(R->Triggers::GetTriggerTime(2)!=1200000200.500) return 6;
  if(R->Triggers::GetTriggerTime(3)!=1200000202.500) return 6;

  cout<<"\tReadTriggerMetaData"<<endl;
  if(R->GetProcessName().compare("GWL")) return 6;
  if(R->GetProcessVersion().compare((string)GWL_PROJECT_VERSION)) return 6;
  if(R->Merge(".").compare("./V1-CTEST_GWL-1200000000-300.root")) return 6;
  ReadTriggers *Rm = new ReadTriggers("./V1-CTEST_GWL-1200000000-300.root", "", 0);
  if(Rm->Segments::GetN()!=2) return 6;
  if(Rm->Segments::GetFirst()!=1200000000.000) return 6;
  if(Rm->Segments::GetLast()!=1200000300.000) return 6;
  if(Rm->Streams::GetName().compare("V1:CTEST")) return 6;
  if(Rm->GwollumOptions::GetOptionsN()!=8) return 6;
  if(Rm->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTI")!=3) return 6;
  if(Rm->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTVI")!=2) return 6;
  if(Rm->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTVS")!=5) return 6;
  if(Rm->GwollumOptions::GetOptionNumberOfValues("DATA", "TESTVS")!=5) return 6;
  delete Rm;
  RemoveFileOrDirectory(errcode, "./V1-CTEST_GWL-1200000000-300.root");

  delete R;
  RemoveFileOrDirectory(errcode, "./V1-CTEST_GWL-1200000000-100.root");
  RemoveFileOrDirectory(errcode, "./V1-CTEST_GWL-1200000200-100.root");

  return 0;
}

