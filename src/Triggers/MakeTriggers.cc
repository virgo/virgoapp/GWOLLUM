/**
 * @file 
 * @brief See MakeTriggers.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "MakeTriggers.h"

ClassImp(MakeTriggers)

////////////////////////////////////////////////////////////////////////////////////
MakeTriggers::MakeTriggers(const string aStreamName, const unsigned int aVerbose): 
GwollumOptions(), Segments(), Streams(aStreamName, aVerbose), Triggers(aVerbose){ 
////////////////////////////////////////////////////////////////////////////////////

  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"MakeTriggers::"<<__func__<<endl;

  // metadata tree
  Mtree = new TTree("metadata", "metadata");
  Mtree->SetDirectory(0);
  Mtree->Branch("Mprocessname", &Mprocessname);
  Mtree->Branch("Mstreamname", &(Streams::Name));
  Mtree->Branch("Mprocessversion", &Mprocessversion);
  Mtree->Branch("Mprocessuser", &Mprocessuser);
  Mtree->Branch("start", &Mstart, "start/D");
  Mtree->Branch("end", &Mend, "end/D");
  Mtree->Branch("Mfmin", &Mfmin, "Mfmin/D");
  Mtree->Branch("Mfmax", &Mfmax, "Mfmax/D");
  Mtree->Branch("Mqmin", &Mqmin, "Mqmin/D");
  Mtree->Branch("Mqmax", &Mqmax, "Mqmax/D");
  Mtree->Branch("Msnrmin", &Msnrmin, "Msnrmin/D");
  Mtree->Branch("Msnrmax", &Msnrmax, "Msnrmax/D");

  // default metadata
  Mprocessname="PROCESS";
  Mprocessversion="0";
  char *login = getlogin();
  if(login==NULL) Mprocessuser = "unknown";
  else            Mprocessuser = (string)login;
  
  // create the hdf5 memory datatype
  h5_type = new H5::CompType(sizeof(h5_triggers));
  h5_type->insertMember("time", HOFFSET(h5_triggers, h5_time), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("frequency", HOFFSET(h5_triggers, h5_freq), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("tstart", HOFFSET(h5_triggers, h5_tstart), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("tend", HOFFSET(h5_triggers, h5_tend), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("fstart", HOFFSET(h5_triggers, h5_fstart), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("fend", HOFFSET(h5_triggers, h5_fend), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("snr", HOFFSET(h5_triggers, h5_snr), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("q", HOFFSET(h5_triggers, h5_q), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("amplitude", HOFFSET(h5_triggers, h5_amp), H5::PredType::NATIVE_DOUBLE);
  h5_type->insertMember("phase", HOFFSET(h5_triggers, h5_ph), H5::PredType::NATIVE_DOUBLE);
  h5_type_s = new H5::CompType(sizeof(h5_segments));
  h5_type_s->insertMember("start", HOFFSET(h5_segments, h5_start), H5::PredType::NATIVE_DOUBLE);
  h5_type_s->insertMember("end", HOFFSET(h5_segments, h5_end), H5::PredType::NATIVE_DOUBLE);
  
}

////////////////////////////////////////////////////////////////////////////////////
MakeTriggers::~MakeTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"MakeTriggers::"<<__func__<<endl;
  delete Mtree;
  delete h5_type;
  delete h5_type_s;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"MakeTriggers::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": corrupted object"<<endl;
    return;
  }
  Triggers::Reset();
  Segments::Reset();
  Mtree->Reset();
}

////////////////////////////////////////////////////////////////////////////////////
string MakeTriggers::Write(const string aOutDir,
			   const string aFileFormat,
			   const string aFileName,
			   const string aSubDir,
			   const string aWriteMode,
			   const bool aWriteMeta){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"MakeTriggers::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": corrupted object"<<endl;
    return "";
  }

  // no trigger, no segment
  if((Ttree->GetEntries()==0)&&(Segments::GetN()==0))
    return "";

  // check output directory
  if(!filesystem::is_directory(aOutDir)){
    Triggers::mon->SetMessageType(monitor_error);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": output directory does not exist ("<<aOutDir<<")"<<endl;
    return "";
  }

  // file formats
  vector <string> fileformat;
  if(aFileFormat.find("hdf5")!=string::npos) fileformat.push_back("h5");
  if(aFileFormat.find("txt")!=string::npos)  fileformat.push_back("txt");
  if(aFileFormat.find("root")!=string::npos) fileformat.push_back("root");
  if(!fileformat.size())                     fileformat.push_back("root");

  // get timing info
  double st, en;
  GetMetaSegment(st, en);

  // no segment
  if(Segments::GetN()==0){
    if(!Segments::AddSegment((unsigned int)floor(st), (unsigned int)ceil(en))){
      return "";
    }
  }

  // output file name
  string outfile;
  string wm;
  gwl_ss.clear(); gwl_ss.str("");

  // loop over output formats
  for(unsigned int f=0; f<fileformat.size(); f++){

    // output file name
    if(aFileName.compare("")){
      gwl_ss<<aOutDir<<"/"<<aFileName<<"."<<fileformat[f];
      outfile = gwl_ss.str();
      gwl_ss.clear(); gwl_ss.str("");
    }
    else
      outfile = Streams::GetTriggerFileName((unsigned int)st, (unsigned int)(ceil(en)-floor(st)), fileformat[f], Mprocessname, aOutDir);

    // save root trees
    if(!fileformat[f].compare("root")){// ROOT format

      // sub-directory
      wm="RECREATE";
      if(aSubDir.compare("")) wm=aWriteMode;// updates are only possible with sub-directories
      TFile outTFile(outfile.c_str(), wm.c_str());
      outTFile.cd();
      if(aSubDir.compare("")){
	outTFile.mkdir(aSubDir.c_str());
	outTFile.cd(aSubDir.c_str());
      }

      // write trigger tree
      if(Ttree->Write("triggers")<0){
        outTFile.Close();
        fileformat.clear();
        MakeTriggers::Reset();
        return "";
      }

      // write metadata (+segments +options) tree
      if(aWriteMeta && !WriteMetaData()){
        outTFile.Close();
        fileformat.clear();
        MakeTriggers::Reset();
        return "";
      }

      outTFile.Close();
    }

    // save h5
    else if(!fileformat[f].compare("h5")) WriteHDF5(outfile);

    // save txt
    else WriteTXT(outfile);

    Triggers::mon->SetMessageType(monitor_info_4);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": "<<Ttree->GetEntries()<<" triggers"<<endl;
    Triggers::mon->SetMessageType(monitor_info_4);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": "<<Segments::GetN()<<" segments"<<endl;
    Triggers::mon->SetMessageType(monitor_info_4);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": written in "<<outfile<<endl;
  }
  
  fileformat.clear();
  MakeTriggers::Reset();

  return outfile;
}

////////////////////////////////////////////////////////////////////////////////////
bool MakeTriggers::WriteMetaData(void){
////////////////////////////////////////////////////////////////////////////////////
  Triggers::mon->SetMessageType(monitor_info_2);
  *Triggers::mon<<"MakeTriggers::"<<__func__<<endl;
  if(GetStatus()==false){
    Triggers::mon->SetMessageType(monitor_fatal);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // get start/stop
  GetMetaSegment(Mstart, Mend);
  
  // set internal metadata
  if(Ttree->GetEntries()){
    Mfmin = Ttree->GetMinimum("fstart");
    Mfmax = Ttree->GetMaximum("fend");
    Mqmin = Ttree->GetMinimum("q");
    Mqmax = Ttree->GetMaximum("q");
    Msnrmin = Ttree->GetMinimum("snr");
    Msnrmax = Ttree->GetMaximum("snr");
  }
  else{
    // keep this to tag files with no triggers
    // see also ReadTriggerMetaData
    Mfmin   = -1.0;
    Mfmax   = -1.0;
    Mqmin   = -1.0;
    Mqmax   = -1.0;
    Msnrmin = -1.0;
    Msnrmax = -1.0;
  }
  
  // fill metadata tree (single entry)
  Mtree->Reset();
  Mtree->Fill();

  // get segment tree
  TTree *Stree = Segments::GetTree();
  if(Stree==NULL){
    Triggers::mon->SetMessageType(monitor_error);
    *Triggers::mon<<"MakeTriggers::"<<__func__<<": no segment tree"<<endl;
    return false;
  }

  // write trees
  if(Mtree->Write("metadata")<=0) return false;
  if(Stree->Write("segments")<=0) return false;
  GwollumOptions::WriteTree("gwloptions");

  delete Stree;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::WriteHDF5(const string aFileName){ 
////////////////////////////////////////////////////////////////////////////////////
 
  // create dataset and data space for segments
  h5_segments *h5_s; hsize_t dimsf_s[1];
  dimsf_s[0] = Segments::GetN();
  h5_s = new h5_segments [Segments::GetN()];
  H5::DataSpace dataspace_s(1, dimsf_s);
  for(unsigned int s=0; s<Segments::GetN(); s++){
    h5_s[s].h5_start=Segments::GetStart(s);
    h5_s[s].h5_end=Segments::GetEnd(s);
  }

  // create dataset and data space for triggers
  h5_triggers *h5_t; hsize_t dimsf[1];
  if(GetClusterN()){// use clusters
    dimsf[0] = GetClusterN();
    h5_t = new h5_triggers [GetClusterN()];
  }
  else{// use triggers
    dimsf[0] = Ttree->GetEntries();
    h5_t = new h5_triggers [Ttree->GetEntries()];
  }
  H5::DataSpace dataspace(1, dimsf);
 
  // save clusters
  if(GetClusterN()){
    for(unsigned int c=0; c<GetClusterN(); c++){
      h5_t[c].h5_time=GetClusterTime(c);
      h5_t[c].h5_freq=GetClusterFrequency(c);
      h5_t[c].h5_snr=GetClusterSnr(c);
      h5_t[c].h5_q=GetClusterQ(c);
      h5_t[c].h5_amp=GetClusterAmplitude(c);
      h5_t[c].h5_ph=GetClusterPhase(c);
      h5_t[c].h5_tstart=GetClusterTimeStart(c);
      h5_t[c].h5_tend=GetClusterTimeEnd(c);
      h5_t[c].h5_fstart=GetClusterFrequencyStart(c);
      h5_t[c].h5_fend=GetClusterFrequencyEnd(c);
    }
  }
  // save triggers
  else{
    for(Long64_t t=0; t<Ttree->GetEntries(); t++){
      Ttree->GetEntry(t);
      h5_t[t].h5_time=GetTriggerTime(t);
      h5_t[t].h5_freq=GetTriggerFrequency(t);
      h5_t[t].h5_snr=GetTriggerSnr(t);
      h5_t[t].h5_q=GetTriggerQ(t);
      h5_t[t].h5_amp=GetTriggerAmplitude(t);
      h5_t[t].h5_ph=GetTriggerPhase(t);
      h5_t[t].h5_tstart=GetTriggerTimeStart(t);
      h5_t[t].h5_tend=GetTriggerTimeEnd(t);
      h5_t[t].h5_fstart=GetTriggerFrequencyStart(t);
      h5_t[t].h5_fend=GetTriggerFrequencyEnd(t);
    }
  }
 
  // create the output file
  H5::H5File* h5_file = new H5::H5File(aFileName.c_str(), H5F_ACC_TRUNC);

  // Create the data space for the attributes
  H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
  
  // File attributes 
  H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
  h5_file->createAttribute("Mprocessname", strdatatype, attr_dataspace).write(strdatatype, Mprocessname);
  h5_file->createAttribute("Mstreamname", strdatatype, attr_dataspace).write(strdatatype, Streams::Name);
  h5_file->createAttribute("Mprocessversion", strdatatype, attr_dataspace).write(strdatatype, Mprocessversion);
  h5_file->createAttribute("Mprocessuser", strdatatype, attr_dataspace).write(strdatatype, Mprocessuser);

  // save options
  GwollumOptions::WriteH5(h5_file, attr_dataspace);
  
  // create the dataset
  H5::DataSet* dataset;
  dataset = new H5::DataSet(h5_file->createDataSet("triggers", *h5_type, dataspace));

  // Write the data to the dataset
  dataset->write(h5_t, *h5_type);
  delete dataset;

  dataset = new H5::DataSet(h5_file->createDataSet("segments", *h5_type_s, dataspace_s));
  dataset->write(h5_s, *h5_type_s);
  delete dataset;

  // cleaning
  delete h5_file;
  delete [] h5_t;
  delete [] h5_s;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::WriteTXT(const string aFileName){ 
////////////////////////////////////////////////////////////////////////////////////
  
  // open txt file stream        
  ofstream txtfile(aFileName.c_str());

  // write file header
  txtfile.precision ( 5 );
  txtfile<<"# central time [s]"<<endl;
  txtfile<<"# central frequency [Hz]"<<endl;
  txtfile<<"# snr []"<<endl;
  txtfile<<"# q []"<<endl;
  txtfile<<"# amplitude [Hz^-1/2]"<<endl;
  txtfile<<"# phase [rad]"<<endl;
  txtfile<<"# starting time [s]"<<endl;
  txtfile<<"# ending time [s]"<<endl;
  txtfile<<"# starting frequency [Hz]"<<endl;
  txtfile<<"# ending frequency [Hz]"<<endl;
  
  // print clusters
  if(GetClusterN()){
    for(unsigned int c=0; c<GetClusterN(); c++){
      txtfile<<fixed<<GetClusterTime(c)<<"\t"
             <<GetClusterFrequency(c)<<"\t"
             <<GetClusterSnr(c)<<"\t"
             <<GetClusterQ(c)<<"\t"
             <<scientific<<GetClusterAmplitude(c)<<"\t"
             <<fixed<<GetClusterPhase(c)<<"\t"
             <<GetClusterTimeStart(c)<<"\t"
             <<GetClusterTimeEnd(c)<<"\t"
             <<GetClusterFrequencyStart(c)<<"\t"
             <<GetClusterFrequencyEnd(c)<<endl;
    }
  }
  // print triggers
  else{
    for(Long64_t t=0; t<Ttree->GetEntries(); t++){
      Ttree->GetEntry(t);
      txtfile<<fixed<<GetTriggerTime(t)<<"\t"
             <<GetTriggerFrequency(t)<<"\t"
             <<GetTriggerSnr(t)<<"\t"
             <<GetTriggerQ(t)<<"\t"
             <<scientific<<GetTriggerAmplitude(t)<<"\t"
             <<fixed<<GetTriggerPhase(t)<<"\t"
             <<GetTriggerTimeStart(t)<<"\t"
             <<GetTriggerTimeEnd(t)<<"\t"
             <<GetTriggerFrequencyStart(t)<<"\t"
             <<GetTriggerFrequencyEnd(t)<<endl;
    }
  }

  txtfile.close();

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MakeTriggers::GetMetaSegment(double &aStart, double &aStop){ 
////////////////////////////////////////////////////////////////////////////////////

  // no segment -> create one from triggers
  if(Segments::GetN()==0){
    if(Ttree->GetEntries()){
      aStart = Ttree->GetMinimum("tstart");
      aStop = Ttree->GetMaximum("time");
    }
    else{
      aStart = 0.0;
      aStop = 1.0;
    }
    return;
  }

  // start - end
  aStart = Segments::GetStart(0);
  aStop = Segments::GetEnd(Segments::GetN()-1);
  return;
}
