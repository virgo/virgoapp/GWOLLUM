# -- Coinc library -----------

set(
  GWOLLUM_COINC_HEADERS
  Coinc2.h
  )

# build root dictionary
include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/../Segments
  ${CMAKE_CURRENT_SOURCE_DIR}/../Streams
  ${CMAKE_CURRENT_SOURCE_DIR}/../Time
  ${CMAKE_CURRENT_SOURCE_DIR}/../Triggers
  ${CMAKE_CURRENT_SOURCE_DIR}/../Utils
  ${HDF5_INCLUDE_DIRS}
  )
root_generate_dictionary(
  CoincDict
  LINKDEF LinkDef.h
  MODULE Coinc
  OPTIONS ${GWOLLUM_COINC_HEADERS}
  )

# compile library
add_library(
  Coinc
  SHARED
  Coinc2.cc
  CoincDict.cxx
  )
target_include_directories(
  Coinc
  PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  )
target_link_libraries(
  Coinc
  Triggers
  )
set_target_properties(
  Coinc PROPERTIES
  PUBLIC_HEADER "${GWOLLUM_COINC_HEADERS}"
  )

# install library
install(
  TARGETS
  Coinc
  RUNTIME DESTINATION ${RUNTIME_DESTINATION}
  LIBRARY DESTINATION ${LIBRARY_DESTINATION}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )

# install ROOT PCM
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/libCoinc_rdict.pcm
  ${CMAKE_CURRENT_BINARY_DIR}/libCoinc.rootmap
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  )
