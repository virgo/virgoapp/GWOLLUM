/**
 * @file 
 * @brief See GwollumPlot.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "GwollumPlot.h"

ClassImp(GwollumPlot)

////////////////////////////////////////////////////////////////////////////////////
GwollumPlot::GwollumPlot(const string aName, const string aStyleName){
////////////////////////////////////////////////////////////////////////////////////
  
  mon = new Monitor("gwplot");

  // random id
  randid = rand();
  gwl_ss.clear(); gwl_ss.str("");
  gwl_ss<<randid<<"_"<<getpid();
  srandid = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  name = aName;

  // color codes for Ox
  oxcolindex[0] = TColor::GetFreeColorIndex();// background
  oxcolor[0] = new TColor(oxcolindex[0], 0.03, 0.03, 0.03);
  oxcolindex[1] = TColor::GetFreeColorIndex();// middleground
  oxcolor[1] = new TColor(oxcolindex[1], 250.0/255.0, 240.0/255.0, 140.0/255.0);
  oxcolindex[2] = TColor::GetFreeColorIndex();// foreground
  oxcolor[2] = new TColor(oxcolindex[2], 255.0/255.0, 250.0/255.0, 229.0/255.0);
  oxcolindex[3] = TColor::GetFreeColorIndex();// grid
  oxcolor[3] = new TColor(oxcolindex[3], 102.0/255.0, 99.0/255.0, 91.0/255.0);
   
  // set style
  if(!aStyleName.compare("GWOLLUM")) SetGwollumStyle();
  else if(!aStyleName.compare("FIRE")) SetFireStyle();
  else if(!aStyleName.compare("PINK")) SetPinkStyle();
  else if(!aStyleName.compare("STANDARD")) SetStandardStyle();
  else if(!aStyleName.compare("OX")) SetOxStyle();
  else{
    mon->SetMessageType(monitor_warning);
    *mon<<"GwollumPlot::"<<__func__<<": "+aStyleName+" style is not supported"<<endl;
    mon->SetMessageType(monitor_warning);
    *mon<<"GwollumPlot::"<<__func__<<": use GWOLLUM style by default"<<endl;
    SetGwollumStyle();
  }

  // create working canvas and pad
  Wcan = new TCanvas((name+"_can_"+srandid).c_str(), (name+"_can").c_str(),0,0,(int)(500*GOLDENNUMBER),500);
  Wcan->Draw();
  Wpad = new TPad((name+"_pad_"+srandid).c_str(), (name+"_pad").c_str(),0,0,1,1);
  Wpad->Draw();
  Wpad->cd();
   
  // legends
  Wleg = new TLegend(0.8, 0.1, 0.98, 0.9);
  Wleg->SetName((name+"_leg").c_str());
  Wleg->SetTextColor(gStyle->GetTextColor());// this is needed. bug in root?

  // additional text
  Wtext = new TText();
  Wtext->SetNDC(true);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumPlot::~GwollumPlot(void){
////////////////////////////////////////////////////////////////////////////////////
  delete Wtext;
  delete Wpad;
  delete Wcan;
  delete Wleg;
  delete oxcolor[0];
  delete oxcolor[1];
  delete oxcolor[2];
  delete oxcolor[3];
  delete mon;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddText(const string aText,
                          const double aX, const double aY, const double aSize,
                          const int aPadIndex){
////////////////////////////////////////////////////////////////////////////////////
  Wpad->cd(aPadIndex); 
  Wtext->SetText(aX, aY, aText.c_str());
  Wtext->SetTextSize(aSize);
  Wtext->Draw("same");
  gPad->Modified(); 
  gPad->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::UpdateText(const string aText){
////////////////////////////////////////////////////////////////////////////////////
  Wtext->SetText(Wtext->GetX(), Wtext->GetY(), aText.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddLegendEntry(const TObject *aObj, const string aLabel, const string aStyle){
////////////////////////////////////////////////////////////////////////////////////
  Wleg->AddEntry(aObj,aLabel.c_str(),aStyle.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::AddLegendHeader(const string aLabel){
////////////////////////////////////////////////////////////////////////////////////
  Wleg->SetHeader(aLabel.c_str());
  Wcan->Modified();
  Wcan->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::DrawLegend(void){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd(); 
  Wpad->SetPad(0.0, 0.0, 0.94, 1.0);
  Wpad->Modified(); gPad->Update();
  Wleg->Draw("same");
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::UnDrawLegend(void){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd();
  Wpad->GetListOfPrimitives()->Remove(Wleg);
  Wcan->GetListOfPrimitives()->Remove(Wleg);
  Wpad->SetPad(0.0, 0.0, 1.0, 1.0);
  Wpad->Modified(); gPad->Update();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::Print(const string aFileName, const double aScaleFactor){
////////////////////////////////////////////////////////////////////////////////////
  if(aScaleFactor<=0) return;
  Wcan->cd(); 
  unsigned int wdth = GetWidth();
  unsigned int hght = GetHeight();
  unsigned int newWidth=(unsigned int)ceil(aScaleFactor*wdth);
  unsigned int newHeight=(unsigned int)ceil(aScaleFactor*hght);
  ResizePlot(newWidth, newHeight);
  Print(aFileName.c_str());
  return ResizePlot(wdth, hght);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::Print(const string aFileName, const unsigned int aNewWidth, const unsigned int aNewHeight){
////////////////////////////////////////////////////////////////////////////////////
  Wcan->cd(); 
  unsigned int wdth = GetWidth();
  unsigned int hght = GetHeight();
  ResizePlot(aNewWidth, aNewHeight);
  Print(aFileName.c_str());
  return ResizePlot(wdth, hght);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetGwollumStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="GWOLLUM";
  gStyle->SetFillColor(1);
  gStyle->SetCanvasColor(1);
  gStyle->SetPadColor(1);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(7);
  gStyle->SetLineColor(7);
  gStyle->SetFuncColor(7);
  gStyle->SetTitleBorderSize(0);

  // Frame
  gStyle->SetFrameFillColor(1);
  gStyle->SetFrameLineColor(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(1);
  gStyle->SetStatColor(1);
  gStyle->SetStatTextColor(0);

  // axis
  gStyle->SetPalette(51);
  gStyle->SetNumberContours(104);
  gStyle->SetAxisColor(0,"XYZ");
  gStyle->SetLabelColor(0,"XYZ");
  gStyle->SetTitleColor(0,"XYZ");
  gStyle->SetGridColor(0);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(0);
  gStyle->SetTitleFillColor(1);
  gStyle->SetHistLineColor(7);
  gStyle->SetHistLineWidth(2);
  //gStyle->SetHistFillColor(0);

  // text
  gStyle->SetTextColor(0);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetPinkStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="PINK";
  gStyle->SetFillColor(1);
  gStyle->SetCanvasColor(1);
  gStyle->SetPadColor(1);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(6);
  gStyle->SetLineColor(6);
  gStyle->SetFuncColor(6);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  gStyle->SetFrameFillColor(1);
  gStyle->SetFrameLineColor(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(1);
  gStyle->SetStatColor(1);
  gStyle->SetStatTextColor(0);

  // axis
  gStyle->SetPalette(81);
  gStyle->SetNumberContours(104);
  gStyle->SetAxisColor(0,"XYZ");
  gStyle->SetAxisColor(1,"Z");
  gStyle->SetLabelColor(0,"XYZ");
  gStyle->SetTitleColor(0,"XYZ");
  gStyle->SetGridColor(0);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(0);
  gStyle->SetTitleFillColor(1);
  gStyle->SetHistLineColor(6);
  gStyle->SetHistLineWidth(2);

  // text
  gStyle->SetTextColor(0);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetFireStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="FIRE";
  gStyle->SetFillColor(1);
  gStyle->SetCanvasColor(1);
  gStyle->SetPadColor(1);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(5);
  gStyle->SetLineColor(5);
  gStyle->SetFuncColor(5);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  gStyle->SetFrameFillColor(1);
  gStyle->SetFrameLineColor(0);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(1);
  gStyle->SetStatColor(1);
  gStyle->SetStatTextColor(0);

  // axis
  gStyle->SetPalette(53);
  gStyle->SetNumberContours(104);
  gStyle->SetAxisColor(0,"XYZ");
  gStyle->SetAxisColor(1,"Z");
  gStyle->SetLabelColor(0,"XYZ");
  gStyle->SetTitleColor(0,"XYZ");
  gStyle->SetGridColor(0);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(0);
  gStyle->SetTitleFillColor(1);
  gStyle->SetHistLineColor(5);
  gStyle->SetHistLineWidth(2);

  // text
  gStyle->SetTextColor(0);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetStandardStyle(void){
////////////////////////////////////////////////////////////////////////////////////
  
  // General
  stylename="STANDARD";
  gStyle->SetFillColor(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetPadColor(0);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(kBlue+2);
  gStyle->SetLineColor(kBlue+2);
  gStyle->SetFuncColor(kBlue+2);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  gStyle->SetFrameFillColor(0);
  gStyle->SetFrameLineColor(1);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(0);
  gStyle->SetStatColor(0);
  gStyle->SetStatTextColor(1);

  // axis
  gStyle->SetNumberContours(104);
  gStyle->SetPalette(55);
  gStyle->SetAxisColor(1,"XYZ");
  gStyle->SetLabelColor(1,"XYZ");
  gStyle->SetTitleColor(1,"XYZ");
  gStyle->SetGridColor(1);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(1);
  gStyle->SetTitleFillColor(0);
  gStyle->SetHistLineColor(kBlue+2);
  gStyle->SetHistLineWidth(2);
  
  // text
  gStyle->SetTextColor(1);
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumPlot::SetOxStyle(void){
////////////////////////////////////////////////////////////////////////////////////

  // General
  stylename="OX";
  gStyle->SetFillColor(oxcolindex[0]);
  gStyle->SetCanvasColor(oxcolindex[0]);
  gStyle->SetPadColor(oxcolindex[0]);
  gStyle->SetPadLeftMargin(0.12);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetMarkerColor(oxcolindex[1]);
  gStyle->SetLineColor(oxcolindex[1]);
  gStyle->SetFuncColor(oxcolindex[1]);
  gStyle->SetTitleBorderSize(0);
 
  // Frame
  //gStyle->SetFrameFillStyle(4000);// transparent?
  gStyle->SetFrameFillColor(oxcolindex[0]);
  gStyle->SetFrameLineColor(oxcolindex[2]);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(oxcolindex[0]);
  gStyle->SetStatColor(oxcolindex[0]);
  gStyle->SetStatTextColor(oxcolindex[2]);

  // axis
  gStyle->SetNumberContours(128);
  gStyle->SetAxisColor(oxcolindex[2],"XYZ");
  gStyle->SetAxisColor(oxcolindex[0],"Z");
  gStyle->SetLabelColor(oxcolindex[2],"XYZ");
  gStyle->SetTitleColor(oxcolindex[2],"XYZ");
  gStyle->SetGridColor(oxcolindex[3]);

  // histos
  gStyle->SetOptStat(0);
  gStyle->SetTitleTextColor(oxcolindex[2]);
  gStyle->SetTitleFillColor(oxcolindex[0]);
  gStyle->SetHistLineColor(oxcolindex[1]);
  gStyle->SetHistLineWidth(2);

  // text
  gStyle->SetTextColor(oxcolindex[2]);

  // palette
  const int number_of_colors = 3;
  double color_positions[number_of_colors] = {0.00, 0.50, 1.00};
  double color_red[number_of_colors] = {oxcolor[0]->GetRed(), oxcolor[1]->GetRed(), oxcolor[2]->GetRed()};
  double color_green[number_of_colors] = {oxcolor[0]->GetGreen(), oxcolor[1]->GetGreen(), oxcolor[2]->GetGreen()};
  double color_blue[number_of_colors] = {oxcolor[0]->GetBlue(), oxcolor[1]->GetBlue(), oxcolor[2]->GetBlue()};
  TColor::CreateGradientColorTable(number_of_colors, color_positions, color_red, color_green, color_blue, 128);
}

/*
bool GwollumPlot::PrintLogo(Float_t v_scale, Float_t skew){

  if(gPad==NULL) return false;
  if(logopad!=NULL){ delete logopad; logopad=NULL; }
  if(logo!=NULL){ delete logo; logo=NULL; }

  // logo
  logo = TImage::Open("$GWOLLUM_DOC/Pics/gwollum_logo_min_trans.gif");
  if(!logo){
    logo=NULL;
    return false;
  }

  logo->SetConstRatio(kFALSE);
  UInt_t h_ = logo->GetHeight();
  UInt_t w_ = logo->GetWidth();
  
  Float_t r = w_/h_;
  Wpad->Update();
  Float_t rpad = Double_t(Wpad->VtoAbsPixel(0) - Wpad->VtoAbsPixel(1))/(Wpad->UtoAbsPixel(1) - Wpad->UtoAbsPixel(0));
  r *= rpad;
  
  Float_t d = 0.055;
  // absolute coordinates
  Float_t x1L = 0.01;
  Float_t y1T = 0.99;
  Float_t x1R = x1L + d*r/skew;
  Float_t y1B = y1T - d*v_scale*skew;
    
  logopad = new TPad("logopad", "logopad", x1L, y1B, x1R, y1T );
  logopad->SetRightMargin(0);
  logopad->SetBottomMargin(0);
  logopad->SetLeftMargin(0);
  logopad->SetTopMargin(0);
    
  Int_t xSizeInPixel = logopad->UtoAbsPixel(1) - logopad->UtoAbsPixel(0);
  Int_t ySizeInPixel = logopad->VtoAbsPixel(0) - logopad->VtoAbsPixel(1);
  if (xSizeInPixel<=25 || ySizeInPixel<=25) {
    delete logopad; logopad=NULL;
    return false; // ROOT doesn't draw smaller than this
  }

  logopad->Draw();
  logopad->cd();
  logo->Draw();


  return true;
} 
*/
