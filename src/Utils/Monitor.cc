/**
 * @file 
 * @brief See Monitor.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Monitor.h"


////////////////////////////////////////////////////////////////////////////////////
Monitor::Monitor(const string aProcessName): std::ostream(this){
////////////////////////////////////////////////////////////////////////////////////

  // init
  verbosity = 0;
  status = true;
  procname = aProcessName;

  // printing options
  this->precision(5);
  cout.precision(5);
  cerr.precision(5);
  this->flags(ios::fixed);
  cout.flags(ios::fixed);
  cerr.flags(ios::fixed);

  // init timer
  time(&timer);
  timer_start = timer;

  // init message
  message = "";
  message_type = monitor_status;
}

////////////////////////////////////////////////////////////////////////////////////
Monitor::~Monitor(void){
////////////////////////////////////////////////////////////////////////////////////

}

////////////////////////////////////////////////////////////////////////////////////
string Monitor::GetProcessingTimeString(void){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int n_sec = GetProcessingTime();
  gwl_ss<<n_sec/86400<<"d, "<<(n_sec%86400)/3600<<"h, "<<(n_sec%3600)/60<<"m, "<<(n_sec%60)<<"s";
  string proc_time = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  return proc_time;
}

////////////////////////////////////////////////////////////////////////////////////
string Monitor::GetProcessDate(void){
////////////////////////////////////////////////////////////////////////////////////

  // get current time
  time(&timer);

  // get GMT time
  ptm = gmtime(&timer);

    // print header
  gwl_ss<<setw(2)<<1900+ptm->tm_year<<"-"
        <<setfill('0')<<setw(2)<<ptm->tm_mon+1<<"-"
        <<setfill('0')<<setw(2)<<ptm->tm_mday<<", "
        <<setfill('0')<<setw(2)<<ptm->tm_hour<<":"
        <<setfill('0')<<setw(2)<<ptm->tm_min<<":"
        <<setfill('0')<<setw(2)<<ptm->tm_sec<<" UTC";

  string ret_string = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  
  return ret_string;
}

////////////////////////////////////////////////////////////////////////////////////
void Monitor::PrintMessage(void){
////////////////////////////////////////////////////////////////////////////////////

  // print message
  if(message_type==monitor_status){
    if(verbosity>=1) cout<<"\033[0;92m"<<GetProcessDate()<<" | STATUS  | "<<message<<"\033[0m"<<endl;
  }
  else if(message_type==monitor_info_2){
    if(verbosity>=2) cout<<"\033[0;96m"<<GetProcessDate()<<" | DEBUG-2 | "<<message<<"\033[0m"<<endl;
  }
  else if(message_type==monitor_info_3){
    if(verbosity>=3) cout<<"\033[0;94m"<<GetProcessDate()<<" | DEBUG-3 | > "<<message<<"\033[0m"<<endl;
  }
  else if(message_type==monitor_info_4){
    if(verbosity>=4) cout<<"\033[0;34m"<<GetProcessDate()<<" | DEBUG-4 | > > "<<message<<"\033[0m"<<endl;
  }
  else if(message_type==monitor_info_5){
    if(verbosity>=5) cout<<GetProcessDate()<<" | DEBUG-5 | > > > "<<message<<endl;
  }
  else if(message_type==monitor_fatal){
    cerr<<"\033[1;91m"<<GetProcessDate()<<" | FATAL   | "<<message<<"\033[0m"<<endl;
    status = false;  
  }
  else if(message_type==monitor_error){
    cerr<<"\033[0;91m"<<GetProcessDate()<<" | ERROR   | "<<message<<"\033[0m"<<endl;
  }
  else if(message_type==monitor_warning){
    cerr<<"\033[0;33m"<<GetProcessDate()<<" | WARNING | "<<message<<"\033[0m"<<endl;
  }
  else{
    cout<<GetProcessDate()<<" | WTF     | "<<message<<endl;
  }

  // reset message
  message_type = monitor_status;
  message = "";
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
int Monitor::overflow(int aChar){
////////////////////////////////////////////////////////////////////////////////////
  MakeMessage(aChar);
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////
void Monitor::MakeMessage(char aChar){
////////////////////////////////////////////////////////////////////////////////////
  if(aChar=='\n') return PrintMessage();
  message += aChar;
  return;
}
