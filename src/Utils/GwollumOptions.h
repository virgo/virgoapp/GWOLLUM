/**
 * @file 
 * @brief Interface to manage options.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __GwollumOptions__
#define __GwollumOptions__

#include "IO.h"
#include "Monitor.h"
#include <TFile.h>
#include <TChain.h>
#include <H5Cpp.h>

using namespace std;

/**
 * @brief Single option.
 * @details A single option is defined by:
 * - A TAG string
 * - A KEY string
 * - A type: int (i), unsigned int (u), double (d), string (s)
 * - A size: an option can be given a fixed size. Use 0 for an option with a variable size.
 * - A default value
 * - A list of values
 */
class GwollumOption{

public:
  /**
   * @name Constructors and destructors
   @{
  */

  /**
   * @brief Constructor of the GwollumOption class (int).
   * @details The option is uniquely identified by a TAG and a KEY string. It is given a default value. It is possible to fix the number of values for the option. If the size is equal to 0, the number of values is not fixed.
   * @param[in] aTag Option tag. Do not use "_"; they will be removed.
   * @param[in] aKey Option aKey. Do not use "_"; they will be removed.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for a variable size.
   */
  GwollumOption(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize=0);
   
  /**
   * @brief Constructor of the GwollumOption class (unsigned int).
   * @details The option is uniquely identified by a TAG and a KEY string. It is given a default value. It is possible to fix the number of values for the option. If the size is equal to 0, the number of values is not fixed.
   * @param[in] aTag Option tag. Do not use "_"; they will be removed.
   * @param[in] aKey Option aKey. Do not use "_"; they will be removed.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for a variable size.
   */
  GwollumOption(const string aTag, const string aKey, const unsigned int aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Constructor of the GwollumOption class (double).
   * @details The option is uniquely identified by a TAG and a KEY string. It is given a default value. It is possible to fix the number of values for the option. If the size is equal to 0, the number of values is not fixed.
   * @param[in] aTag Option tag. Do not use "_"; they will be removed.
   * @param[in] aKey Option aKey. Do not use "_"; they will be removed.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for a variable size.
   */
  GwollumOption(const string aTag, const string aKey, const double aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Constructor of the GwollumOption class (string).
   * @details The option is uniquely identified by a TAG and a KEY string. It is given a default value. It is possible to fix the number of values for the option. If the size is equal to 0, the number of values is not fixed.
   * @param[in] aTag Option tag. Do not use "_"; they will be removed.
   * @param[in] aKey Option aKey. Do not use "_"; they will be removed.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for a variable size.
   */
  GwollumOption(const string aTag, const string aKey, const string aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Destructor of the GwollumOption class.
   */
  virtual ~GwollumOption(void);

  /**
     @}
  */

  /**
   * @brief Tests whether an option value was provided.
   * @details This applies if at least one value was provided with Set() or Add().
   * @returns true if this is the case.
   */
  inline bool IsProvided(void){ return provided; };

  /**
   * @brief Returns the option tag.
   */
  inline string GetTag(void){ return tag; };

  /**
   * @brief Returns the option key.
   */
  inline string GetKey(void){ return key; };

  /**
   * @brief Returns the option type.
   */
  inline char GetType(void){ return valtype; };

  /**
   * @brief Returns the option size.
   * @returns 0 for variable-size options. Use GetNumberOfValues() to get the actual number of option values.
   */
  inline unsigned int GetSize(void){ return osize; };

  /**
   * @brief Returns the number of values.
   */
  inline unsigned int GetNumberOfValues(void){ return ivec.size()+uvec.size()+dvec.size()+svec.size(); };

  /**
   * @brief Returns the option value (int).
   * @warning Only call this function for `int` options!
   * @returns true if the value exists, false otherwise.
   * @param[out] aValue Returned value.
   * @param[in] aValIndex Value index.
   */
  inline bool GetValueI(int &aValue, const unsigned int aValIndex){
    if(aValIndex<ivec.size()){
      aValue = ivec[aValIndex];
      return true;
    }
    return false;
  };

  /**
   * @brief Returns the option value (int).
   * @warning Only call this function for `int` options!
   * @param[in] aValIndex Value index.
   * @returns The default value if the index is out of range.
   */
  inline int GetValueI(const unsigned int aValIndex=0){
    if(aValIndex<ivec.size()) return ivec[aValIndex];
    return idef;
  };

  /**
   * @brief Returns the list of option values (int).
   * @warning Only call this function for `int` options!
   */
  inline vector<int> GetValuesI(void){
    return ivec;
  };

  /**
   * @brief Returns the option default value (int).
   */
  inline int GetDefaultI(void){ return idef; };

  /**
   * @brief Adds an option value (int).
   * @details Appends the value to the list of option values.
   * @warning This function only works for variable-size options. Use the Set() function for fixed-sized options.
   * @param[in] aValue Option value.
   */
  inline void Add(const int aValue){
    if((valtype=='i')&&(osize==0)){
      ivec.push_back(aValue);
      provided = true;
    }
  };

  /**
   * @brief Updates an option value (int).
   * @param[in] aValIndex Option index.
   * @param[in] aValue Option value.
   */
  inline void Set(const unsigned int aValIndex, const int aValue){
    if(valtype!='i') return;
    if(aValIndex<ivec.size()){
      ivec[aValIndex] = aValue;
      provided = true;
    }
  };

  /**
   * @brief Returns the option value (unsigned int).
   * @warning Only call this function for `unsigned int` options!
   * @returns true if the value exists, false otherwise.
   * @param[out] aValue Returned value.
   * @param[in] aValIndex Value index.
   */
  inline bool GetValueU(unsigned int &aValue, const unsigned int aValIndex){
    if(aValIndex<uvec.size()){
      aValue = uvec[aValIndex];
      return true;
    }
    return false;
  };

  /**
   * @brief Returns the option value (unsigned int).
   * @warning Only call this function for `unsigned int` options!
   * @param[in] aValIndex Option index.
   * @returns The default value if the index is out of range.
   */
  inline unsigned int GetValueU(const unsigned int aValIndex=0){
    if(aValIndex<uvec.size()) return uvec[aValIndex];
    return udef;
  };

  /**
   * @brief Returns the list of option values (unsigned int).
   * @warning Only call this function for `unsigned int` options!
   */
  inline vector<unsigned int> GetValuesU(void){
    return uvec;
  };

  /**
   * @brief Returns the option default value (unsigned int).
   */
  inline unsigned int GetDefaultU(void){ return udef; };

  /**
   * @brief Adds the option value (unsigned int).
   * @details Appends the value to the list of option values.
   * @warning This function only works for variable-size options. Use the Set() function for fixed-sized options.
   * @param[in] aValue Option value.
   */
  inline void Add(const unsigned int aValue){
    if((valtype=='u')&&(osize==0)){
      uvec.push_back(aValue);
      provided = true;
    }
  };

  /**
   * @brief Updates an option value (unsigned int).
   * @param[in] aValIndex Option index.
   * @param[in] aValue Option value.
   */
  inline void Set(const unsigned int aValIndex, const unsigned int aValue){
    if(valtype!='u') return;
    if(aValIndex<uvec.size()){
      uvec[aValIndex] = aValue;
      provided = true;
    }
  };

  /**
   * @brief Returns the option value (double).
   * @warning Only call this function for `double` options!
   * @returns true if the value exists, false otherwise.
   * @param[out] aValue Returned value.
   * @param[in] aValIndex Value index.
   */
  inline bool GetValueD(double &aValue, const unsigned int aValIndex){
    if(aValIndex<dvec.size()){
      aValue = dvec[aValIndex];
      return true;
    }
    return false;
  };

  /**
   * @brief Returns the option value (double).
   * @warning Only call this function for `double` options!
   * @param[in] aValIndex Option index.
   * @returns The default value if the index is out of range.
   */
  inline double GetValueD(const unsigned int aValIndex=0){
    if(aValIndex<dvec.size()) return dvec[aValIndex];
    return ddef;
  };

  /**
   * @brief Returns the list of option values (double).
   * @warning Only call this function for `double` options!
   */
  inline vector<double> GetValuesD(void){
    return dvec;
  };

  /**
   * @brief Returns the option default value (double).
   */
  inline double GetDefaultD(void){ return ddef; };

  /**
   * @brief Adds the option value (double).
   * @details Appends the value to the list of option values.
   * @warning This function only works for variable-size options. Use the Set() function for fixed-sized options.
   * @param[in] aValue Option value.
   */
  inline void Add(const double aValue){
    if((valtype=='d')&&(osize==0)){
      dvec.push_back(aValue);
      provided = true;
    }
  };

  /**
   * @brief Updates an option value (double).
   * @param[in] aValIndex Option index.
   * @param[in] aValue Option value.
   */
  inline void Set(const unsigned int aValIndex, const double aValue){
    if(valtype!='d') return;
    if(aValIndex<dvec.size()){
      dvec[aValIndex] = aValue;
      provided = true;
    }      
  };

  /**
   * @brief Returns the option value (string).
   * @warning Only call this function for `string` options!
   * @returns true if the value exists, false otherwise.
   * @param[out] aValue Returned value.
   * @param[in] aValIndex Value index.
   */
  inline bool GetValueS(string &aValue, const unsigned int aValIndex){
    if(aValIndex<svec.size()){
      aValue = svec[aValIndex];
      return true;
    }
    return false;
  };

  /**
   * @brief Returns the option value (string).
   * @warning Only call this function for `string` options!
   * @param[in] aValIndex Option index.
   * @returns The default value if the index is out of range.
   */
  inline string GetValueS(const unsigned int aValIndex=0){
    if(aValIndex<svec.size()) return svec[aValIndex];
    return sdef;
  };

  /**
   * @brief Returns the list of option values (string).
   * @warning Only call this function for `string` options!
   */
  inline vector<string> GetValuesS(void){
    return svec;
  };

  /**
   * @brief Returns the option default value (string).
   */
  inline string GetDefaultS(void){ return sdef; };

  /**
   * @brief Adds the option value (string).
   * @details Appends the value to the list of option values.
   * @warning This function only works for variable-size options. Use the Set() function for fixed-sized options.
   * @param[in] aValue Option value.
   */
  inline void Add(const string aValue){
    if((valtype=='s')&&(osize==0)){
      svec.push_back(aValue);
      provided = true;
    }
  };

  /**
   * @brief Updates an option value (string).
   * @param[in] aValIndex Option index.
   * @param[in] aValue Option value.
   */
  inline void Set(const unsigned int aValIndex, const string aValue){
    if(valtype!='s') return;
    if(aValIndex<svec.size()){
      svec[aValIndex] = aValue;
      provided = true;
    }
  };
  
  /**
   * @brief Returns a codified option name.
   * @details The option name is codified: `[TAG]_[KEY]_[INDEX]_[SIZE]`:
   * - `[TAG]`: option tag word.
   * - `[KEY]`: option key word.
   * - `[INDEX]`: option value index.
   * - `[SIZE]`: option size (0 for variable option).
   *
   * @param[in] aValIndex Option value index.
   */
  string GetOptionName(const unsigned int aValIndex);

  /**
   * @brief Resets the option.
   * @details All the option values are removed. Fixed-size option values are set to default. Variable-size options are simply empty.
   */
  void Reset(void);

  /**
   * @brief Set option to default values.
   * @details For fixed-size options, all values are set to default. For variable-size options, the option is given one single value set to default.
   */
  void SetDefault(void);

private:

  bool provided;            ///< Set to true if the option was explicitly given.
  string tag;               ///< Option tag.
  string key;               ///< Option key.
  char valtype;             ///< Option value type.
  unsigned int osize;       ///< Option size (number of fixed values).
  int idef;                 ///< Option integer default value.
  unsigned int udef;        ///< Option unsigned integer default value.
  double ddef;              ///< Option double default value.
  string sdef;              ///< Option string default value.
  vector<int> ivec;         ///< Option integer vector.
  vector<unsigned int> uvec;///< Option unsigned integer vector.
  vector<double> dvec;      ///< Option double vector.
  vector<string> svec;      ///< Option string vector.
};


/**
 * @brief Interface to manage options.
 * @details This class offers a single interface to manage a list of options. A single option is described by the GwollumOption class.
 *
 * A set of options can be created "manually" by calling the Add() functions. Then, it is possible to parse a text file to get the values for the options with ReadTextFile().
 *
 * Another possibility is to call the ReadTree() or ReadChain() functions which downloads the option value from a TTree. In this case, the list of options is completely determined by the content of the TTree.
 *
 * A set of options can then be saved:
 * - as a text file with WriteTextFile()
 * - as a TTree with FillTree() / WriteTree()
 * - as a list of attributes in a hdf5 file with WriteH5()
 *
 * @author Florent Robinet
 */
class GwollumOptions{

 public:
  
  /**
   * @name Constructors and destructors
   @{
  */

  /**
   * @brief Constructor of the GwollumOptions class.
   */
  GwollumOptions(void);

  /**
   * @brief Destructor of the GwollumOptions class.
   */
  virtual ~GwollumOptions(void);

  /**
     @}
  */

  /**
   * @brief Copies an external GwollumOptions object into this.
   * @details Previous options are reset (as well as the TTree). New options are imported.
   * @param[in] aG GwollumOptions object to copy.
   */
  void Copy(GwollumOptions *aG);

  /**
   * @brief Resets the list of options.
   */
  inline void Reset(void){ options.clear(); OptTree->Reset(); };

  /**
   * @brief Returns the option index.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @returns The option index if found, the number of options otherwise.
   */
  inline unsigned int GetOptionIndex(const string aTag, const string aKey){
    for(unsigned int o=0; o<options.size(); o++)
      if((!options[o].GetTag().compare(aTag))&&(!options[o].GetKey().compare(aKey))) return o;
    return options.size();
  };

  /**
   * @brief Resets the list of option values.
   * @details All the option values are removed. Fixed-size option values are set to default. Variable-size options are simply empty.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  void ResetOption(const string aTag, const string aKey);

  /**
   * @brief Removes an option.
   * @details The option matching the input tag and key is removed from the list.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  void RemoveOption(const string aTag, const string aKey);

  /**
   * @brief Checks whether two GwollumOptions objects are identical.
   * @details They are said to be identical if:
   * - The number of options is the same.
   * - The options are defined with the same TAG/KEY/Type/Size.
   * - The options have the same number of values.
   *
   * @note The actual option values are not checked! They can differ.
   * @param[in] aG GwollumOptions object to test.
   * @returns true if the two GwollumOptions objects are identical.
   */
  bool IsTheSame(GwollumOptions *aG);

  /**
   * @brief Adds an option to the list (int).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  void Add(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Sets a value of an option (int).
   * @details This function can only be used to update an existing option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   * @param[in] aValue New value.
   */
  void SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const int aValue);

  /**
   * @brief Appends a value to a variable-size option (int).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValue New value to append.
   */
  void AddOptionValue(const string aTag, const string aKey, const int aValue);

  /**
   * @brief Adds an option to the list (unsigned int).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  void Add(const string aTag, const string aKey, const unsigned int aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Sets a value of an option (unsigned int).
   * @details This function can only be used to update an existing option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   * @param[in] aValue New value.
   */
  void SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const unsigned int aValue);

  /**
   * @brief Appends a value to a variable-size option (unsigned int).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValue New value to append.
   */
  void AddOptionValue(const string aTag, const string aKey, const unsigned int aValue);

  /**
   * @brief Adds an option to the list (double).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  void Add(const string aTag, const string aKey, const double aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Sets a value of an option (double).
   * @details This function can only be used to update an existing option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   * @param[in] aValue New value.
   */
  void SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const double aValue);

  /**
   * @brief Appends a value to a variable-size option (double).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValue New value to append.
   */
  void AddOptionValue(const string aTag, const string aKey, const double aValue);

  /**
   * @brief Adds an option to the list (string).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  void Add(const string aTag, const string aKey, const string aDefaultValue, const unsigned int aSize=0);

  /**
   * @brief Sets a value of an option (string).
   * @details This function can only be used to update an existing option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   * @param[in] aValue New value.
   */
  void SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const string aValue);

  /**
   * @brief Appends a value to a variable-size option (string).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValue New value to append.
   */
  void AddOptionValue(const string aTag, const string aKey, const string aValue);

  /**
   * @brief Checks whether an option was provided.
   * @returns true if this the case.
   * @sa GwollumOption::IsProvided().
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  bool IsProvided(const string aTag, const string aKey);

  /**
   * @brief Checks whether an option exists.
   * @returns true if this combination of TAG/KEY exists.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  inline bool Is(const string aTag, const string aKey){
    if(GetOptionIndex(aTag, aKey)==options.size()) return false;
    return true;
  };

  /**
   * @brief Sets an option to default value.
   * @sa GwollumOption::SetDefault().
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  void SetDefault(const string aTag, const string aKey);

  /**
   * @brief Returns an option tag.
   * @returns "" if the option index is out of range.
   * @param[in] aOptionIndex Option index.
   */
  inline string GetOptionTag(const unsigned int aOptionIndex){
    if(aOptionIndex>=options.size()) return "";
    return options[aOptionIndex].GetTag();
  };

  /**
   * @brief Returns an option type.
   * @returns '0' if the option index is out of range.
   * @param[in] aOptionIndex Option index.
   */
  inline char GetOptionType(const unsigned int aOptionIndex){
    if(aOptionIndex>=options.size()) return '0';
    return options[aOptionIndex].GetType();
  };

  /**
   * @brief Returns an option default value.
   * @param[in] aOptionIndex Option index.
   * @pre The option index must be valid.
   */
  inline int GetOptionDefaultI(const unsigned int aOptionIndex){
    return options[aOptionIndex].GetDefaultI();
  };

  /**
   * @brief Returns an option default value.
   * @param[in] aOptionIndex Option index.
   * @pre The option index must be valid.
   */
  inline unsigned int GetOptionDefaultU(const unsigned int aOptionIndex){
    return options[aOptionIndex].GetDefaultU();
  };

  /**
   * @brief Returns an option default value.
   * @param[in] aOptionIndex Option index.
   * @pre The option index must be valid.
   */
  inline double GetOptionDefaultD(const unsigned int aOptionIndex){
    return options[aOptionIndex].GetDefaultD();
  };

  /**
   * @brief Returns an option default value.
   * @param[in] aOptionIndex Option index.
   * @pre The option index must be valid.
   */
  inline string GetOptionDefaultS(const unsigned int aOptionIndex){
    return options[aOptionIndex].GetDefaultS();
  };

  /**
   * @brief Returns an option key.
   * @returns "" if the option index is out of range.
   * @param[in] aOptionIndex Option index.
   */
  inline string GetOptionKey(const unsigned int aOptionIndex){
    if(aOptionIndex>=options.size()) return "";
    return options[aOptionIndex].GetKey();
  };

  /**
   * @brief Returns an option size.
   * @param[in] aOptionIndex Option index.
   * @pre The option index must be valid.
   */
  inline unsigned int GetOptionSize(const unsigned int aOptionIndex){
    return options[aOptionIndex].GetSize();
  };

  /**
   * @brief Returns the option type.
   * @returns '0' if the TAG/KEY does not exist.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  char GetOptionType(const string aTag, const string aKey);

  /**
   * @brief Returns the option size.
   * @returns 0 if the TAG/KEY does not exist (note that this can be confused with a variable-size option!).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  unsigned int GetOptionSize(const string aTag, const string aKey);

  /**
   * @brief Returns the option number of values.
   * @returns 0 if the TAG/KEY does not exist.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  unsigned int GetOptionNumberOfValues(const string aTag, const string aKey);

  /**
   * @brief Returns an option value (int).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, false is returned. If the value index does not exist, false is returned.
   * @param[out] aValue Returned option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  bool GetOptionValueI(int &aValue, const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns an option value (int).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, 0 is returned. If the value index does not exist, the default value for this option is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  int GetOptionValueI(const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns the list of option values (int).
   * @details The option is identified by its tag and key words.
   * @warning If the option TAG/KEY does not exist, an empty vector is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  vector<int> GetOptionValuesI(const string aTag, const string aKey);

  /**
   * @brief Returns an option value (unsigned int).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, false is returned. If the value index does not exist, false is returned.
   * @param[out] aValue Returned option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  bool GetOptionValueU(unsigned int &aValue, const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns an option value (unsigned int).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, 0 is returned. If the value index does not exist, the default value for this option is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  unsigned int GetOptionValueU(const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns the list of option values (unsigned int).
   * @details The option is identified by its tag and key words.
   * @warning If the option TAG/KEY does not exist, an empty vector is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  vector<unsigned int> GetOptionValuesU(const string aTag, const string aKey);

  /**
   * @brief Returns an option value (double).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, false is returned. If the value index does not exist, false is returned.
   * @param[out] aValue Returned option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  bool GetOptionValueD(double &aValue, const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns an option value (double).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, 0 is returned. If the value index does not exist, the default value for this option is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  double GetOptionValueD(const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns the list of option values (double).
   * @details The option is identified by its tag and key words.
   * @warning If the option TAG/KEY does not exist, an empty vector is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  vector<double> GetOptionValuesD(const string aTag, const string aKey);

  /**
   * @brief Returns an option value (string).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, false is returned. If the value index does not exist, false is returned.
   * @param[out] aValue Returned option value.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  bool GetOptionValueS(string &aValue, const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns an option value (string).
   * @details The option is identified by its tag and key words. For this option, the value index must be provided.
   * @warning If the option TAG/KEY does not exist, an empty string is returned. If the value index does not exist, the default value for this option is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aValIndex Value index.
   */
  string GetOptionValueS(const string aTag, const string aKey, const unsigned int aValIndex);

  /**
   * @brief Returns the list of option values (string).
   * @details The option is identified by its tag and key words.
   * @warning If the option TAG/KEY does not exist, an empty vector is returned.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   */
  vector<string> GetOptionValuesS(const string aTag, const string aKey);

  /**
   * @brief Reads an option file and extract the list of options.
   * @details The input text file is scanned with the IO class. All the key/tag options defined until now are extracted:
   * - vector of parameters are downloaded according to the defined type.
   * - if an option is not given in the option file, the default value is used.
   *
   * @note If the input option file does not exist, this function returns immediately.
   * @param[in] aOptionFile Path to the option file.
   * @returns The number of options.
   */
  unsigned int ReadTextFile(const string aOptionFile);

  /**
   * @brief Writes all options in a text file.
   * @param[in] aFileName Path to the output text file.
   */
  void WriteTextFile(const string aFileName);

  /**
   * @brief Writes the options as attributes in a hdf5 file.
   * @param[in] aFile Pointer to an open Hdf5 file.
   * @param[in] aDataSpace Data space for the attributes.
   */
  void WriteH5(H5::H5File* aFile, H5::DataSpace &aDataSpace);

  /**
   * @brief Reads options from a TTree in a ROOT file.
   * @returns true if the tree was correctly parsed and options were extracted.
   * @param[in] aRootFileName Path to the ROOT file.
   * @param[in] aTreeName Option TTree name.
   */
  bool ReadTree(const string aRootFileName, const string aTreeName);

  /**
   * @brief Reads options from a TTree.
   * @param[in] aTree Pointer to the option TTree.
   */
  bool ReadTree(TTree *aTree);

  /**
   * @brief Reads options from a TChain.
   * @details The first tree in the TChain is used to define the option set. Then, the next trees in the TChain are scanned and tested against the option set with IsTheSame(). If the list of options (not the values!) are consistent across the TChain, the option set is said to be uniform. The result of this check is returned.
   * @returns true if the option set was successfully extracted from the first tree in the chain. 
   * @param[out] aIsUniform Flag set to true if the list of options (not the values!) are uniform across the TChain.
   * @param[in] aChain Pointer to the option TChain.
   */
  bool ReadChain(bool &aIsUniform, TChain *aChain);

  /**
   * @brief Saves options in a TTree.
   * @details The options are saved in a TTree. Call this function when all options have been set. Then, the option TTree can be saved in a ROOT file by calling WriteTree().
   * @note Call this function only once, after all options are set.
   * @warning If a variable-size option is empty, it will not be saved in the TTree. You need to have at least one option value. You can also call SetDefault() to fill the option with a single default value.
   */
  void FillTree(void);

  /**
   * @brief Writes the option TTree in a TFile.
   * @note The FillTree() must be called first to fill the TTree with options.
   * @param[in] aTreeName TTree name.
   * @param[in] aRootFileName Path to the ROOT file.
   * @param[in] aSubDirectory Sub-directory name.
   */
  void WriteTree(const string aTreeName, const string aRootFileName, const string aSubDirectory="");

  /**
   * @brief Writes the option TTree in a TFile.
   * @note The FillTree() must be called first to fill the TTree with options.
   * @param[in] aTreeName TTree name.
   * @pre A TFile must be open before calling this function. The TTree::Write() function is called here.
   */
  inline void WriteTree(const string aTreeName){ OptTree->Write(aTreeName.c_str()); };

  /**
   * @brief Returns the number of options.
   */
  unsigned int GetOptionsN(void){ return options.size(); };


 private:

  Monitor *mon;                  ///< Class monitor.
  vector<GwollumOption> options; ///< List of options.
  TTree *OptTree;                ///< Option TTree.
  string t_tag;                  ///< Option TTree: tag.
  string t_key;                  ///< Option TTree: key.
  char t_type;                   ///< Option TTree: type.
  unsigned int t_size;           ///< Option TTree: size.
  int t_idef;                    ///< Option TTree: default (int).
  unsigned int t_udef;           ///< Option TTree: default (unsigned int).
  double t_ddef;                 ///< Option TTree: default (double).
  string t_sdef;                 ///< Option TTree: default (string).
  int t_ival;                    ///< Option TTree: value (int).
  unsigned int t_uval;           ///< Option TTree: value (unsigned int).
  double t_dval;                 ///< Option TTree: value (double).
  string t_sval;                 ///< Option TTree: value (string).


  ClassDef(GwollumOptions,0)
};


#endif


