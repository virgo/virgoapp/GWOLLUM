/**
 * @file 
 * @brief See IO.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "IO.h"

//////////////////////////////////////////////////////////////////////////////
IO::IO(const char* filepattern): fCurline(0){
//////////////////////////////////////////////////////////////////////////////

  mon = new Monitor("io");
  
  // loop on all files matching pattern, store each non empty line in 
  // fAllLines for efficient searches later.
  vector<string> files = Glob(filepattern);
  for(unsigned int i=0; i<files.size(); i++)
    ParseFile(files[i].c_str());
}

//////////////////////////////////////////////////////////////////////////////
bool IO::ParseFile(const char* filename) {
//////////////////////////////////////////////////////////////////////////////

  ifstream in(filename);
  if(!in.good()){
    mon->SetMessageType(monitor_warning);
    *mon<<"IO::"<<__func__<<": the option file "<<filename<<" cannot be read"<<endl;
    return false;
  }

  char data[sLinesize];
  char s[sLinesize];
  int pos=0;

  do{ 
    in.seekg(pos);
    in.getline(s,sLinesize);
    
    pos = in.tellg();     
    
    if(string(s).empty()) {
      continue; // remove empty lines
    }

    istringstream lin(s);  
    
    string tag;
    lin>>tag;
    
    if(!strncmp(tag.c_str(),"/*",2)) continue; // remove commented lines
    if(!strncmp(tag.c_str(),"//",2)) continue; // remove commented lines
    if(!strncmp(tag.c_str(),"%",1)) continue;  // remove commented lines
    if(!strncmp(tag.c_str(),"#",1)) continue;  // remove commented lines
    
    lin.get(data,sLinesize);
    
    fAllLines.push_back(pair<string, string>(tag, data));
  } while(in.good());
  
  if(in.eof()) return true;
  else{
    mon->SetMessageType(monitor_error);
    *mon<<"IO::"<<__func__<<": the option file "<<filename<<" is corrupted"<<endl;
    return false;
  }
}

//////////////////////////////////////////////////////////////////////////////
IO::~IO(void){
//////////////////////////////////////////////////////////////////////////////
  fAllLines.clear();
  delete mon;
}

//////////////////////////////////////////////////////////////////////////////
void IO::Dump(ostream& out) const {
//////////////////////////////////////////////////////////////////////////////
  for (unsigned i=0; i<fAllLines.size(); i++)
    out<<fAllLines[i].first<< "\t" << fAllLines[i].second << endl; 
}

//////////////////////////////////////////////////////////////////////////////
ostream& operator<<(ostream& out, IO& io){
//////////////////////////////////////////////////////////////////////////////
  if(!out) return out;
  io.Dump(out);
  return out;
}

//////////////////////////////////////////////////////////////////////////////
bool IO::GetOpt(const char* tag, const char* key, string& value) const{
//////////////////////////////////////////////////////////////////////////////
  string data = GetLineData(tag,key);
  
  char cstr[sLinesize];
  istringstream in(data.c_str());  
  in.get(cstr,sLinesize);
  
  value = cstr;
  if(!value.empty()){
    int pos = value.find_first_not_of(" \t");
    value = value.substr(pos);
  }
  return !value.empty();
}

//////////////////////////////////////////////////////////////////////////////
string IO::GetLineData(const char* tag, const char* key) const {
//////////////////////////////////////////////////////////////////////////////
  // if tag matches several option lines, data is the data corresponding
  // to the last tag

  char data[sLinesize];
  bool found = false;
  for(unsigned i=0; i<fAllLines.size(); i++){
    if( !fnmatch(fAllLines[i].first.c_str(), tag, 0) ){ 
      istringstream in(fAllLines[i].second.c_str());
      string readkey; in>>readkey;
      
      if(readkey == key){
	found=true;
	in.get(data,sLinesize);
      }
    }
  }
  if(found){
    // trailing spaces may be present at the end of "data"
    // remove them : 
    string str(data);
    int pos = str.find_last_not_of(" ");
    str.replace(pos+1, str.size(),"");
    return str;
  }
  else return string();
}

//////////////////////////////////////////////////////////////////////////////
string IO::GetNextLineData(const char* tag, const char* key){
//////////////////////////////////////////////////////////////////////////////

  if(fCurtag != tag || fCurkey != key){
    // not the same request
    fCurline = 0;
    fCurtag = tag;
    fCurkey = key;
  }

  char data[sLinesize];
  bool found = false;
  for(unsigned int i=fCurline; i<fAllLines.size(); i++){
    if( !fnmatch(fAllLines[i].first.c_str(), tag, 0) ){ 
      istringstream in(fAllLines[i].second.c_str());
      string readkey; in>>readkey;
      
      if(readkey == key){
	found=true;
	in.get(data,sLinesize);
	fCurline=i+1;
	break;
      }
    }
  }
  if(found) return string(data);
  return "";
}






















