/**
 * @file 
 * @brief Process monitoring.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Monitor__
#define __Monitor__
#include "CUtils.h"

using namespace std;

/**
 * @brief Enumerator to describe the different types of messages.
 * @sa PrintMessage()
 */
enum MonitorMessageType{
  monitor_status, ///< Status message (verbosity >=1).
  monitor_info_2, ///< Nominal message (verbosity >=2): function call.
  monitor_info_3, ///< Nominal message (verbosity >=3): function tasks.
  monitor_info_4, ///< Nominal message (verbosity >=4): debugging.
  monitor_info_5, ///< Nominal message (verbosity >=5).
  monitor_fatal,  ///< Fatal message.
  monitor_error,  ///< Error message.
  monitor_warning,///< Warning message.
  monitor_n       ///< Number of message types
};

/**
 * @brief Monitor a GWOLLUM processing.
 * @details This class is designed to monitor a GWOLLUM processing. It manages:
 * - The verbosity level.
 * - The production of messages.
 *
 * The verbosity level must be set with SetVerbosityLevel(). It defines the quantity of information printed in the standard C error output stream `stdout`. Before printing a message, the message type must be defined with SetMessageType(). Then use the `<<` operator to output the message.
 *
 * Similarly error message are printed in the standard C error output stream `stderr`.
 * @sa MonitorMessageType for the message definition.
 */
class Monitor: private std::streambuf, public std::ostream{
  
public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
  /**
   * @brief Constructor of the Monitor class.
   * @param[in] aProcessName Process name.
   */
  Monitor(const string aProcessName);
  
  /**
   * @brief Destructor of the Monitor class.
   */
  virtual ~Monitor(void);
  
  /**
     @}
  */
  
  /**
   * @brief Sets the verbosity level.
   * @details Verbosity levels:
   * - 0: no prints.
   * - 1: MonitorMessageType::monitor_info_1.
   * - 2: MonitorMessageType::monitor_info_2.
   * - 3: MonitorMessageType::monitor_info_3.
   *
   * @param[in] aVerbosity Verbosity level.
   */
  inline void SetVerbosityLevel(const unsigned int aVerbosity){ verbosity = aVerbosity; };

  /**
   * @brief Returns the verbosity level.
   */
  inline unsigned int GetVerbosityLevel(void){ return verbosity; };
  
  /**
   * @brief Sets the message type.
   * @param[in] aMessageType Message type.
   */
  inline void SetMessageType(const MonitorMessageType aMessageType){ message_type = aMessageType;
  };

  /**
   * @brief Returns the current processing duration [s].
   */
  inline unsigned int GetProcessingTime(void){
    time(&timer);
    return (unsigned int)(timer-timer_start);
  }

  /**
   * @brief Returns the current processing duration as a string.
   */
  string GetProcessingTimeString(void);

  /**
   * @brief Returns the current process date (UTC) as a formatted string.
   * @details Formatted string: "YYYY-MM-DD, hh:mm:ss UTC"
   */
  string GetProcessDate(void);

  /**
   * @brief Returns the object status.
   */
  inline bool GetStatus(void){ return status; };

  /**
   * @brief Sets a new object status.
   * @param[in] aNewStatus New object status.
   */
  inline void SetStatus(const bool aNewStatus){ status = aNewStatus; };

  /**
   * @brief Updates the object status.
   * @param[in] aStatus Status update.
   */
  inline void UpdateStatus(const bool aStatus){ status = status && aStatus; };

private:
  
  unsigned int verbosity;          ///< Verbosity level.
  bool status;                     ///< Object status.
  string procname;                 ///< Process name

  struct tm *ptm;                  ///< GMT time.
  time_t timer;                    ///< Timer.
  time_t timer_start;              ///< Timer start.

  string message;                  ///< Message.
  MonitorMessageType message_type; ///< Message type.

  /**
   * @brief Prints the current message.
   * @details Error/Fatal/Warning messages are always printed in the standard C error output stream `stderr`.
   * Nominal messages are printed in the standard C error output stream `stdout` depending on the verbosity level.
   * 
   */
  void PrintMessage(void);
  
  /**
   * @brief Overrides the std::basic_streambuf::overflow() function.
   * @param[in] aChar Character.
   */
  int overflow(int aChar) override;
  
  /**
   * @brief Builds the message one character at a time.
   * @details The message is printed (PrintMessage()) when a new line is detected.
   * @param[in] aChar Character.
   */
  void MakeMessage(char aChar);
    
};


#endif


