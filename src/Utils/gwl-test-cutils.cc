/**
 * @file 
 * @brief Program to test the CUtils functions.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "CUtils.h"

using namespace std;

/**
 * @brief Test program.
 */
int main(void){

  //*********************************************************************
  // IsPowerOfTwo(unsigned int x)
  //*********************************************************************
  cout<<"\nIsPowerOfTwo(unsigned int x)"<<endl;
  if(IsPowerOfTwo(0)==true) return 1;
  if(IsPowerOfTwo(1)==false) return 1;
  if(IsPowerOfTwo(2)==false) return 1;
  if(IsPowerOfTwo(3)==true) return 1;
  if(IsPowerOfTwo(4)==false) return 1;
  if(IsPowerOfTwo(128)==false) return 1;
  if(IsPowerOfTwo(12345)==true) return 1;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // NextPowerOfTwo(double x)
  //*********************************************************************
  cout<<"\nNextPowerOfTwo(double x)"<<endl;
  if(NextPowerOfTwo(0)!=0) return 2;
  if(NextPowerOfTwo(1)!=1) return 2;
  if(NextPowerOfTwo(2)!=2) return 2;
  if(NextPowerOfTwo(3)!=4) return 2;
  if(NextPowerOfTwo(4)!=4) return 2;
  if(NextPowerOfTwo(5)!=8) return 2;
  if(NextPowerOfTwo(110)!=128) return 2;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // SplitString(const string stringtodivide, const char separator=' ')
  //*********************************************************************
  cout<<"\nSplitString(const string stringtodivide, const char separator=' ')"<<endl;
  vector<string> stringtest;
  stringtest = SplitString("test1", ' ');
  if(stringtest.size()!=1) return 3;
  if(stringtest[0].compare("test1")) return 3;
  stringtest = SplitString(" test1 test2 ", ' ');
  if(stringtest.size()!=2) return 3;
  if(stringtest[0].compare("test1")) return 3;
  if(stringtest[1].compare("test2")) return 3;
  stringtest = SplitString("test1?test2?test3 ", '?');
  if(stringtest.size()!=3) return 3;
  if(stringtest[0].compare("test1")) return 3;
  if(stringtest[1].compare("test2")) return 3;
  if(stringtest[2].compare("test3 ")) return 3;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // StringToUpper(string stringtoconvert)
  //*********************************************************************
  cout<<"\nStringToUpper(string stringtoconvert)"<<endl;
  if(StringToUpper("abcd").compare("ABCD")) return 4;
  if(StringToUpper("aBcD").compare("ABCD")) return 4;
  if(StringToUpper("aBcD;eFgH").compare("ABCD;EFGH")) return 4;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // GetFileNameFromPath(const string filepathname)
  //*********************************************************************
  cout<<"\nGetFileNameFromPath(const string filepathname)"<<endl;
  if(GetFileNameFromPath("/test1/test2/test3/test4.txt").compare("test4.txt")) return 5;
  if(GetFileNameFromPath("test4.txt").compare("test4.txt")) return 5;
  if(GetFileNameFromPath("./test4.txt").compare("test4.txt")) return 5;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // ReplaceAll(string str, const string& from, const string& to)
  //*********************************************************************
  cout<<"\nReplaceAll(string str, const string& from, const string& to)"<<endl;
  if(ReplaceAll("This is a string", "i", "a").compare("Thas as a strang")) return 6;
  if(ReplaceAll("This is a string", "T", "H").compare("Hhis is a string")) return 6;
  if(ReplaceAll("This is a string", " ", "").compare("Thisisastring")) return 6;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // CreateDirectory(error_code& aErrorCode, const string aPath)
  //*********************************************************************
  cout<<"\nCreateDirectory(error_code& aErrorCode, const string aPath)"<<endl;
  error_code ec;
  RemoveFileOrDirectory(ec, "./CreateDirectory1");
  if(CreateDirectory(ec, "./CreateDirectory1")==false) return 7;
  if(CreateDirectory(ec, "./CreateDirectory1")==false) return 7;
  if(CreateDirectory(ec, "./CreateDirectory1/CreateDirectory2/CreateDirectory3")==false) return 7;
  if(CreateDirectory(ec, "./CreateDirectory1/CreateDirectory2/CreateDirectory3")==false) return 7;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // RemoveFileOrDirectory(error_code& aErrorCode, const string aPath)
  //*********************************************************************
  cout<<"\nRemoveFileOrDirectory(error_code& aErrorCode, const string aPath)"<<endl;
  if(RemoveFileOrDirectory(ec, "./CreateDirectory1/CreateDirectory2/CreateDirectory3")!=1) return 8;
  if(RemoveFileOrDirectory(ec, "./CreateDirectory1")!=2) return 8;
  if(RemoveFileOrDirectory(ec, "./CreateDirectory1")!=0) return 8;
  cout<<"\tOK"<<endl;

  //*********************************************************************
  // CopyFile(error_code& aErrorCode, const string aFrom, const string aTo, const bool aOverwrite)
  //*********************************************************************
  cout<<"\nCopyFile(error_code& aErrorCode, const string aFrom, const string aTo, const bool aOverwrite)"<<endl;
  if(CopyFile(ec, "/this/should/not/exist", "/whatever", false)==true) return 9;
  if(CopyFile(ec, ".", "/whatever", false)==true) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./whatever/gwl-test-cutils", false)==true) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./whatever/gwl-test-cutils", true)==true) return 9;
  if(CreateDirectory(ec, "./CreateDirectory1/CreateDirectory2/CreateDirectory3")==false) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3", false)==false) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3", false)==true) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3", true)==false) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3/gwl-test-cutils", false)==true) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3/gwl-test-cutils", true)==false) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3/CreateDirectory4/gwl-test-cutils", false)==true) return 9;
  if(CopyFile(ec, "./gwl-test-cutils", "./CreateDirectory1/CreateDirectory2/CreateDirectory3/CreateDirectory4/gwl-test-cutils", true)==true) return 9;
  if(RemoveFileOrDirectory(ec, "./CreateDirectory1")!=4) return 9;
  cout<<"\tOK"<<endl;

  return 0;
}

