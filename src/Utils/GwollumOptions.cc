/**
 * @file 
 * @brief See GwollumOptions.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "GwollumOptions.h"

ClassImp(GwollumOptions)

////////////////////////////////////////////////////////////////////////////////////
GwollumOption::GwollumOption(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////

  // defalt values
  provided = false;
  tag = ReplaceAll(aTag, "_", "");
  key = ReplaceAll(aKey, "_", "");
  valtype='i';
  idef = aDefaultValue;
  udef = 0;
  ddef = 0.0;
  sdef = "";
  osize = aSize;
  ivec.assign(osize, aDefaultValue);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumOption::GwollumOption(const string aTag, const string aKey, const unsigned int aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////

  // defalt values
  provided = false;
  tag = ReplaceAll(aTag, "_", "");
  key = ReplaceAll(aKey, "_", "");;
  valtype='u';
  idef = 0;
  udef = aDefaultValue;
  ddef = 0.0;
  sdef = "";
  osize = aSize;
  uvec.assign(osize, aDefaultValue);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumOption::GwollumOption(const string aTag, const string aKey, const double aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////

  // defalt values
  provided = false;
  tag = ReplaceAll(aTag, "_", "");
  key = ReplaceAll(aKey, "_", "");;
  valtype='d';
  idef = 0;
  udef = 0;
  ddef = aDefaultValue;
  sdef = "";
  osize = aSize;
  dvec.assign(osize, aDefaultValue);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumOption::GwollumOption(const string aTag, const string aKey, const string aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////

  // defalt values
  provided = false;
  tag = ReplaceAll(aTag, "_", "");
  key = ReplaceAll(aKey, "_", "");;
  valtype='s';
  idef = 0;
  udef = 0;
  ddef = 0.0;
  sdef = aDefaultValue;
  osize = aSize;
  svec.assign(osize, aDefaultValue);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumOption::~GwollumOption(void){
////////////////////////////////////////////////////////////////////////////////////
  ivec.clear();
  uvec.clear();
  dvec.clear();
  svec.clear();
}

////////////////////////////////////////////////////////////////////////////////////
string GwollumOption::GetOptionName(const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  gwl_ss.clear(); gwl_ss.str("");
  gwl_ss<<tag<<"_"<<key<<"_"<<aValIndex<<"_"<<osize;
  string sret = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  return sret;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOption::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  provided = false;
  ivec.clear();
  uvec.clear();
  dvec.clear();
  svec.clear();
  if(valtype=='i') ivec.assign(osize, idef);
  else if(valtype=='u') uvec.assign(osize, udef);
  else if(valtype=='d') dvec.assign(osize, ddef);
  else svec.assign(osize, sdef);
  return ;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOption::SetDefault(void){
////////////////////////////////////////////////////////////////////////////////////

  // fix-size option
  if(osize>0){
    if(valtype=='i') for(unsigned int v=0; v<ivec.size(); v++) ivec[v] = idef;
    else if(valtype=='u') for(unsigned int v=0; v<uvec.size(); v++) uvec[v] = udef;
    else if(valtype=='d') for(unsigned int v=0; v<dvec.size(); v++) dvec[v] = ddef;
    else for(unsigned int v=0; v<svec.size(); v++) svec[v] = sdef;
    provided = false;
  }

  // variable-size option
  else{
    ivec.clear();
    uvec.clear();
    dvec.clear();
    svec.clear();
    if(valtype=='i') ivec.push_back(idef);
    else if(valtype=='u') uvec.push_back(udef);
    else if(valtype=='d') dvec.push_back(ddef);
    else svec.push_back(sdef);
    provided = false;
  }
  
  return;
}





////////////////////////////////////////////////////////////////////////////////////
GwollumOptions::GwollumOptions(void){
////////////////////////////////////////////////////////////////////////////////////
  options.clear();

  mon = new Monitor("gwoptions");

  OptTree = new TTree("gwoptions", "gwoptions");
  OptTree->SetDirectory(0);
  OptTree->Branch("tag", &t_tag);
  OptTree->Branch("key", &t_key);
  OptTree->Branch("type", &t_type, "type/C");
  OptTree->Branch("size", &t_size, "size/i");
  OptTree->Branch("idef", &t_idef, "idef/I");
  OptTree->Branch("udef", &t_udef, "udef/i");
  OptTree->Branch("ddef", &t_ddef, "ddef/D");
  OptTree->Branch("sdef", &t_sdef);
  OptTree->Branch("ival", &t_ival, "ival/I");
  OptTree->Branch("uval", &t_uval, "uval/i");
  OptTree->Branch("dval", &t_dval, "dval/D");
  OptTree->Branch("sval", &t_sval);
}

////////////////////////////////////////////////////////////////////////////////////
GwollumOptions::~GwollumOptions(void){
////////////////////////////////////////////////////////////////////////////////////
  options.clear();
  delete OptTree;
  delete mon;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::Copy(GwollumOptions *aG){
////////////////////////////////////////////////////////////////////////////////////
  GwollumOptions::Reset();
  options = aG->options;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::ResetOption(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Reset();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::RemoveOption(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options.erase(options.begin()+oindex);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::IsTheSame(GwollumOptions *aG){
////////////////////////////////////////////////////////////////////////////////////

  // test the number of options
  if(options.size()!=aG->GetOptionsN()) return false;

  // loop over options
  unsigned int oext = 0;
  for(unsigned int o=0; o<options.size(); o++){

    // check external options
    for(oext=0; oext<aG->GetOptionsN(); oext++){
      if(aG->options[oext].GetTag().compare(options[o].GetTag())) continue;
      if(aG->options[oext].GetKey().compare(options[o].GetKey())) continue;
      if(aG->options[oext].GetSize()!=options[o].GetSize()) continue;
      if(aG->options[oext].GetNumberOfValues()!=options[o].GetNumberOfValues()) continue;
      break;
    }

    // no matching option
    if(oext==aG->GetOptionsN()) return false;
  }

  // all options have a match
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::Add(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////
  options.push_back(GwollumOption(aTag, aKey, aDefaultValue, aSize));
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const int aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Set(aValIndex, aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::AddOptionValue(const string aTag, const string aKey, const int aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Add(aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::Add(const string aTag, const string aKey, const unsigned int aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////
  options.push_back(GwollumOption(aTag, aKey, aDefaultValue, aSize));
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const unsigned int aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Set(aValIndex, aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::AddOptionValue(const string aTag, const string aKey, const unsigned int aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Add(aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::Add(const string aTag, const string aKey, const double aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////
  options.push_back(GwollumOption(aTag, aKey, aDefaultValue, aSize));
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const double aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Set(aValIndex, aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::AddOptionValue(const string aTag, const string aKey, const double aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Add(aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::Add(const string aTag, const string aKey, const string aDefaultValue, const unsigned int aSize){
////////////////////////////////////////////////////////////////////////////////////
  options.push_back(GwollumOption(aTag, aKey, aDefaultValue, aSize));
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::SetOptionValue(const string aTag, const string aKey, const unsigned int aValIndex, const string aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Set(aValIndex, aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::AddOptionValue(const string aTag, const string aKey, const string aValue){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].Add(aValue);
  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::IsProvided(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return 0;
  return options[oindex].IsProvided();
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::SetDefault(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return;
  options[oindex].SetDefault();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
char GwollumOptions::GetOptionType(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return '0';
  return options[oindex].GetType();
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int GwollumOptions::GetOptionSize(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return 0;
  return options[oindex].GetSize();
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int GwollumOptions::GetOptionNumberOfValues(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return 0;
  return options[oindex].GetNumberOfValues();
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::GetOptionValueI(int &aValue, const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return false;// option does not exist
  return options[oindex].GetValueI(aValue, aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
int GwollumOptions::GetOptionValueI(const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return 0;// option does not exist
  return options[oindex].GetValueI(aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
vector<int> GwollumOptions::GetOptionValuesI(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  vector<int> vnull;
  if(oindex>=options.size()) return vnull;// option does not exist
  return options[oindex].GetValuesI();
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::GetOptionValueU(unsigned int &aValue, const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return false;// option does not exist
  return options[oindex].GetValueU(aValue, aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int GwollumOptions::GetOptionValueU(const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return 0;// option does not exist
  return options[oindex].GetValueU(aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
vector<unsigned int> GwollumOptions::GetOptionValuesU(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  vector<unsigned int> vnull;
  if(oindex>=options.size()) return vnull;// option does not exist
  return options[oindex].GetValuesU();
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::GetOptionValueD(double &aValue, const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return false;// option does not exist
  return options[oindex].GetValueD(aValue, aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
double GwollumOptions::GetOptionValueD(const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return 0.0;// option does not exist
  return options[oindex].GetValueD(aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
vector<double> GwollumOptions::GetOptionValuesD(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  vector<double> vnull;
  if(oindex>=options.size()) return vnull;// option does not exist
  return options[oindex].GetValuesD();
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::GetOptionValueS(string &aValue, const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return false;// option does not exist
  return options[oindex].GetValueS(aValue, aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
string GwollumOptions::GetOptionValueS(const string aTag, const string aKey, const unsigned int aValIndex){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  if(oindex>=options.size()) return "";// option does not exist
  return options[oindex].GetValueS(aValIndex);
}

////////////////////////////////////////////////////////////////////////////////////
vector<string> GwollumOptions::GetOptionValuesS(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int oindex = GetOptionIndex(aTag, aKey);
  vector<string> vnull;
  if(oindex>=options.size()) return vnull;// option does not exist
  return options[oindex].GetValuesS();
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int GwollumOptions::ReadTextFile(const string aOptionFile){
////////////////////////////////////////////////////////////////////////////////////

  // option parser
  IO *io = new IO(aOptionFile.c_str());
  if(io->IsZombie()){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": the option file "<<aOptionFile<<" cannot be parsed"<<endl;
    delete io;
    return 0;
  }

  // vector of options
  vector<int> ivec;
  vector<unsigned int> uvec;
  vector<double> dvec;
  vector<string> svec;
  
  for(unsigned int o=0; o<options.size(); o++){

    //******** int
    if(options[o].GetType()=='i'){
      if(io->GetAllOpt(options[o].GetTag().c_str(), options[o].GetKey().c_str(), ivec)){
        if(options[o].GetSize()==0) for(unsigned int v=0; v<ivec.size(); v++) options[o].Add(ivec[v]);
        else for(unsigned int v=0; v<ivec.size(); v++) options[o].Set(v,ivec[v]);
      }
      else options[o].SetDefault();
      ivec.clear();
    }
    
    //******** unsigned int
    else if(options[o].GetType()=='u'){
      if(io->GetAllOpt(options[o].GetTag().c_str(), options[o].GetKey().c_str(), uvec)){
        if(options[o].GetSize()==0) for(unsigned int v=0; v<uvec.size(); v++) options[o].Add(uvec[v]);
        else for(unsigned int v=0; v<uvec.size(); v++) options[o].Set(v, uvec[v]);
      }
      else options[o].SetDefault();
      uvec.clear();
    }

    //******** double
    else if(options[o].GetType()=='d'){
      if(io->GetAllOpt(options[o].GetTag().c_str(), options[o].GetKey().c_str(), dvec)){
        if(options[o].GetSize()==0) for(unsigned int v=0; v<dvec.size(); v++) options[o].Add(dvec[v]);
        else for(unsigned int v=0; v<dvec.size(); v++) options[o].Set(v, dvec[v]);
      }
      else options[o].SetDefault();
      dvec.clear();
    }

    //******** string
    else{
      if(io->GetAllOpt(options[o].GetTag().c_str(), options[o].GetKey().c_str(), svec)){
        if(options[o].GetSize()==0) for(unsigned int v=0; v<svec.size(); v++) options[o].Add(svec[v]);
        else for(unsigned int v=0; v<svec.size(); v++) options[o].Set(v, svec[v]);
      }
      else options[o].SetDefault();
      svec.clear();
    }
  }

  delete io;
  return GetOptionsN();
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::WriteTextFile(const string aFileName){
////////////////////////////////////////////////////////////////////////////////////

  ofstream ofs(aFileName.c_str(), ofstream::out);
  if(!ofs.is_open()) return;

  // loop over options
  for(unsigned int o=0; o<options.size(); o++){


    // fixed option --> use single line
    if(options[o].GetSize()>0){
      ofs<<options[o].GetTag()<<" "<<options[o].GetKey();
      if(options[o].GetType()=='i'){
        for(unsigned int v=0; v<options[o].GetSize(); v++) ofs<<" "<<options[o].GetValueI(v);
      }
      else if(options[o].GetType()=='u'){
        for(unsigned int v=0; v<options[o].GetSize(); v++) ofs<<" "<<options[o].GetValueU(v);
      }
      else if(options[o].GetType()=='d'){
        for(unsigned int v=0; v<options[o].GetSize(); v++) ofs<<" "<<options[o].GetValueD(v);
      }
      else{
        for(unsigned int v=0; v<options[o].GetSize(); v++) ofs<<" "<<options[o].GetValueS(v);
      }
      ofs<<endl<<endl;
    }

    // variable option --> use multiple lines
    else{
      if(options[o].GetType()=='i'){
        for(unsigned int v=0; v<options[o].GetNumberOfValues(); v++) ofs<<options[o].GetTag()<<" "<<options[o].GetKey()<<" "<<options[o].GetValueI(v)<<endl;
      }
      else if(options[o].GetType()=='u'){
        for(unsigned int v=0; v<options[o].GetNumberOfValues(); v++) ofs<<options[o].GetTag()<<" "<<options[o].GetKey()<<" "<<options[o].GetValueU(v)<<endl;
      }
      else if(options[o].GetType()=='d'){
        for(unsigned int v=0; v<options[o].GetNumberOfValues(); v++) ofs<<options[o].GetTag()<<" "<<options[o].GetKey()<<" "<<options[o].GetValueD(v)<<endl;
      }
      else{
        for(unsigned int v=0; v<options[o].GetNumberOfValues(); v++) ofs<<options[o].GetTag()<<" "<<options[o].GetKey()<<" "<<options[o].GetValueS(v)<<endl;
      }
      ofs<<endl;

    }
  }

  ofs.close();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::WriteH5(H5::H5File* aFile, H5::DataSpace &aDataSpace){
////////////////////////////////////////////////////////////////////////////////////

  string optname;
  int ival;
  unsigned int uval;
  double dval;
  string sval;
  H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters

  for(unsigned int o=0; o<options.size(); o++){

    // loop over values
    for(unsigned int v=0; v<options[o].GetNumberOfValues(); v++){

      // option name
      optname = options[o].GetOptionName(v);
      
      // save option value
      if(options[o].GetType()=='i'){
        ival = options[o].GetValueI(v);
        aFile->createAttribute(optname, H5::PredType::NATIVE_INT, aDataSpace).write(H5::PredType::NATIVE_INT, &ival);
      }
      else if(options[o].GetType()=='u'){
        uval = options[o].GetValueU(v);
        aFile->createAttribute(optname, H5::PredType::NATIVE_UINT, aDataSpace).write(H5::PredType::NATIVE_UINT, &uval);
      }
      else if(options[o].GetType()=='d'){
        dval = options[o].GetValueD(v);
        aFile->createAttribute(optname, H5::PredType::NATIVE_DOUBLE, aDataSpace).write(H5::PredType::NATIVE_DOUBLE, &dval);
      }
      else{
        sval = options[o].GetValueS(v);
        aFile->createAttribute(optname, strdatatype, aDataSpace).write(strdatatype, sval);
      }
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::ReadTree(const string aRootFileName, const string aTreeName){
////////////////////////////////////////////////////////////////////////////////////

  // open root file
  TFile *fin = new TFile(aRootFileName.c_str(), "read");
  if(fin->IsZombie()) return false;
  fin->cd();

  // get ttree
  TTree *tree = NULL;
  fin->GetObject(aTreeName.c_str(), tree);
  if(tree==NULL){ fin->Close(); return false; }

  // read tree
  if(!ReadTree(tree)) return false;

  delete tree;
  fin->Close();
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::ReadTree(TTree *aTree){
////////////////////////////////////////////////////////////////////////////////////

  // no data --> nothing to do
  if(aTree->GetEntries()==0) return true;
  
  // use string pointer to attach branches
  string *t_tag_tmp=0, *t_key_tmp=0, *t_sdef_tmp=0, *t_sval_tmp=0;
  
  // attach branches
  if(aTree->SetBranchAddress("tag", &t_tag_tmp) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing tag branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("key", &t_key_tmp) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing key branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("type", &t_type) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing type branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("size", &t_size) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing size branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("idef", &t_idef) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing idef branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("udef", &t_udef) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing udef branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("ddef", &t_ddef) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing ddef branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("sdef", &t_sdef_tmp) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing sdef branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("ival", &t_ival) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing ival branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("uval", &t_uval) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing uval branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("dval", &t_dval) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing dval branch"<<endl;
    return false;
  }
  if(aTree->SetBranchAddress("sval", &t_sval_tmp) != 0){
    mon->SetMessageType(monitor_error);
    *mon<<"GwollumOptions::"<<__func__<<": missing sval branch"<<endl;
    return false;
  }

  // read tree and fill options
  // entries are assumed to be sorted by tag/key
  string tag_prev = "";
  string key_prev = "";
  unsigned int val_index = 0;
  for(unsigned int t=0; t<aTree->GetEntries(); t++){
    aTree->GetEntry(t);

    // same option
    if((!(t_tag_tmp->compare(tag_prev)))&&(!(t_key_tmp->compare(key_prev)))){
      // add value
      if(t_type=='i'){
        if(t_size==0) options.back().Add(t_ival);// variable-size
        else options.back().Set(val_index, t_ival);// fixed-size
      }
      if(t_type=='u'){
        if(t_size==0) options.back().Add(t_uval);// variable-size
        else options.back().Set(val_index, t_uval);// fixed-size
      }
      if(t_type=='d'){
        if(t_size==0) options.back().Add(t_dval);// variable-size
        else options.back().Set(val_index, t_dval);// fixed-size
      }
      else{
        if(t_size==0) options.back().Add(*t_sval_tmp);// variable-size
        else options.back().Set(val_index, *t_sval_tmp);// fixed-size
      }
      val_index++;
    }

    // --> init new option
    else{
      val_index = 0;
      tag_prev = *t_tag_tmp;
      key_prev = *t_key_tmp;
      if(t_type=='i'){
        options.push_back(GwollumOption(*t_tag_tmp, *t_key_tmp, t_idef, t_size));
        if(t_size==0) options.back().Add(t_ival);// variable-size
        else options.back().Set(val_index, t_ival);// fixed-size
      }
      else if(t_type=='u'){
        options.push_back(GwollumOption(*t_tag_tmp, *t_key_tmp, t_udef, t_size));
        if(t_size==0) options.back().Add(t_uval);// variable-size
        else options.back().Set(val_index, t_uval);// fixed-size
      }
      else if(t_type=='d'){
        options.push_back(GwollumOption(*t_tag_tmp, *t_key_tmp, t_ddef, t_size));
        if(t_size==0) options.back().Add(t_dval);// variable-size
        else options.back().Set(val_index, t_dval);// fixed-size
      }
      else{
        options.push_back(GwollumOption(*t_tag_tmp, *t_key_tmp, *t_sdef_tmp, t_size));
        if(t_size==0) options.back().Add(*t_sval_tmp);// variable-size
        else options.back().Set(val_index, *t_sval_tmp);// fixed-size
      }
      val_index++;
    }
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool GwollumOptions::ReadChain(bool &aIsUniform, TChain *aChain){
////////////////////////////////////////////////////////////////////////////////////

  GwollumOptions *go = NULL;
  int previous = -1;
  aIsUniform = true;
  
  for(unsigned int o=0; o<aChain->GetEntries(); o++){

    // load tree
    if(aChain->LoadTree(o)<0) continue;
    
    // use first tree as a reference
    if(previous==-1){
      if(!GwollumOptions::ReadTree(aChain->GetTree())) return false;
      previous = aChain->GetTreeNumber();
      continue;
    }
    
    // new option tree
    if(aChain->GetTreeNumber()!=previous){
      delete go;
      go = new GwollumOptions();
      go->ReadTree(aChain->GetTree());
      previous = aChain->GetTreeNumber();
      aIsUniform = IsTheSame(go) && aIsUniform;
    }

    // stop if not uniform
    if(aIsUniform==false) break;
  }
  
  delete go;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::FillTree(void){
////////////////////////////////////////////////////////////////////////////////////

  // reset TTree
  OptTree->Reset();

  // fill TTree
  for(unsigned int o=0; o<options.size(); o++){

    t_tag = options[o].GetTag();
    t_key = options[o].GetKey();
    t_type = options[o].GetType();
    t_size = options[o].GetSize();
    t_idef = options[o].GetDefaultI();
    t_udef = options[o].GetDefaultU();
    t_ddef = options[o].GetDefaultD();
    t_sdef = options[o].GetDefaultS();
    for(unsigned int v=0; v<options[o].GetNumberOfValues(); v++){
      t_ival = options[o].GetValueI(v);
      t_uval = options[o].GetValueU(v);
      t_dval = options[o].GetValueD(v);
      t_sval = options[o].GetValueS(v);
      OptTree->Fill();
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void GwollumOptions::WriteTree(const string aTreeName, const string aRootFileName, const string aSubDirectory){
////////////////////////////////////////////////////////////////////////////////////

  // open TFile
  TFile *fout = new TFile(aRootFileName.c_str(), "recreate");
  if(fout->IsZombie()) return;
  fout->cd();

  // create sub-directory
  if(aSubDirectory.compare("")){
    fout->mkdir(aSubDirectory.c_str());
    fout->cd(aSubDirectory.c_str());
  }

  // write tree
  WriteTree(aTreeName);

  // exit
  fout->Close();
  return;
}
