/**
 * @file 
 * @brief Program to test the GwollumOptions class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "GwollumOptions.h"

using namespace std;

/**
 * @brief Test program.
 */
int main(int argc, char* argv[]){

  //*********************************************************************
  // GwollumOption(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize=0)
  //*********************************************************************
  cout<<"\nGwollumOption(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize)"<<endl;
  GwollumOption *GO_if = new GwollumOption("TAG", "KEY", (int)1, 3);
  GwollumOption *GO_iv = new GwollumOption("TAG", "KEY", (int)1, 0);
  GwollumOption *GO_uf = new GwollumOption("TAG", "KEY", (unsigned int)2, 3);
  GwollumOption *GO_uv = new GwollumOption("TAG", "KEY", (unsigned int)2, 0);
  GwollumOption *GO_df = new GwollumOption("TAG", "KEY", (double)3.0, 3);
  GwollumOption *GO_dv = new GwollumOption("TAG", "KEY", (double)3.0, 0);
  GwollumOption *GO_sf = new GwollumOption("TAG", "KEY", (string)"toto", 3);
  GwollumOption *GO_sv = new GwollumOption("TAG", "KEY", (string)"toto", 0);

  cout<<"\tGetTag()"<<endl;
  if(GO_if->GetTag().compare("TAG")) return 1;
  if(GO_iv->GetTag().compare("TAG")) return 1;
  if(GO_uf->GetTag().compare("TAG")) return 1;
  if(GO_uv->GetTag().compare("TAG")) return 1;
  if(GO_df->GetTag().compare("TAG")) return 1;
  if(GO_dv->GetTag().compare("TAG")) return 1;
  if(GO_sf->GetTag().compare("TAG")) return 1;
  if(GO_sv->GetTag().compare("TAG")) return 1;

  cout<<"\tGetKey()"<<endl;
  if(GO_if->GetKey().compare("KEY")) return 2;
  if(GO_iv->GetKey().compare("KEY")) return 2;
  if(GO_uf->GetKey().compare("KEY")) return 2;
  if(GO_uv->GetKey().compare("KEY")) return 2;
  if(GO_df->GetKey().compare("KEY")) return 2;
  if(GO_dv->GetKey().compare("KEY")) return 2;
  if(GO_sf->GetKey().compare("KEY")) return 2;
  if(GO_sv->GetKey().compare("KEY")) return 2;

  cout<<"\tGetType()"<<endl;
  if(GO_if->GetType()!='i') return 3;
  if(GO_iv->GetType()!='i') return 3;
  if(GO_uf->GetType()!='u') return 3;
  if(GO_uv->GetType()!='u') return 3;
  if(GO_df->GetType()!='d') return 3;
  if(GO_dv->GetType()!='d') return 3;
  if(GO_sf->GetType()!='s') return 3;
  if(GO_sv->GetType()!='s') return 3;

  cout<<"\tGetSize()"<<endl;
  if(GO_if->GetSize()!=3) return 4;
  if(GO_iv->GetSize()!=0) return 4;
  if(GO_uf->GetSize()!=3) return 4;
  if(GO_uv->GetSize()!=0) return 4;
  if(GO_df->GetSize()!=3) return 4;
  if(GO_dv->GetSize()!=0) return 4;
  if(GO_sf->GetSize()!=3) return 4;
  if(GO_sv->GetSize()!=0) return 4;

  cout<<"\tGetNumberOfValues()"<<endl;
  if(GO_if->GetNumberOfValues()!=3) return 5;
  if(GO_iv->GetNumberOfValues()!=0) return 5;
  if(GO_uf->GetNumberOfValues()!=3) return 5;
  if(GO_uv->GetNumberOfValues()!=0) return 5;
  if(GO_df->GetNumberOfValues()!=3) return 5;
  if(GO_dv->GetNumberOfValues()!=0) return 5;
  if(GO_sf->GetNumberOfValues()!=3) return 5;
  if(GO_sv->GetNumberOfValues()!=0) return 5;

  cout<<"\tGetValue()"<<endl;
  if(GO_if->GetValueI(0)!=1) return 6;
  if(GO_if->GetValueI(1)!=1) return 6;
  if(GO_if->GetValueI(2)!=1) return 6;
  if(GO_iv->GetValueI(0)!=1) return 6;
  if(GO_uf->GetValueU(0)!=2) return 6;
  if(GO_uf->GetValueU(1)!=2) return 6;
  if(GO_uf->GetValueU(2)!=2) return 6;
  if(GO_uv->GetValueU(0)!=2) return 6;
  if(GO_df->GetValueD(0)!=3.0) return 6;
  if(GO_df->GetValueD(1)!=3.0) return 6;
  if(GO_df->GetValueD(2)!=3.0) return 6;
  if(GO_dv->GetValueD(0)!=3.0) return 6;
  if(GO_sf->GetValueS(0).compare("toto")) return 6;
  if(GO_sf->GetValueS(1).compare("toto")) return 6;
  if(GO_sf->GetValueS(2).compare("toto")) return 6;
  if(GO_sv->GetValueS(0).compare("toto")) return 6;

  cout<<"\tSet()"<<endl;
  GO_if->Set(0, (int)10); GO_if->Set(1, (int)11);
  if(GO_if->GetValueI(0)!=10) return 7;
  if(GO_if->GetValueI(1)!=11) return 7;
  if(GO_if->GetValueI(2)!=1) return 7;
  GO_uf->Set(0, (unsigned int)10); GO_uf->Set(1, (unsigned int)11);
  if(GO_uf->GetValueU(0)!=10) return 7;
  if(GO_uf->GetValueU(1)!=11) return 7;
  if(GO_uf->GetValueU(2)!=2) return 7;
  GO_df->Set(0, (double)10.0); GO_df->Set(1, (double)11.0);
  if(GO_df->GetValueD(0)!=10.0) return 7;
  if(GO_df->GetValueD(1)!=11.0) return 7;
  if(GO_df->GetValueD(2)!=3.0) return 7;
  GO_sf->Set(0, (string)"einstein"); GO_sf->Set(1, (string)"curie");
  cout<<GO_sf->GetValueS(0)<<" "<<GO_sf->GetValueS(1)<<endl;
  if(GO_sf->GetValueS(0).compare("einstein")) return 7;
  if(GO_sf->GetValueS(1).compare("curie")) return 7;
  if(GO_sf->GetValueS(2).compare("toto")) return 7;

  GO_if->Set(2, (int)12); GO_if->Set(3, (int)13);
  if(GO_if->GetNumberOfValues()!=3) return 7;
  if(GO_if->GetValueI(2)!=12) return 7;
  if(GO_if->GetValueI(3)!=1) return 7;
  GO_uf->Set(2, (unsigned int)12); GO_uf->Set(3, (unsigned int)13);
  if(GO_uf->GetNumberOfValues()!=3) return 7;
  if(GO_uf->GetValueU(2)!=12) return 7;
  if(GO_uf->GetValueU(3)!=2) return 7;
  GO_df->Set(2, (double)12.0); GO_df->Set(3, (double)13.0);
  if(GO_df->GetNumberOfValues()!=3) return 7;
  if(GO_df->GetValueD(2)!=12.0) return 7;
  if(GO_df->GetValueD(3)!=3.0) return 7;
  GO_sf->Set(2, (string)"hubble"); GO_sf->Set(3, (string)"galileo");
  if(GO_sf->GetNumberOfValues()!=3) return 7;
  if(GO_sf->GetValueS(2).compare("hubble")) return 7;
  if(GO_sf->GetValueS(3).compare("toto")) return 7;

  cout<<"\tAdd()"<<endl;
  GO_iv->Add((int)10); GO_iv->Add((int)11);
  GO_iv->Add((int)12); GO_iv->Add((int)13);
  if(GO_iv->GetNumberOfValues()!=4) return 7;
  if(GO_iv->GetValueI(0)!=10) return 7;
  if(GO_iv->GetValueI(1)!=11) return 7;
  if(GO_iv->GetValueI(2)!=12) return 7;
  if(GO_iv->GetValueI(3)!=13) return 7;
  GO_uv->Add((unsigned int)10); GO_uv->Add((unsigned int)11);
  GO_uv->Add((unsigned int)12); GO_uv->Add((unsigned int)13);
  if(GO_uv->GetNumberOfValues()!=4) return 7;
  if(GO_uv->GetValueU(0)!=10) return 7;
  if(GO_uv->GetValueU(1)!=11) return 7;
  if(GO_uv->GetValueU(2)!=12) return 7;
  if(GO_uv->GetValueU(3)!=13) return 7;
  GO_dv->Add((double)10.0); GO_dv->Add((double)11.0);
  GO_dv->Add((double)12.0); GO_dv->Add((double)13.0);
  if(GO_dv->GetNumberOfValues()!=4) return 7;
  if(GO_dv->GetValueD(0)!=10.0) return 7;
  if(GO_dv->GetValueD(1)!=11.0) return 7;
  if(GO_dv->GetValueD(2)!=12.0) return 7;
  if(GO_dv->GetValueD(3)!=13.0) return 7;
  GO_sv->Add((string)"einstein"); GO_sv->Add((string)"curie");
  GO_sv->Add((string)"hubble"); GO_sv->Add((string)"galileo");
  if(GO_sv->GetNumberOfValues()!=4) return 7;
  if(GO_sv->GetValueS(0).compare("einstein")) return 7;
  if(GO_sv->GetValueS(1).compare("curie")) return 7;
  if(GO_sv->GetValueS(2).compare("hubble")) return 7;
  if(GO_sv->GetValueS(3).compare("galileo")) return 7;

  cout<<"\tGetValue()"<<endl;
  int vi; unsigned int vu; double vd; string vs;
  if(!GO_if->GetValueI(vi, 0)) return 7;
  if(vi!=10) return 7;
  if(!GO_iv->GetValueI(vi, 0)) return 7;
  if(vi!=10) return 7;
  if(!GO_uf->GetValueU(vu, 0)) return 7;
  if(vu!=10) return 7;
  if(!GO_uv->GetValueU(vu, 0)) return 7;
  if(vu!=10) return 7;
  if(!GO_df->GetValueD(vd, 0)) return 7;
  if(vd!=10.0) return 7;
  if(!GO_dv->GetValueD(vd, 0)) return 7;
  if(vd!=10.0) return 7;
  if(!GO_sf->GetValueS(vs, 0)) return 7;
  if(vs.compare("einstein")) return 7;
  if(!GO_sv->GetValueS(vs, 0)) return 7;
  if(vs.compare("einstein")) return 7;

  cout<<"\tSet()"<<endl;
  GO_if->Set(0, (int)20); GO_if->Set(1, (int)21);
  if(GO_if->GetValueI(0)!=20) return 8;
  if(GO_if->GetValueI(1)!=21) return 8;
  if(GO_if->GetValueI(2)!=12) return 8;
  GO_uf->Set(0, (unsigned int)20); GO_uf->Set(1, (unsigned int)21);
  if(GO_uf->GetValueU(0)!=20) return 8;
  if(GO_uf->GetValueU(1)!=21) return 8;
  if(GO_uf->GetValueU(2)!=12) return 8;
  GO_df->Set(0, 20.0); GO_df->Set(1, 21.0);
  if(GO_df->GetValueD(0)!=20.0) return 8;
  if(GO_df->GetValueD(1)!=21.0) return 8;
  if(GO_df->GetValueD(2)!=12.0) return 8;
  GO_sf->Set(0, "hawkins"); GO_sf->Set(1, "archimede");
  if(GO_sf->GetValueS(0).compare("hawkins")) return 7;
  if(GO_sf->GetValueS(1).compare("archimede")) return 7;
  if(GO_sf->GetValueS(2).compare("hubble")) return 7;

  GO_iv->Set(0, (int)20); GO_iv->Set(1, (int)21);
  if(GO_iv->GetValueI(0)!=20) return 8;
  if(GO_iv->GetValueI(1)!=21) return 8;
  if(GO_iv->GetValueI(2)!=12) return 8;
  GO_uv->Set(0, (unsigned int)20); GO_uv->Set(1, (unsigned int)21);
  if(GO_uv->GetValueU(0)!=20) return 8;
  if(GO_uv->GetValueU(1)!=21) return 8;
  if(GO_uv->GetValueU(2)!=12) return 8;
  GO_dv->Set(0, 20.0); GO_dv->Set(1, 21.0);
  if(GO_dv->GetValueD(0)!=20.0) return 8;
  if(GO_dv->GetValueD(1)!=21.0) return 8;
  if(GO_dv->GetValueD(2)!=12.0) return 8;
  GO_sv->Set(0, "hawkins"); GO_sv->Set(1, "archimede");
  if(GO_sv->GetValueS(0).compare("hawkins")) return 8;
  if(GO_sv->GetValueS(1).compare("archimede")) return 8;
  if(GO_sv->GetValueS(2).compare("hubble")) return 8;

  cout<<"\tSetDefault()"<<endl;
  GO_if->SetDefault();
  GO_uf->SetDefault();
  GO_df->SetDefault();
  GO_sf->SetDefault();
  GO_iv->SetDefault();
  GO_uv->SetDefault();
  GO_dv->SetDefault();
  GO_sv->SetDefault();
  if(GO_if->GetNumberOfValues()!=3) return 9;
  if(GO_uf->GetNumberOfValues()!=3) return 9;
  if(GO_df->GetNumberOfValues()!=3) return 9;
  if(GO_sf->GetNumberOfValues()!=3) return 9;
  if(GO_iv->GetNumberOfValues()!=1) return 9;
  if(GO_uv->GetNumberOfValues()!=1) return 9;
  if(GO_dv->GetNumberOfValues()!=1) return 9;
  if(GO_sv->GetNumberOfValues()!=1) return 9;
  if(GO_if->GetValueI(0)!=1) return 8;
  if(GO_uf->GetValueU(0)!=2) return 8;
  if(GO_df->GetValueD(0)!=3.0) return 8;
  if(GO_sf->GetValueS(0).compare("toto")) return 8;
  if(GO_if->GetValueI(1)!=1) return 8;
  if(GO_uf->GetValueU(1)!=2) return 8;
  if(GO_df->GetValueD(1)!=3.0) return 8;
  if(GO_sf->GetValueS(1).compare("toto")) return 8;
  if(GO_if->GetValueI(2)!=1) return 8;
  if(GO_uf->GetValueU(2)!=2) return 8;
  if(GO_df->GetValueD(2)!=3.0) return 8;
  if(GO_sf->GetValueS(2).compare("toto")) return 8;
  if(GO_iv->GetValueI(0)!=1) return 8;
  if(GO_uv->GetValueU(0)!=2) return 8;
  if(GO_dv->GetValueD(0)!=3.0) return 8;
  if(GO_sv->GetValueS(0).compare("toto")) return 8;

  delete GO_if;
  delete GO_iv;
  delete GO_uf;
  delete GO_uv;
  delete GO_df;
  delete GO_dv;
  delete GO_sf;
  delete GO_sv;


  //*********************************************************************
  // GwollumOptions()
  //*********************************************************************
  cout<<"\nGwollumOptions()"<<endl;
  GwollumOptions *GO = new GwollumOptions();

  cout<<"\tGetOptionsN()"<<endl;
  if(GO->GetOptionsN()!=0) return 20;

  // add fixed options
  GO->Add("DATA", "TESTI", (int)0, 3);
  GO->Add("DATA", "TESTU", (unsigned int)0, 1);
  GO->Add("DATA", "TESTD", 0.0, 1);
  GO->Add("DATA", "TESTS", "zero", 1);

  // read option file
  cout<<"\tReadTextFile()"<<endl;
  char *c_optfile = getenv("GWOLLUM_DATA");
  string optfile;
  if(argc==2) optfile = (string)argv[1];
  else if(c_optfile==NULL) optfile=".";
  else optfile = (string)c_optfile;
  optfile+="/gwollumoptions.opt";
  if(GO->ReadTextFile(optfile)!=4) return 21;

  cout<<"\tGetOptionSize()"<<endl;
  if(GO->GetOptionSize("DATA", "TESTI")!=3) return 22;
  if(GO->GetOptionSize("DATA", "TESTU")!=1) return 22;
  if(GO->GetOptionSize("DATA", "TESTD")!=1) return 22;
  if(GO->GetOptionSize("DATA", "TESTS")!=1) return 22;
  if(GO->GetOptionSize("DATA", "DOESNOTEXIST")!=0) return 22;

  cout<<"\tGetOptionNumberOfValues()"<<endl;
  if(GO->GetOptionNumberOfValues("DATA", "TESTI")!=3) return 22;
  if(GO->GetOptionNumberOfValues("DATA", "TESTU")!=1) return 22;
  if(GO->GetOptionNumberOfValues("DATA", "TESTD")!=1) return 22;
  if(GO->GetOptionNumberOfValues("DATA", "TESTS")!=1) return 22;
  if(GO->GetOptionNumberOfValues("DATA", "DOESNOTEXIST")!=0) return 22;

  cout<<"\tGetOptionValue()"<<endl;
  if(GO->GetOptionValueI("DATA", "TESTI", 0)!=-1) return 23;
  if(GO->GetOptionValueI("DATA", "TESTI", 1)!=-2) return 23;
  if(GO->GetOptionValueI("DATA", "TESTI", 2)!=-3) return 23;
  if(GO->GetOptionValueI("DATA", "TESTI", 3)!=0) return 23;
  if(GO->GetOptionValueU("DATA", "TESTU", 0)!=2) return 23;
  if(GO->GetOptionValueU("DATA", "TESTU", 1)!=0) return 23;
  if(GO->GetOptionValueD("DATA", "TESTD", 0)!=3.09) return 23;
  if(GO->GetOptionValueD("DATA", "TESTD", 1)!=0.0) return 23;
  if(GO->GetOptionValueS("DATA", "TESTS", 0).compare("four")!=0) return 23;
  if(GO->GetOptionValueS("DATA", "TESTS", 1).compare("zero")!=0) return 23;

  // add variable options
  GO->Add("DATA", "TESTVI", (int)0, 0);
  GO->Add("DATA", "TESTVU", (unsigned int)0, 0);
  GO->Add("DATA", "TESTVD", 0.0, 0);
  GO->Add("DATA", "TESTVS", "zero", 0);

  // read option file
  cout<<"\tReadTextFile()"<<endl;
  if(GO->ReadTextFile(optfile)!=8) return 24;

  cout<<"\tGetOptionSize()"<<endl;
  if(GO->GetOptionSize("DATA", "TESTVI")!=0) return 25;
  if(GO->GetOptionSize("DATA", "TESTVU")!=0) return 25;
  if(GO->GetOptionSize("DATA", "TESTVD")!=0) return 25;
  if(GO->GetOptionSize("DATA", "TESTVS")!=0) return 25;

  cout<<"\tGetOptionNumberOfValues()"<<endl;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVI")!=2) return 25;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVU")!=3) return 25;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVD")!=4) return 25;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVS")!=5) return 25;

  cout<<"\tGetOptionValue()"<<endl;
  if(GO->GetOptionValueI("DATA", "TESTVI", 0)!=-1) return 26;
  if(GO->GetOptionValueI("DATA", "TESTVI", 1)!=-2) return 26;
  if(GO->GetOptionValueI("DATA", "TESTVI", 2)!=0) return 26;
  if(GO->GetOptionValueU("DATA", "TESTVU", 0)!=2) return 26;
  if(GO->GetOptionValueU("DATA", "TESTVU", 1)!=3) return 26;
  if(GO->GetOptionValueU("DATA", "TESTVU", 2)!=4) return 26;
  if(GO->GetOptionValueU("DATA", "TESTVU", 3)!=0) return 26;
  if(GO->GetOptionValueD("DATA", "TESTVD", 0)!=4.23) return 26;
  if(GO->GetOptionValueD("DATA", "TESTVD", 1)!=5.67) return 26;
  if(GO->GetOptionValueD("DATA", "TESTVD", 2)!=7.89) return 26;
  if(GO->GetOptionValueD("DATA", "TESTVD", 3)!=2.22) return 26;
  if(GO->GetOptionValueD("DATA", "TESTVD", 4)!=0.0) return 26;
  if(GO->GetOptionValueS("DATA", "TESTVS", 0).compare("one")!=0) return 26;
  if(GO->GetOptionValueS("DATA", "TESTVS", 1).compare("two")!=0) return 26;
  if(GO->GetOptionValueS("DATA", "TESTVS", 2).compare("three")!=0) return 26;
  if(GO->GetOptionValueS("DATA", "TESTVS", 3).compare("four")!=0) return 26;
  if(GO->GetOptionValueS("DATA", "TESTVS", 4).compare("five")!=0) return 26;
  if(GO->GetOptionValueS("DATA", "TESTVS", 5).compare("zero")!=0) return 26;

  cout<<"\tWriteTextFile()"<<endl;
  error_code ec;
  if(!CreateDirectory(ec, "./gwl-test-gwollumoptions-dir")){
    cerr<<"Cannot create test directory ("<<ec<<")"<<endl;
    return 1;
  }
  // just testing it does not crash
  GO->WriteTextFile("./gwl-test-gwollumoptions-dir/options.txt");

  cout<<"\tWriteTree()"<<endl;
  // just testing it does not crash
  GO->FillTree();
  GO->WriteTree("gwloptions", "./gwl-test-gwollumoptions-dir/options.root", "");

  delete GO;
  GO = new GwollumOptions();

  cout<<"\tReadTree()"<<endl;
  if(GO->ReadTree("./gwl-test-gwollumoptions-dir/doesnotexist.root", "gwloptions")==true) return 30;
  if(GO->GetOptionsN()!=0) return 30;
  if(GO->ReadTree("./gwl-test-gwollumoptions-dir/options.root", "doesnotexist")==true) return 30;
  if(GO->GetOptionsN()!=0) return 30;
  if(GO->ReadTree("./gwl-test-gwollumoptions-dir/options.root", "gwloptions")==false) return 30;
  if(GO->GetOptionsN()!=8) return 30;

  cout<<"\tChecking options..."<<endl;
  if(GO->GetOptionSize("DATA", "TESTI")!=3) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTI")!=3) return 30;
  if(GO->GetOptionValueI("DATA", "TESTI", 0)!=-1) return 30;
  if(GO->GetOptionValueI("DATA", "TESTI", 1)!=-2) return 30;
  if(GO->GetOptionValueI("DATA", "TESTI", 2)!=-3) return 30;
  if(GO->GetOptionSize("DATA", "TESTVI")!=0) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVI")!=2) return 30;
  if(GO->GetOptionValueI("DATA", "TESTVI", 0)!=-1) return 30;
  if(GO->GetOptionValueI("DATA", "TESTVI", 1)!=-2) return 30;
  if(GO->GetOptionValueI("DATA", "TESTVI", 2)!=0) return 30;
  
  if(GO->GetOptionSize("DATA", "TESTU")!=1) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTU")!=1) return 30;
  if(GO->GetOptionValueU("DATA", "TESTU", 0)!=2) return 30;
  if(GO->GetOptionSize("DATA", "TESTVU")!=0) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVU")!=3) return 30;
  if(GO->GetOptionValueU("DATA", "TESTVU", 0)!=2) return 30;
  if(GO->GetOptionValueU("DATA", "TESTVU", 1)!=3) return 30;
  if(GO->GetOptionValueU("DATA", "TESTVU", 2)!=4) return 30;

  if(GO->GetOptionSize("DATA", "TESTD")!=1) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTD")!=1) return 30;
  if(GO->GetOptionValueD("DATA", "TESTD", 0)!=3.09) return 30;
  if(GO->GetOptionSize("DATA", "TESTVD")!=0) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVD")!=4) return 30;
  if(GO->GetOptionValueD("DATA", "TESTVD", 0)!=4.23) return 30;
  if(GO->GetOptionValueD("DATA", "TESTVD", 1)!=5.67) return 30;
  if(GO->GetOptionValueD("DATA", "TESTVD", 2)!=7.89) return 30;
  if(GO->GetOptionValueD("DATA", "TESTVD", 3)!=2.22) return 30;
  
  if(GO->GetOptionSize("DATA", "TESTS")!=1) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTS")!=1) return 30;
  if(GO->GetOptionValueS("DATA", "TESTS", 0).compare("four")) return 30;
  if(GO->GetOptionSize("DATA", "TESTVS")!=0) return 30;
  if(GO->GetOptionNumberOfValues("DATA", "TESTVS")!=5) return 30;
  if(GO->GetOptionValueS("DATA", "TESTVS", 0).compare("one")) return 30;
  if(GO->GetOptionValueS("DATA", "TESTVS", 1).compare("two")) return 30;
  if(GO->GetOptionValueS("DATA", "TESTVS", 2).compare("three")) return 30;
  if(GO->GetOptionValueS("DATA", "TESTVS", 3).compare("four")) return 30;
  if(GO->GetOptionValueS("DATA", "TESTVS", 4).compare("five")) return 30;

  cout<<"\tRemoveOption()"<<endl;
  GO->RemoveOption("TOTO", "TATA"); // no effects
  GO->RemoveOption("DATA", "TESTVU");
  GO->RemoveOption("DATA", "TESTI");
  if(GO->GetOptionsN()!=6) return 31;
  if(GO->GetOptionSize("DATA", "TESTVI")!=0) return 31;
  if(GO->GetOptionValueI("DATA", "TESTVI", 1)!=-2) return 31;
  if(GO->GetOptionSize("DATA", "TESTI")!=0) return 31;
  if(GO->GetOptionSize("DATA", "TESTVU")!=0) return 31;


  delete GO;

  cout<<"\tOK"<<endl;



  return 0;
}

