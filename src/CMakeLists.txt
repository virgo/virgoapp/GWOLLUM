# -- dependencies ------------

# FFTW
pkg_check_modules(FFTW REQUIRED "fftw3")

# FrameL
pkg_check_modules(FRAMEL REQUIRED "framel>=8.42")
message(STATUS "FrameL dir: ${FRAMEL_LIBRARY_DIRS}")
message(STATUS "FrameL libraries: ${FRAMEL_LIBRARIES}")

# HDF5
find_package(HDF5 COMPONENTS CXX)

# add link paths
link_directories(${FFTW_LIBRARY_DIRS} ${FRAMEL_LIBRARY_DIRS} ${HDF5_LIBRARY_DIRS})

# -- ROOT -------------------

find_package(ROOT 6.26 REQUIRED COMPONENTS Core RIO Hist Graf Gpad Tree TreePlayer Matrix MathCore)
message(STATUS "Found ROOT libraries in ${ROOT_LIBRARY_DIR}")
message(STATUS "ROOT libraries: ${ROOT_LIBRARIES}")
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
include(${ROOT_USE_FILE})

# -- C libraries ------------

add_subdirectory(Time)
add_subdirectory(Utils)
add_subdirectory(Segments)
add_subdirectory(Streams)
add_subdirectory(Triggers)
add_subdirectory(Inject)
add_subdirectory(Coinc)

# -- pkgconfig --------------

# install pkgconfig file
string(
  TOLOWER
  ${PROJECT_NAME}
  PROJECT_NAME_LOWER
  )
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME_LOWER}.pc.in"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME_LOWER}.pc"
  @ONLY
  )
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME_LOWER}.pc
  DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/pkgconfig
  )
