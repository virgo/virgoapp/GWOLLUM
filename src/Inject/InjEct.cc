/**
 * @file 
 * @brief See InjEct.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjEct.h"

ClassImp(InjEct)

/////////////////////////////////////////////////////////////////////////
InjEct::InjEct(const string aPattern,
               const string aPlotStyle,
               const unsigned int aVerbose):
InjRea(aPattern, aVerbose), GwollumPlot("INJ", aPlotStyle){ 
/////////////////////////////////////////////////////////////////////////

  InjRea::mon->SetMessageType(monitor_info_2);
  *InjRea::mon<<"InjEct::"<<__func__<<endl;

  // for plotting
  GwollumPlot::SetGridx(1);  GwollumPlot::SetGridy(1);
  
  // default tukey fraction: 10%
  tukey_frac = 0.1;

  // default antenna factor update period = 1 s
  antenna_period = 1.0;
  
  // dummy containers
  hplus  = new TGraph(0); hplus->SetName("InjEct_hplus");
  hcross = new TGraph(0); hcross->SetName("InjEct_hcross");
  hdet   = new TGraph(0); hdet->SetName("InjEct_hdet");
}

/////////////////////////////////////////////////////////////////////////
InjEct::~InjEct(void){
/////////////////////////////////////////////////////////////////////////
  InjRea::mon->SetMessageType(monitor_info_2);
  *InjRea::mon<<"InjEct::"<<__func__<<endl;
  delete hplus;
  delete hcross;
  delete hdet;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::Inject(Streams *aDetectorStream,
                    const unsigned int aDataSize, double *aData, const double aTimeStart){
/////////////////////////////////////////////////////////////////////////
  InjRea::mon->SetMessageType(monitor_info_2);
  *InjRea::mon<<"InjEct::"<<__func__<<endl;
  if(InjRea::mon->GetStatus()==false){
    InjRea::mon->SetMessageType(monitor_fatal);
    *InjRea::mon<<"InjEct::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // segment start - end
  double sstart = aTimeStart;
  double send = sstart + (double)aDataSize/(double)aDetectorStream->GetNativeFrequency();

  double istart_geocentric, iend_geocentric, istart_local, iend_local;
  double t_local;

  // loop over injections
  for(Long64_t i=0; i<InjRea::GetN(); i++){

    // load injection
    if(LoadInjection(i)<=0) continue;

    // Injecion time end:
    // the injection ends before the requested segment --> skip
    iend_geocentric = InjRea::GetInjectionTimeEnd();
    iend_local = aDetectorStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), iend_geocentric);
    if(iend_local<sstart) continue;

    // Injecion time start:
    // the injection starts after the requested segment --> skip
    istart_geocentric = InjRea::GetInjectionTimeStart();
    istart_local = aDetectorStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), istart_geocentric);
    if(istart_local>=send) continue;

    // construct the local waveform
    if(InjRea::GetInjectionType()==injtype_user) ConstructUserWaveform(aDetectorStream);
    else if(InjRea::GetInjectionType()==injtype_sinegauss) ConstructSineGaussWaveform(aDetectorStream);
    else continue;

    // add injection waveform
    for(unsigned int d=0; d<aDataSize; d++){

      // local time
      t_local = aTimeStart+(double)d/(double)aDetectorStream->GetNativeFrequency();

      // out of range
      if(t_local<hdet->GetX()[0]) continue;
      if(t_local>=hdet->GetX()[hdet->GetN()-1]) break;

      // inject
      aData[d] += hdet->Eval(aTimeStart+(double)d/(double)aDetectorStream->GetNativeFrequency());
    }

  }

  return;  
}

/////////////////////////////////////////////////////////////////////////
void InjEct::Plot(Streams *aDetectorStream, const Long64_t aInjectionIndex){
/////////////////////////////////////////////////////////////////////////
  InjRea::mon->SetMessageType(monitor_info_2);
  *InjRea::mon<<"InjEct::"<<__func__<<endl;
  if(InjRea::mon->GetStatus()==false){
    InjRea::mon->SetMessageType(monitor_fatal);
    *InjRea::mon<<"InjEct::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // load requested injection
  if(InjRea::LoadInjection(aInjectionIndex)<=0) return;
  
  // construct the local waveform
  if(InjRea::GetInjectionType()==injtype_user) ConstructUserWaveform(aDetectorStream);
  else if(InjRea::GetInjectionType()==injtype_sinegauss) ConstructSineGaussWaveform(aDetectorStream);
  else return;
  
  GwollumPlot::Draw(hplus, "AL");
  GwollumPlot::Draw(hcross, "Lsame");
  GwollumPlot::Draw(hdet, "Lsame");
  
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::ConstructUserWaveform(Streams *aDetectorStream){
/////////////////////////////////////////////////////////////////////////
  
  // reset h+ (if size change)
  if(hplus->GetN() != InjRea::wave_hplus->GetN()){
    delete hplus;
    hplus = new TGraph(InjRea::wave_hplus->GetN()); hplus->SetName("InjEct_hplus");
    hplus->GetXaxis()->SetNoExponent();
    hplus->SetTitle("h_{+}");
    hplus->SetBit(TGraph::kIsSortedX);
  }

  // reset hx (if size change)
  if(hcross->GetN() != InjRea::wave_hcross->GetN()){
    delete hcross;
    hcross = new TGraph(InjRea::wave_hcross->GetN()); hcross->SetName("InjEct_hcross");
    hcross->GetXaxis()->SetNoExponent();
    hcross->SetTitle("h_{x}");
    hcross->SetBit(TGraph::kIsSortedX);
  }
  
  // reset hdet (if size change)
  if(hdet->GetN() != InjRea::wave_hplus->GetN()){
    delete hdet;
    hdet = new TGraph(InjRea::wave_hplus->GetN()); hdet->SetName("InjEct_hdet");
    hdet->GetXaxis()->SetNoExponent();
    hdet->SetTitle("h_{det}");
    hdet->SetBit(TGraph::kIsSortedX);
  }
  
  // tukey window
  double *tukey = GetTukeyWindow((unsigned int)InjRea::wave_hplus->GetN(), tukey_frac);
  
  double t_local, amplitude;
  double fplus, fcross;
  double t_prev = -antenna_period;
  double fp0, fc0;
  double fp1, fc1;
  double t0 = 0.0;
  double gmst;
  
  // waveforms at the detector
  for(unsigned int i=0; i<(unsigned int)InjRea::wave_hplus->GetN(); i++){
    
    // local time
    t_local = aDetectorStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), InjRea::wave_hplus->GetX()[i]+GetInjectionTime());
    
    // local h+
    amplitude = InjRea::GetInjectionAmplitude()*InjRea::wave_hplus->GetY()[i];
    hplus->SetPoint(i, t_local, amplitude);
    
    // local hx
    amplitude = InjRea::GetInjectionAmplitude()*InjRea::wave_hcross->GetY()[i];
    hcross->SetPoint(i, t_local, amplitude);
    
    // antenna factors interpolator
    if(t_local-t_prev>antenna_period){
      t0 = t_local;
      gmst = GreenwichMeanSiderealTime(t0);
      aDetectorStream->GetDetectorAMResponse(fp0, fc0,
                                             InjRea::GetInjectionRa(),
                                             InjRea::GetInjectionDec(),
                                             InjRea::GetInjectionPolarization(),
                                             gmst);
      aDetectorStream->GetDetectorAMResponse(fp1, fc1,
                                             InjRea::GetInjectionRa(),
                                             InjRea::GetInjectionDec(),
                                             InjRea::GetInjectionPolarization(),
                                             gmst+antenna_period);
      t_prev = t0;
    }

    // antenna factors
    fplus = fp0 + (t_local - t0)/antenna_period*(fp1 - fp0);
    fcross = fc0 + (t_local - t0)/antenna_period*(fc1 - fc0);
    
    // hdet
    hdet->SetPoint(i, t_local, tukey[i]*(fplus * hplus->GetY()[i] + fcross * hcross->GetY()[i]));
  }
 
  delete [] tukey;
  return;
}

/////////////////////////////////////////////////////////////////////////
void InjEct::ConstructSineGaussWaveform(Streams *aDetectorStream){
/////////////////////////////////////////////////////////////////////////
  
  // number of points
  unsigned int npoints = (unsigned int)TMath::Ceil((InjRea::GetInjectionTimeEnd()-InjRea::GetInjectionTimeStart())*(double)aDetectorStream->GetNativeFrequency());

  // reset h+ (if size change)
  if((unsigned int)hplus->GetN() != npoints){
    delete hplus;
    hplus = new TGraph(npoints); hplus->SetName("InjEct_hplus");
    hplus->GetXaxis()->SetNoExponent();
    hplus->SetTitle("h_{+}");
    hplus->SetBit(TGraph::kIsSortedX);
  }
  
  // reset hx (if size change)
  if((unsigned int)hcross->GetN() != npoints){
    delete hcross;
    hcross = new TGraph(npoints); hcross->SetName("InjEct_hcross");
    hcross->GetXaxis()->SetNoExponent();
    hcross->SetTitle("h_{x}");
    hcross->SetBit(TGraph::kIsSortedX);
  }
  
  // reset hdet (if size change)
  if((unsigned int)hdet->GetN() != npoints){
    delete hdet;
    hdet = new TGraph(npoints); hdet->SetName("InjEct_hdet");
    hdet->GetXaxis()->SetNoExponent();
    hdet->SetTitle("h_{det}");
    hdet->SetBit(TGraph::kIsSortedX);
  }
  
  // tukey window
  double *tukey = GetTukeyWindow(npoints, tukey_frac);

  double t_local, t_geocentric;
  double fplus, fcross;
  double t_prev = -antenna_period;
  double fp0, fc0;
  double fp1, fc1;
  double t0 = 0.0;
  double gmst;

  // "peak" amplitudes of h+ and hx
  double h0plus  = InjRea::GetSineGaussh0plus();
  double h0cross = InjRea::GetSineGaussh0cross();
    
  // parameters
  double sigma  = 2.0 * InjRea::GetInjectionSigma()*InjRea::GetInjectionSigma();
  double factor = 2.0 * TMath::Pi() * InjRea::GetInjectionFrequency();

  // waveforms at the detector
  for(unsigned int i=0; i<(unsigned int)hplus->GetN(); i++){

    // local time
    t_geocentric = ((double)i-(double)hplus->GetN()/2.0)/(double)aDetectorStream->GetNativeFrequency();
    t_local = aDetectorStream->GetLocalTime(InjRea::GetInjectionRa(), InjRea::GetInjectionDec(), t_geocentric+InjRea::GetInjectionTime());
      
    // local h+
    hplus->SetPoint(i, t_local, h0plus * TMath::Exp(-t_geocentric*t_geocentric/sigma) * TMath::Cos(factor*t_geocentric));

    // local hx at the detector
    hcross->SetPoint(i, t_local, h0cross * TMath::Exp(-t_geocentric*t_geocentric/sigma) * TMath::Sin(factor*t_geocentric));
    
    // antenna factors interpolator
    if(t_local-t_prev>antenna_period){
      t0 = t_local;
      gmst = GreenwichMeanSiderealTime(t0);
      aDetectorStream->GetDetectorAMResponse(fp0, fc0,
                                             InjRea::GetInjectionRa(),
                                             InjRea::GetInjectionDec(),
                                             InjRea::GetInjectionPolarization(),
                                             gmst);
      aDetectorStream->GetDetectorAMResponse(fp1, fc1,
                                             InjRea::GetInjectionRa(),
                                             InjRea::GetInjectionDec(),
                                             InjRea::GetInjectionPolarization(),
                                             gmst+antenna_period);
      t_prev = t0;
    }

    // antenna factors
    fplus = fp0 + (t_local - t0)/antenna_period*(fp1 - fp0);
    fcross = fc0 + (t_local - t0)/antenna_period*(fc1 - fc0);
    
    // hdet
    hdet->SetPoint(i, t_local, tukey[i]*(fplus*hplus->GetY()[i] + fcross*hcross->GetY()[i]));
  }

  delete [] tukey;
  return;
}

