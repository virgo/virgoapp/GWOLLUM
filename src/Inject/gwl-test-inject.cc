/**
 * @file 
 * @brief Program to test the injection classes.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjGen.h"
#include "InjEct.h"

using namespace std;

/**
 * @brief Test program.
 */
int main(void){

  // test directory
  error_code ec;
  if(!CreateDirectory(ec, (string)filesystem::temp_directory_path()+"/gwl-test-inject-dir")){
    cerr<<"Cannot create test directory ("<<ec<<")"<<endl;
    return 1;
  }
  
  //*********************************************************************
  // InjGen(Segments *aInSegments, const string aInjType, const string aInjName, const unsigned int aVerbose)
  //*********************************************************************
  cout<<"\nInjGen(Segments *aInSegments, const string aInjType, const string aInjName, const unsigned int aVerbose)"<<endl;
  Segments *Seg = new Segments(1126250000, 1126251000);
  Seg->AddSegment(1126253000, 1126254000);
  InjGen *I = new InjGen(Seg, injtype_sinegauss, "TESTSG", 0);
  I->SetAmplitudeRange(1.5e-19, 1.5e-19);
  I->SetSigmaRange(4.5, 4.5);
  I->SetFrequencyRange(132.4, 132.4);
  I->SetInjectionTimeSpacing(150.0);
  I->SetInjectionTimeJitter(0.0);
  cout<<"\tGetN()"<<endl;
  if(I->GetN()!=0) return 2;
  
  //*********************************************************************
  // GenerateInjections(void)
  //*********************************************************************
  cout<<"\nGenerateInjections(void)"<<endl;
  if(I->GenerateInjections()!=14) return 3;
  cout<<"\tGetN()"<<endl;
  if(I->GetN()!=14) return 3;

  //*********************************************************************
  // Write(const string aRootFileName)
  //*********************************************************************
  cout<<"\nWrite(\""<<(string)filesystem::temp_directory_path()<<"/gwl-test-inject-dir/gw-test-inject1.root\")"<<endl;
  if(I->Write((string)filesystem::temp_directory_path()+"/gwl-test-inject-dir/gw-test-inject1.root")==false) return 3;
  delete I;
  delete Seg;

  //*********************************************************************
  // InjTre(const string aInjType, const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nInjTre(injtype_user, \"TESTUSER\", 0)"<<endl;
  InjTre *J = new InjTre(injtype_user, "TESTUSER", 0);
  TGraph *G = new TGraph(4);
  G->SetPoint(0, 0, 0.0);
  G->SetPoint(1, 1, 1.0);
  G->SetPoint(2, 2, 2.0);
  G->SetPoint(3, 3, 3.0);
  cout<<"\tSetWave()"<<endl;
  if(!J->SetWave(G, G)) return 4;
  delete G;
  cout<<"\tAdd()"<<endl;
  if(J->Add(2.0, 2.0, 1126250711.0, 1e-19, 0.2, 1.5, 5.0, 120.0)!=1) return 4;
  if(J->Add(2.0, 2.0, 1126253804.0, 2e-19, 0.3, 1.6, 4.0, 130.0)!=2) return 4;
  cout<<"\tGetN()"<<endl;
  if(J->GetN()!=2) return 4;
  cout<<"\tWrite(\""<<(string)filesystem::temp_directory_path()<<"/gwl-test-inject-dir/gw-test-inject2.root\")"<<endl;
  if(!J->Write((string)filesystem::temp_directory_path()+"/gwl-test-inject-dir/gw-test-inject2.root")) return 4;
  J->Reset();
  cout<<"\tAdd()"<<endl;
  if(J->Add(2.0, 2.0, 1126250545.0, 2.0e-19, 0.31, 1.6, 4.0, 130.0)!=1) return 4;
  if(J->Add(2.123, 2.456, 1126250545.765, 3.5e-19, 0.32, 1.123, 0.4, 654.0)!=2) return 4;
  cout<<"\tGetN()"<<endl;
  if(J->GetN()!=2) return 4;
  cout<<"\tWrite(\""<<(string)filesystem::temp_directory_path()<<"/gwl-test-inject-dir/gw-test-inject3.root\")"<<endl;
  if(!J->Write((string)filesystem::temp_directory_path()+"/gwl-test-inject-dir/gw-test-inject3.root")) return 4;
  delete J;

  //*********************************************************************
  // InjRea(const string aPattern, const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nInjRea(\""<<(string)filesystem::temp_directory_path()<<"/gwl-test-inject-dir/gw-test-inject*.root\", 0)"<<endl;
  InjRea *R = new InjRea((string)filesystem::temp_directory_path()+"/gwl-test-inject-dir/gw-test-inject*.root", 0);
  cout<<"\tGetN()"<<endl;
  if(R->GetN()!=18) return 5;
  cout<<"\tGetInjectionIndex()"<<endl;
  if(R->GetInjectionIndex()!=0) return 5;
  cout<<"\tGetInjectionNameN()"<<endl;
  if(R->GetInjectionNameN()!=2) return 5;
  cout<<"\tGetInjectionType()"<<endl;
  if(R->GetInjectionType()!=injtype_sinegauss) return 5;
  cout<<"\tGetInjectionName()"<<endl;
  if(R->GetInjectionName().compare("TESTSG")) return 5;
  cout<<"\tGetInjectionName(0)"<<endl;
  if(R->GetInjectionName(0).compare("TESTSG")) return 5;
  cout<<"\tGetInjectionName(1)"<<endl;
  if(R->GetInjectionName(1).compare("TESTUSER")) return 5;
  cout<<"\tGetInputFilePattern()"<<endl;
  if(R->GetInputFilePattern().compare((string)filesystem::temp_directory_path()+"/gwl-test-inject-dir/gw-test-inject*.root")!=0) return 5;

  cout<<"\tGetInjectionAmplitudeMin/Max()"<<endl;
  if(R->GetInjectionAmplitudeMin(0)!=1.5e-19) return 5;
  if(R->GetInjectionAmplitudeMax(0)!=1.5e-19) return 5;
  if(R->GetInjectionAmplitudeMin(1)!=1.0e-19) return 5;
  if(R->GetInjectionAmplitudeMax(1)!=3.5e-19) return 5;
  if(R->GetInjectionAmplitudeMin(R->GetInjectionNameN())!=1.0e-19) return 5;
  if(R->GetInjectionAmplitudeMax(R->GetInjectionNameN())!=3.5e-19) return 5;

  cout<<"\tGetInjectionSigmaMin/Max()"<<endl;
  if(R->GetInjectionSigmaMin(0)!=4.5) return 5;
  if(R->GetInjectionSigmaMax(0)!=4.5) return 5;
  if(R->GetInjectionSigmaMin(1)!=0.4) return 5;
  if(R->GetInjectionSigmaMax(1)!=5.0) return 5;
  if(R->GetInjectionSigmaMin(R->GetInjectionNameN())!=0.4) return 5;
  if(R->GetInjectionSigmaMax(R->GetInjectionNameN())!=5.0) return 5;

  cout<<"\tGetInjectionFrequencyMin/Max()"<<endl;
  if(R->GetInjectionFrequencyMin(0)!=132.4) return 5;
  if(R->GetInjectionFrequencyMax(0)!=132.4) return 5;
  if(R->GetInjectionFrequencyMin(1)!=120.0) return 5;
  if(R->GetInjectionFrequencyMax(1)!=654.0) return 5;
  if(R->GetInjectionFrequencyMin(R->GetInjectionNameN())!=120.0) return 5;
  if(R->GetInjectionFrequencyMax(R->GetInjectionNameN())!=654.0) return 5;

  cout<<"\tLoadInjection(5)"<<endl;
  if(R->LoadInjection(5)<=0) return 5;
  cout<<"\tGetInjectionIndex()"<<endl;
  if(R->GetInjectionIndex()!=5) return 5;
  cout<<"\tGetInjectionName()"<<endl;
  if(R->GetInjectionName().compare("TESTUSER")) return 5;
  cout<<"\tGetInjectionType()"<<endl;
  if(R->GetInjectionType()!=injtype_user) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250545.765) return 5;
  cout<<"\tGetInjectionRa()"<<endl;
  if(R->GetInjectionRa()!=2.123) return 5;
  cout<<"\tGetInjectionDec()"<<endl;
  if(R->GetInjectionDec()!=2.456) return 5;
  cout<<"\tGetInjectionEccentricity()"<<endl;
  if(R->GetInjectionEccentricity()!=0.32) return 5;
  cout<<"\tGetInjectionPolarization()"<<endl;
  if(R->GetInjectionPolarization()!=1.123) return 5;

  cout<<"\tLoadInjection(0)"<<endl;
  if(R->LoadInjection(0)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250000.0) return 5;
  cout<<"\tLoadInjection(6)"<<endl;
  if(R->LoadInjection(6)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250600.0) return 5;
  cout<<"\tLoadInjection(7)"<<endl;
  if(R->LoadInjection(7)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126250711.0) return 5;
  cout<<"\tLoadInjection(12)"<<endl;
  if(R->LoadInjection(12)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126253300.0) return 5;
  cout<<"\tLoadInjection(15)"<<endl;
  if(R->LoadInjection(15)<=0) return 5;
  cout<<"\tGetInjectionTime()"<<endl;
  if(R->GetInjectionTime()!=1126253750.0) return 5;

  if(R->LoadInjection(10)<=0) return 5;
  R->SetInjectionTag(false);
  cout<<"\tSetInjectionTag()"<<endl;
  if(R->LoadInjection(7)<=0) return 5;
  cout<<"\tGetInjectionTag()"<<endl;
  if(R->GetInjectionTag()==false) return 5;
  if(R->LoadInjection(10)<=0) return 5;
  cout<<"\tGetInjectionTag()"<<endl;
  if(R->GetInjectionTag()==true) return 5;

  cout<<"\tLoadInjection(4)"<<endl;
  if(R->LoadInjection(4)<=0) return 5;
  cout<<"\tGetInjectionTimeStart()"<<endl;
  if(R->GetInjectionTimeStart()!=1126250545.00000) return 5;
  cout<<"\tGetInjectionTimeEnd()"<<endl;
  if(R->GetInjectionTimeEnd()!=1126250548.00000) return 5;

  cout<<"\tLoadInjection(11)"<<endl;
  if(R->LoadInjection(11)<=0) return 5;
  cout<<"\tGetInjectionIndex()"<<endl;
  if(R->GetInjectionIndex()!=11) return 5;
  cout<<"\tGetInjectionTimeStart()"<<endl;
  if(R->GetInjectionTimeStart()!=R->GetInjectionTime()-10.0*R->GetInjectionSigma()) return 5;
  cout<<"\tGetInjectionTimeEnd()"<<endl;
  if(R->GetInjectionTimeEnd()!=R->GetInjectionTime()+10.0*R->GetInjectionSigma()) return 5;

  cout<<"\tR->GetInjectionIndexAfter() 1"<<endl;
  if(R->GetInjectionIndexAfter(1126240000.0)!=0) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 2"<<endl;
  if(R->GetInjectionIndexAfter(1126250000.0)!=0) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 3"<<endl;
  if(R->GetInjectionIndexAfter(1126250450.0)!=3) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 4"<<endl;
  if(R->GetInjectionIndexAfter(1126250599.0)!=6) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 5"<<endl;
  if(R->GetInjectionIndexAfter(1126250302.0)!=3) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 6"<<endl;
  if(R->GetInjectionIndexAfter(1126253700.0)!=15) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 7"<<endl;
  if(R->GetInjectionIndexAfter(1126253900.0)!=17) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 8"<<endl;
  if(R->GetInjectionIndexAfter(1126253901.0)!=R->GetN()) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 9"<<endl;
  if(R->GetInjectionIndexAfter(1126254900.0)!=R->GetN()) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 10"<<endl;
  if(R->GetInjectionIndexAfter(1126250500.0)!=4) return 5;
  cout<<"\tR->GetInjectionIndexAfter() 11"<<endl;
  if(R->GetInjectionIndexAfter(1126240000.0)!=0) return 5;

 
  delete R;

  //*********************************************************************
  // InjEct(const string aPattern, const string aPlotStyle="STANDARD", const unsigned int aVerbose=0)
  //*********************************************************************
  cout<<"\nInjEct(const string aPattern, const string aPlotStyle, const unsigned int aVerbose)"<<endl;
  Streams *St = new Streams("H1:TOTO");
  St->SetFrequencies(1024, 1024);
  InjEct *IE = new InjEct((string)filesystem::temp_directory_path()+"/gwl-test-inject-dir/gw-test-inject*.root");
  cout<<"\tGetN()"<<endl;
  if(IE->GetN()!=18) return 6;

  cout<<"\tInject()"<<endl;
  unsigned int datasize = 10*St->GetNativeFrequency();
  double *udata = new double [datasize];
  for(unsigned int i=0; i<datasize; i++) udata[i] = 0.0;
  IE->Inject(St, datasize, udata, 1126250895.0);//sine-Gauss

  for(unsigned int i=0; i<datasize; i++) udata[i] = 0.0;
  IE->Inject(St, datasize, udata, 1126250706);// user wave

  cout<<"\tDelete"<<endl;
  delete [] udata;
  delete St;
  delete IE;
  
  cout<<"\tRemove files"<<endl;
  // For some reason, it does not work on some file systems...
  if(RemoveFileOrDirectory(ec, (string)filesystem::temp_directory_path()+"/gwl-test-inject-dir")==0) cerr<<"clean-up failed"<<endl;
  
  return 0;
}

