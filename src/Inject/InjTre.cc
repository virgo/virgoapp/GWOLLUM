/**
 * @file 
 * @brief See InjTre.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjTre.h"

ClassImp(InjTre)


/////////////////////////////////////////////////////////////////////////
InjTre::InjTre(const injtype aInjType, const string aInjName, const unsigned int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // monitoring
  mon = new Monitor("injtre");
  mon->SetVerbosityLevel(aVerbose);
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjTre::"<<__func__<<endl;

  // injection
  wave_name = ReplaceAll(aInjName, " ", "_");
  wave_type = (UInt_t)aInjType;
  
  // default values
  inj_ra = 0.0;
  inj_dec = 0.0;
  inj_time = 0.0;
  inj_ecc = 0.0;
  inj_psi = 0.0;
  inj_amp = 0.0;
  inj_sigma = 0.0;
  inj_f0 = 0.0;

  // default waveforms
  wave_hplus = new TGraph(1);
  wave_hplus->SetName("gwl_inj_hplus");
  wave_hcross = new TGraph(1);
  wave_hcross->SetName("gwl_inj_hcross");
  for(unsigned int i=0; i<(unsigned int)wave_hplus->GetN(); i++){
    wave_hplus->SetPoint(i, (double)i/(double)wave_hplus->GetN(), 0.0);
    wave_hcross->SetPoint(i, (double)i/(double)wave_hplus->GetN(), 0.0);
  }
  
  // injection TTree
  InjTree = new TTree("Injections", "Injections");
  InjTree->SetDirectory(0);
  InjTree->Branch("ra",           &inj_ra,    "ra/D");
  InjTree->Branch("dec",          &inj_dec,   "dec/D");
  InjTree->Branch("time",         &inj_time,  "time/D");
  InjTree->Branch("time_ns",      &inj_time_ns,"time_ns/i");
  InjTree->Branch("eccentricity", &inj_ecc,   "eccentricity/D");
  InjTree->Branch("polarization", &inj_psi,   "polarization/D");
  InjTree->Branch("amplitude",    &inj_amp,   "amplitude/D");
  InjTree->Branch("sigma",        &inj_sigma, "sigma/D");
  InjTree->Branch("frequency",    &inj_f0,    "frequency/D");

  // waveform TTree
  WaveTree = new TTree("Waveform", "Waveform");
  WaveTree->SetDirectory(0);
  WaveTree->Branch("wavename", &wave_name);
  WaveTree->Branch("wavetype", &wave_type, "wavetype/i");
  WaveTree->Branch("hplus", "TGraph", &wave_hplus);
  WaveTree->Branch("hcross", "TGraph", &wave_hcross);

}

/////////////////////////////////////////////////////////////////////////
InjTre::~InjTre(void){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjTre::"<<__func__<<endl;
  delete InjTree;
  delete WaveTree;
  delete wave_hplus;
  delete wave_hcross;
  delete mon;
}

/////////////////////////////////////////////////////////////////////////
bool InjTre::SetWave(TGraph *aHplus, TGraph *aHcross){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjTre::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"InjTre::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // h+ must exist
  if(aHplus==NULL) return false;
  if(aHplus==0) return false;

  // h+ must have at least one point
  if(aHplus->GetN()==0) return false;

  // h+ and hx must have the same number of points
  if((aHcross!=NULL)&&(aHcross->GetN()!=aHplus->GetN())) return false;

  // hx must have at least one point
  if((aHcross!=NULL)&&aHcross->GetN()==0) return false;

  // h+ and hx must be aligned in time
  if((aHcross!=NULL)&&(aHcross->GetX()[0]!=aHplus->GetX()[0])) return false;

  // reset
  WaveTree->Reset();
  delete wave_hplus;
  delete wave_hcross;

  // save parameters
  wave_hplus = (TGraph*)aHplus->Clone("gwl_inj_hplus");
  if(aHcross!=NULL){
    wave_hcross = (TGraph*)aHcross->Clone("gwl_inj_hcross");
  }
  else{
    wave_hcross = new TGraph(wave_hplus->GetN());
    wave_hcross->SetName("gwl_inj_hcross");
    for(unsigned int i=0; i<(unsigned int)wave_hplus->GetN(); i++){
      wave_hcross->SetPoint(i, wave_hplus->GetX()[i], 0.0);
    }
  }

  return true;
}

/////////////////////////////////////////////////////////////////////////
unsigned int InjTre::Add(const double aRa, 
                         const double aDec, 
                         const double aTime, 
                         const double aAmplitude, 
                         const double aEccentricity, 
                         const double aPolarization, 
                         const double aSigma, 
                         const double aFrequency){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjTre::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"InjTre::"<<__func__<<": corrupted object"<<endl;
    return false;
  }
  inj_ra = aRa;
  inj_dec = aDec;
  inj_time = aTime;
  inj_time_ns = (unsigned int)((aTime-floor(aTime))*1000000000.0);
  inj_ecc = aEccentricity;
  inj_psi = aPolarization;
  inj_amp = aAmplitude;
  inj_sigma = aSigma;
  inj_f0 = aFrequency;
  InjTree->Fill();
  return (unsigned int)InjTree->GetEntries();
}

/////////////////////////////////////////////////////////////////////////
bool InjTre::Write(const string aRootFileName){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjTre::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"InjTre::"<<__func__<<": corrupted object"<<endl;
    return false;
  }
  TFile finj(aRootFileName.c_str(),"recreate");
  if(finj.IsZombie()) return false;
  finj.cd();
  InjTree->Write(InjTree->GetTitle());
  if(WaveTree->GetEntries()==0) WaveTree->Fill();// single entry
  WaveTree->Write(WaveTree->GetTitle());
  finj.Close();
  return true;
}







