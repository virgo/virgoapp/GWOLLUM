/**
 * @file 
 * @brief Read a set of injections.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjRea__
#define __InjRea__

#include <TChain.h>
#include <TTreeIndex.h>
#include <TH1.h>
#include <TGraph.h>
#include <TMath.h>
#include "InjTyp.h"
#include "Monitor.h"

using namespace std;

/**
 * @brief Read a set of injections.
 * @details Injection parameters, generated with the InjGen class, are loaded.
 * @author Florent Robinet
 */
class InjRea{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjRea class.
   * @details Injections in ROOT files designated by a file pattern are loaded.
   * Each injection is tagged to 'true' by default.
   * Injection files must be generated with the InjGen class.
   * @param aPattern Injection file pattern.
   * @param aVerbose Verbosity level.
   */
  InjRea(const string aPattern, const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the InjRea class.
   */
  virtual ~InjRea();
  /**
     @}
  */
  
  /**
   * @brief Returns the index of the current injection.
   */
  inline Long64_t GetInjectionIndex(void){ return inj_index; };

  /**
   * @brief Returns the index of the first injection after a given time.
   * @note The found injection is loaded. 
   * @returns If no injections are found after the reference time, the number of injections is returned and the last injection is loaded.
   * @param[in] aTime Reference time [s]. 
   */
  Long64_t GetInjectionIndexAfter(const double aTime);

  /**
   * @brief Loads a given injection.
   * @returns The function returns the number of bytes read from the input buffer.
   * If entry does not exist or if an I/O error occurs, the function returns 0.
   * @param[in] aInjIndex Injection index.
   */
  int LoadInjection(const Long64_t aInjIndex);

  /**
   * @brief Returns the SineGauss \f$h_{+rss}\f$ peak amplitude of current injection.
   * @details For details see K. Riles, LIGO-T040055-00
   */
  double GetSineGaussh0plus(void);

  /**
   * @brief Returns the SineGauss \f$h_{\times rss}\f$ peak amplitude of current injection.
   * @details For details see K. Riles, LIGO-T040055-00
   */
  double GetSineGaussh0cross(void);

  /**
   * @brief Returns the current injection name index.
   */
  inline unsigned int GetInjectionNameIndex(void){
    unsigned int n = 0;
    for(n=0; n<wave_names.size(); n++)
      if(!(*wave_name).compare(wave_names[n])) break;
    return n;
  };

  /**
   * @brief Returns the current injection name.
   */
  inline string GetInjectionName(void){ return *wave_name; };

  /**
   * @brief Returns the number of injection names.
   */
  inline unsigned int GetInjectionNameN(void){ return wave_names.size(); };

  /**
   * @brief Returns a given injection name in the list.
   * @param[in] aNameIndex Index in the list of names.
   * @pre The input index must be smaller than GetInjectionNameN().
   */
  inline string GetInjectionName(const unsigned int aNameIndex){ return wave_names[aNameIndex]; };

  /**
   * @brief Returns the current injection type.
   */
  inline injtype GetInjectionType(void){ return (injtype)wave_type; };

  /**
   * @brief Returns the current injection GPS time [s].
   */
  inline double GetInjectionTime(void){ return inj_time; };

  /**
   * @brief Returns the minimum injection GPS time.
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global minimum is returned.
   */
  inline double GetInjectionTimeMin(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_timemin[aNameIndex];
    return inj_timemin[wave_names.size()];
  };

  /**
   * @brief Returns the maximum injection GPS time.
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global maximum is returned.
   */
  inline double GetInjectionTimeMax(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_timemax[aNameIndex];
    return inj_timemax[wave_names.size()];
  };

  /**
   * @brief Returns the current injection right-ascension [rad].
   */
  inline double GetInjectionRa(void){ return inj_ra; };

  /**
   * @brief Returns the current injection declination [rad].
   */
  inline double GetInjectionDec(void){ return inj_dec; };

  /**
   * @brief Returns the current injection eccentricity.
   */
  inline double GetInjectionEccentricity(void){ return inj_ecc; };

  /**
   * @brief Returns the current injection polarization [rad].
   */
  inline double GetInjectionPolarization(void){ return inj_psi; };

  /**
   * @brief Returns the current injection amplitude.
   */
  inline double GetInjectionAmplitude(void){ return inj_amp; };

  /**
   * @brief Returns the minimum injection amplitude.
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global minimum is returned.
   */
  inline double GetInjectionAmplitudeMin(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_ampmin[aNameIndex];
    return inj_ampmin[wave_names.size()];
  };

  /**
   * @brief Returns the maximum injection amplitude.
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global maximum is returned.
   */
  inline double GetInjectionAmplitudeMax(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_ampmax[aNameIndex];
    return inj_ampmax[wave_names.size()];
  };

  /**
   * @brief Returns the current injection sigma [s].
   */
  inline double GetInjectionSigma(void){ return inj_sigma; };

  /**
   * @brief Returns the minimum injection sigma [s].
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global minimum is returned.
   */
  inline double GetInjectionSigmaMin(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_sigmamin[aNameIndex];
    return inj_sigmamin[wave_names.size()];
  };

  /**
   * @brief Returns the maximum injection sigma [s].
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global maximum is returned.
   */
  inline double GetInjectionSigmaMax(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_sigmamax[aNameIndex];
    return inj_sigmamax[wave_names.size()];
  };
  
  /**
   * @brief Returns the current injection frequency [Hz].
   */
  inline double GetInjectionFrequency(void){ return inj_f0; };

  /**
   * @brief Returns the minimum injection frequency [Hz].
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global minimum is returned.
   */
  inline double GetInjectionFrequencyMin(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_f0min[aNameIndex];
    return inj_f0min[wave_names.size()];
  };

  /**
   * @brief Returns the maximum injection frequency [Hz].
   * @param[in] aNameIndex Index in the list of names. If this index is out-of-range, the global maximum is returned.
   */
  inline double GetInjectionFrequencyMax(const unsigned int aNameIndex){
    if(aNameIndex<wave_names.size()) return inj_f0max[aNameIndex];
    return inj_f0max[wave_names.size()];
  };

  /**
   * @brief Returns the GPS starting time of the injection [s] at the center of the Earth.
   * @details It depends on the type of waveform:
   * - Sine-Gaussian waveform: this is 10 sigmas before the injection time.
   * - User-defined waveform: start time of the \f$h_{+}\f$ time series relatively to the injection time.
   * @returns 0 if the waveform type is unknown.
   */
  double GetInjectionTimeStart(void);

  /**
   * @brief Returns the GPS ending time of the injection [s] at the center of the Earth.
   * @details It depends on the type of waveform:
   * - Sine-Gaussian waveform: this is 10 sigmas after the injection time.
   * - User-defined waveform: end time of the \f$h_{+}\f$ time series relatively to the injection time.
   * @returns 0 if the waveform type is unknown.
   */
  double GetInjectionTimeEnd(void);

  /**
   * @brief Returns the current injection tag.
   */
  inline bool GetInjectionTag(void){ return inj_tag[InjTree->GetReadEntry()]; };

  /**
   * @brief Returns an injection tag.
   * @param[in] aInjIndex Injection index.
   */
  inline bool GetInjectionTag(const Long64_t aInjIndex){ return inj_tag[sort_index->GetIndex()[aInjIndex]]; };

  /**
   * @brief Sets a new tag value to the current injection.
   * @param[in] aTag New tag value.
   */
  inline void SetInjectionTag(const bool aTag){ inj_tag[InjTree->GetReadEntry()]=aTag; };

  /**
   * @brief Sets a new tag value to an injection.
   * @param[in] aInjIndex Injection index.
   * @param[in] aTag New tag value.
   */
  inline void SetInjectionTag(const Long64_t aInjIndex, const bool aTag){ inj_tag[sort_index->GetIndex()[aInjIndex]]=aTag; };

  /**
   * @brief Returns the number of injections of a given name.
   * @param[in] aNameIndex Index in the list of names.
   */
  inline Long64_t GetN(const unsigned int aNameIndex){
    return wave_name_n[aNameIndex];
  };
  
  /**
   * @brief Returns the number of injections.
   */
  inline Long64_t GetN(void){ return InjTree->GetEntries(); };
  
  /**
   * @brief Returns the 1D distribution of an injection parameter.
   * @note The returned histograms must be deleted by the user.
   * @warning Only injections tagged to true are considered.
   * @param[in] aParamName Parameter name. Currently supported: "amplitude" and "f0".
   * @param[in] aNbins Number of bins (must be >0).
   * @param[in] aBinType Binning type. Currently supported: "UNIFORM" and "LOG".
   */
  TH1D* GetInjectionParamDist(const string aParamName, const unsigned int aNbins=1, const string aBinType="UNIFORM");

  /**
   * @brief Returns the input file pattern provided in the constructor.
   */
  inline string GetInputFilePattern(void){ return pattern; };

protected:
  
  Monitor *mon;         ///< Class monitor.
  TGraph *wave_hplus;   ///< Waveform \f$h_{+}(t)\f$.
  TGraph *wave_hcross;  ///< Waveform \f$h_{\times}(t)\f$.
  TChain *InjTree;      ///< Injection tree.
  
private:
  
  string pattern;       ///< Input file pattern.

  // INJECTION TREE
  Long64_t inj_index;   ///< Current injection index.
  TTreeIndex *sort_index;///< TTree index.
  double inj_ra;        ///< Injection right ascension.
  double inj_dec;       ///< Injection declination.
  double inj_psi;       ///< Injection polarization angle.
  double inj_time;      ///< Injection time.
  double inj_amp;       ///< Injection amplitude.
  double inj_f0;        ///< Injection frequency.
  double inj_sigma;     ///< Injection sigma.
  double inj_ecc;       ///< Injection eccentricity.

  // MIN/MAX
  double *inj_timemin;   ///< Time min.
  double *inj_timemax;   ///< Time max.
  double *inj_sigmamin;  ///< Sigma min.
  double *inj_sigmamax;  ///< Sigma max.
  double *inj_ampmin;    ///< Amplitude min.
  double *inj_ampmax;    ///< Amplitude max.
  double *inj_f0min;     ///< Frequency min.
  double *inj_f0max;     ///< Frequency max.
 
  bool *inj_tag;        ///< Injection tags.
 
  // WAVEFORM TREE
  TChain *WaveTree;     ///< Waveform tree.
  UInt_t wave_type;     ///< Waveform type: see InjTyp.h.
  string *wave_name;    ///< Waveform name
  vector<string> wave_names;///< List of waveform names.
  Long64_t *wave_name_n;/// List of number of injections /name.

  ClassDef(InjRea,0)
};

#endif


