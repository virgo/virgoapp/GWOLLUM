/**
 * @file 
 * @brief Inject simulated signals in a data stream.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __InjEct__
#define __InjEct__

#include "Streams.h"
#include "InjRea.h"
#include "GwollumPlot.h"

using namespace std;

/**
 * @brief Inject simulated signals in a data stream.
 * @details This class injects signal waveforms in a data vector using injection files produced with InjGen.
 *
 * The injection waveform is generated at the center of the Earth using the parameters from the injection files. Both polarizations \f$h_{+}\f$ and \f$h_{\times}\f$ are calculated. The waveform timing is adjusted to a given detector (Streams): the waveforms are calculated with the detector local time, accounting for the light travel time and the sky position of the source. Then we compute the detector strain:
 *\f[
 h_{det}(t) = T(t)\times \left(F_{+}(t)\times h_{+}(t) + F_{\times}(t)\times h_{\times}(t)\right).
 \f]
 * The detector antenna factors \f$F_{+}\f$ and \f$F_{\times}\f$ are calculated for the source sky position and polarization angle and at any given time. Moreover, a Tukey window \f$T(t)\f$ is applied to have a nice transition to 0 at both ends of the waveform.
 *
 * @sa ConstructSineGaussWaveform() and ConstructUserWaveform()
 *
 * @author Florent Robinet
 */
class InjEct: public InjRea, public GwollumPlot {
  
 public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the InjEct class.
   * @details Injections listed in the ROOT files designated by a file pattern are loaded with InjRea.
   * The Tukey window to frame the injection signal is set to 10%: see SetTukeyFraction().
   * @param[in] aPattern Injection file pattern.
   * @param[in] aPlotStyle GwollumPlot style.
   * @param[in] aVerbose Verbosity level.
   */
  InjEct(const string aPattern,
         const string aPlotStyle="STANDARD",
         const unsigned int aVerbose=0);

  /**
   * @brief Destructor of the InjEct class.
   */
  virtual ~InjEct(void);
  /**
     @}
  */

  /**
   * @brief Injects in a data vector.
   * @details All the signals overlapping the input data vector are injected.
   * @note The native sampling frequency of the Streams object is considered to be the data sampling frequency.
   * @param[in] aDetectorStream Detector stream. It is assumed to be a valid Streams object.
   * @param[in] aDataSize Input vector size.
   * @param[in,out] aData Pointer to input data vector (time-domain).
   * @param[in] aTimeStart GPS time of first data sample.
   * @todo Optimize the loop over injections.
   */
  void Inject(Streams *aDetectorStream,
              const unsigned int aDataSize, double *aData, const double aTimeStart);

  /**
   * @brief Plots the local waveform of a given injection.
   * @param[in] aDetectorStream Detector stream. It is assumed to be a valid Streams object.
   * @param[in] aInjectionIndex Injection index.
   */
  void Plot(Streams *aDetectorStream, const Long64_t aInjectionIndex);

  /**
   * @brief Sets the transition of the Tukey window.
   * @details This number (between 0 and 1) sets the fraction of the waveform used to transition the time series to 0 at both ends. For exemple, using a fraction of 0.1 means that it takes 5% of the waveform duration to transition from 0 to 1 and 5% again to transition from 1 to 0.
   * @param[in] aFraction Fraction of the waveform duration: it must be a number between 0 and 1.
   */
  inline void SetTukeyFraction(const double aFraction){
    if(aFraction<0.0) return;
    if(aFraction>1.0) return;
    tukey_frac = aFraction;
  };

  /**
   * @brief Sets the periodicty to update the antenna factor.
   * @details When calculating the injection waveform (ConstructUserWaveform()), the antenna factor is updated periodically. Set the periodidicity with this function.
   * @param[in] aPeriod Time period to update the antenna factors [s].
   */
  inline void SetAntennaPeriodicity(const double aPeriod){
    if(aPeriod>0.0) antenna_period = aPeriod;
  };

 private:

  double tukey_frac;     ///< Fraction of the waveform use to transition to 0 (between 0 and 1).
  TGraph *hplus;         ///< Injection waveform \f$h_{+}\f$ (local time).
  TGraph *hcross;        ///< Injection waveform \f$h_{\times}\f$ (local time).
  TGraph *hdet;          ///< Injection waveform projected on the detector \f$h_{det}\f$.
  double antenna_period; ///< Optimization: periodicity to update the antenna factors.
    
  /**
   * @brief Constructs the local time-domain waveforms \f$h_{+}\f$, \f$h_{\times}\f$, and \f$h_{det}\f$ for the current "user-defined" injection.
   * @details The \f$h_{+}(t)\f$ and \f$h_{\times}(t)\f$ waveforms are first calculated at the center of the Earth. Then, they are time-shifted to match the detector local time. Moreover, the wavform amplitude is rescaled by the injection amplitude.
   *
   * Then we compute the detector strain:
   *\f[
   h_{det}(t) = T(t)\times \left(F_{+}(t)\times h_{+}(t) + F_{\times}(t)\times h_{\times}(t)\right).
   \f]
   * The detector antenna factors \f$F_{+}\f$ and \f$F_{\times}\f$ are calculated for the source sky position and polarization angle and at any given time. They are calculated periodically as defined with SetAntennaPeriodicity(). For each waveform sample, the antenna factors are interpolated between the two time values.
   *
   * Moreover, a Tukey window \f$T(t)\f$ is applied to have nice transitions to 0.
   * @param[in] aDetectorStream Detector stream. It is assumed to be a valid Streams object.
   */
  void ConstructUserWaveform(Streams *aDetectorStream);

  /**
   * @brief Constructs the local time-domain waveforms \f$h_{+}\f$, \f$h_{\times}\f$, and \f$h_{det}\f$ for the current "Sine-Gaussian" injection.
   * @details The \f$h_{+}(t)\f$ and \f$h_{\times}(t)\f$ waveforms are first calculated at the center of the Earth. Then, they are time-shifted to match the detector local time. Moreover, the wavform amplitude is rescaled by the injection amplitude.
   *
   * Then we compute the detector strain:
   *\f[
   h_{det}(t) = T(t)\times \left(F_{+}(t)\times h_{+}(t) + F_{\times}(t)\times h_{\times}(t)\right).
   \f]
   * The detector antenna factors \f$F_{+}\f$ and \f$F_{\times}\f$ are calculated for the source sky position and polarization angle and at any given time. They are calculated periodically as defined with SetAntennaPeriodicity(). For each waveform sample, the antenna factors are interpolated between the two time values.
   *
   * Moreover, a Tukey window \f$T(t)\f$ is applied to have nice transitions to 0.
   * @todo Document the waveform calculation.
   * @param[in] aDetectorStream Detector stream. It is assumed to be a valid Streams object.
   */
  void ConstructSineGaussWaveform(Streams *aDetectorStream);

  ClassDef(InjEct,0)
};

#endif


