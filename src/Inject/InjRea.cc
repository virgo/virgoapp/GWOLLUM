/**
 * @file 
 * @brief See InjRea.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "InjRea.h"

ClassImp(InjRea)

/////////////////////////////////////////////////////////////////////////
InjRea::InjRea(const string aPattern, const unsigned int aVerbose){ 
/////////////////////////////////////////////////////////////////////////

  // monitoring
  mon = new Monitor("injrea");
  mon->SetVerbosityLevel(aVerbose);
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjRea::"<<__func__<<endl;

  // parameters
  pattern = aPattern;
  
  // get list of patterns and load it
  vector <string> patlist = SplitString(pattern,' ');

  // waveform tree
  wave_name = 0;
  wave_hplus = 0;
  wave_hcross = 0;
  WaveTree = new TChain("Waveform");
  for(unsigned int p=0; p<patlist.size(); p++) WaveTree->Add(patlist[p].c_str());
  WaveTree->SetDirectory(0);

  if(WaveTree->SetBranchAddress("wavename", &wave_name)!=0) wave_name = new string("gwl_inj");;
  if(WaveTree->SetBranchAddress("wavetype", &wave_type)!=0) wave_type = (unsigned int)injtype_n;
  if(WaveTree->SetBranchAddress("hplus", &wave_hplus)!=0){
    wave_hplus = new TGraph(1);
    for(unsigned int i=0; i<(unsigned int)wave_hplus->GetN(); i++)
      wave_hplus->SetPoint(i, (double)i/(double)wave_hplus->GetN(), 0.0);
  }
  if(WaveTree->SetBranchAddress("hcross", &wave_hcross)!=0){
    wave_hcross = new TGraph(1);
    for(unsigned int i=0; i<(unsigned int)wave_hcross->GetN(); i++)
      wave_hcross->SetPoint(i, (double)i/(double)wave_hcross->GetN(), 0.0);
  }

  // get list of waveform names
  wave_names.clear();
  for(Long64_t w=0; w<WaveTree->GetEntries(); w++){
    WaveTree->GetEntry(w);

    // test existing name in the list
    if(GetInjectionNameIndex()==wave_names.size()) wave_names.push_back(*wave_name);
  }

  // allocate memory for min/max for each injection name
  // +1 is for all injections
  inj_timemin = new double [wave_names.size()+1];
  inj_timemax = new double [wave_names.size()+1];
  inj_sigmamin = new double [wave_names.size()+1];
  inj_sigmamax = new double [wave_names.size()+1];
  inj_ampmin = new double [wave_names.size()+1];
  inj_ampmax = new double [wave_names.size()+1];
  inj_f0min = new double [wave_names.size()+1];
  inj_f0max = new double [wave_names.size()+1];
  wave_name_n = new Long64_t [wave_names.size()];
  
  // init
  for(unsigned int n=0; n<wave_names.size(); n++) wave_name_n[n] = 0;
  for(unsigned int n=0; n<=wave_names.size(); n++){
    inj_timemin[n] = -1.0;
    inj_timemax[n] = -1.0;
    inj_ampmin[n] = -1.0;
    inj_ampmax[n] = -1.0;
    inj_sigmamin[n] = -1.0;
    inj_sigmamax[n] = -1.0;
    inj_f0min[n] = -1.0;
    inj_f0max[n] = -1.0;
  }
  
  // injection tree
  InjTree = new TChain("Injections");
  for(unsigned int p=0; p<patlist.size(); p++) InjTree->Add(patlist[p].c_str());
  patlist.clear();
  InjTree->SetDirectory(0);

  // injection tags
  inj_tag = new bool [InjTree->GetEntries()];

  // attach branches
  unsigned int inj_time_ns = 0.0;
  if(InjTree->SetBranchAddress("ra", &inj_ra)<0) inj_ra = -1.0;
  if(InjTree->SetBranchAddress("dec", &inj_dec)<0) inj_dec = -1.0;
  if(InjTree->SetBranchAddress("time", &inj_time)<0) inj_time = -1.0;
  if(InjTree->SetBranchAddress("time_ns", &inj_time_ns)<0) inj_time_ns = 0;
  if(InjTree->SetBranchAddress("eccentricity", &inj_ecc)<0) inj_ecc = -1.0;
  if(InjTree->SetBranchAddress("polarization", &inj_psi)<0) inj_psi = -1.0;
  if(InjTree->SetBranchAddress("amplitude", &inj_amp)<0) inj_amp = -1.0;
  if(InjTree->SetBranchAddress("sigma", &inj_sigma)<0) inj_sigma = -1.0;
  if(InjTree->SetBranchAddress("frequency", &inj_f0)<0) inj_f0 = -1.0;

  // TTree index
  if(InjTree->GetEntries()>0) sort_index = new TTreeIndex(InjTree, "time","time_ns");
  else sort_index = new TTreeIndex();
  inj_index = -1;
  InjTree->SetBranchStatus("time_ns", false);

  // separate injections by names
  unsigned int nsel = 0;
  for(Long64_t i=0; i<InjTree->GetEntries(); i++){

    // init tag to true for all injections
    inj_tag[i] = true;

    // load injection
    if(InjTree->GetEntry(i)<=0){
      inj_tag[i] = false;
      continue;
    }
    if(WaveTree->GetEntry(InjTree->GetTreeNumber())<=0){
      inj_tag[i] = false;
      continue;
    }
    
    // name match (there must be one!)
    nsel = 0;
    while((*wave_name).compare(wave_names[nsel])) nsel++;

    // count injection with this name
    wave_name_n[nsel]++;
    
    // time min/max
    if(inj_time>inj_timemax[nsel]) inj_timemax[nsel] = inj_time;
    if(inj_timemin[nsel]<0.0) inj_timemin[nsel] = inj_time;
    if(inj_time<inj_timemin[nsel]) inj_timemin[nsel] = inj_time;
    
    // amplitude min/max
    if(inj_amp>inj_ampmax[nsel]) inj_ampmax[nsel] = inj_amp;
    if(inj_ampmin[nsel]<0.0) inj_ampmin[nsel] = inj_amp;
    if(inj_amp<inj_ampmin[nsel]) inj_ampmin[nsel] = inj_amp;
    
    // sigma min/max
    if(inj_sigma>inj_sigmamax[nsel]) inj_sigmamax[nsel] = inj_sigma;
    if(inj_sigmamin[nsel]<0.0) inj_sigmamin[nsel] = inj_sigma;
    if(inj_sigma<inj_sigmamin[nsel]) inj_sigmamin[nsel] = inj_sigma;
    
    // f0 min/max
    if(inj_f0>inj_f0max[nsel]) inj_f0max[nsel] = inj_f0;
    if(inj_f0min[nsel]<0.0) inj_f0min[nsel] = inj_f0;
    if(inj_f0<inj_f0min[nsel]) inj_f0min[nsel] = inj_f0;
  }

  // absolute min/max
  for(unsigned int n=0; n<wave_names.size(); n++){
    if(inj_timemax[n]>inj_timemax[wave_names.size()]) inj_timemax[wave_names.size()] = inj_timemax[n];
    if(inj_timemin[wave_names.size()]<0.0) inj_timemin[wave_names.size()] = inj_timemin[n];
    if(inj_timemin[n]<inj_timemin[wave_names.size()]) inj_timemin[wave_names.size()] = inj_timemin[n];

    if(inj_ampmax[n]>inj_ampmax[wave_names.size()]) inj_ampmax[wave_names.size()] = inj_ampmax[n];
    if(inj_ampmin[wave_names.size()]<0.0) inj_ampmin[wave_names.size()] = inj_ampmin[n];
    if(inj_ampmin[n]<inj_ampmin[wave_names.size()]) inj_ampmin[wave_names.size()] = inj_ampmin[n];

    if(inj_sigmamax[n]>inj_sigmamax[wave_names.size()]) inj_sigmamax[wave_names.size()] = inj_sigmamax[n];
    if(inj_sigmamin[wave_names.size()]<0.0) inj_sigmamin[wave_names.size()] = inj_sigmamin[n];
    if(inj_sigmamin[n]<inj_sigmamin[wave_names.size()]) inj_sigmamin[wave_names.size()] = inj_sigmamin[n];

    if(inj_f0max[n]>inj_f0max[wave_names.size()]) inj_f0max[wave_names.size()] = inj_f0max[n];
    if(inj_f0min[wave_names.size()]<0.0) inj_f0min[wave_names.size()] = inj_f0min[n];
    if(inj_f0min[n]<inj_f0min[wave_names.size()]) inj_f0min[wave_names.size()] = inj_f0min[n];
  }

  // set to first injection
  LoadInjection(0);

  mon->SetMessageType(monitor_info_4);
  *mon<<"InjRea::"<<__func__<<": "<<InjTree->GetEntries()<<" injections are loaded"<<endl;
  for(unsigned int n=0; n<wave_names.size(); n++){
    mon->SetMessageType(monitor_info_4);
    *mon<<"InjRea::"<<__func__<<": time:      "<<inj_timemin[n]<<" --> "<<inj_timemax[n]<<endl;
    mon->SetMessageType(monitor_info_4);
    *mon<<"InjRea::"<<__func__<<": frequency: "<<inj_f0min[n]<<" --> "<<inj_f0max[n]<<" Hz"<<endl;
    mon->SetMessageType(monitor_info_4);
    *mon<<"InjRea::"<<__func__<<": sigma:     "<<inj_sigmamin[n]<<" --> "<<inj_sigmamax[n]<<" s"<<endl;
    mon->SetMessageType(monitor_info_4);
    *mon<<"InjRea::"<<__func__<<": amplitude: "<<scientific<<inj_ampmin[n]<<" --> "<<inj_ampmax[n]<<endl;
  }
}

/////////////////////////////////////////////////////////////////////////
InjRea::~InjRea(void){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjRea::"<<__func__<<endl;
  delete [] inj_timemin;
  delete [] inj_timemax;
  delete [] inj_sigmamin;
  delete [] inj_sigmamax;
  delete [] inj_ampmin;
  delete [] inj_ampmax;
  delete [] inj_f0min;
  delete [] inj_f0max;
  delete [] wave_name_n;
  delete wave_name;
  delete InjTree;
  //delete sort_index;
  delete [] inj_tag;
  delete wave_hplus;
  delete wave_hcross;
  delete mon;
}

/////////////////////////////////////////////////////////////////////////
Long64_t InjRea::GetInjectionIndexAfter(const double aTime){
/////////////////////////////////////////////////////////////////////////

  // current index
  Long64_t i = inj_index;
  
  // search backward
  while((inj_time>=aTime)&&(i>0)){
    i--;
    InjTree->GetEntry(sort_index->GetIndex()[i]);
  }

  // search forward
  while((inj_time<aTime)&&(i<InjTree->GetEntries()-1)){
    i++;
    InjTree->GetEntry(sort_index->GetIndex()[i]);
  }

  // there is no injections after that time
  if(inj_time<aTime){
    LoadInjection(InjTree->GetEntries()-1);// load last injection
    return InjTree->GetEntries();
  }

  // load injection and return
  LoadInjection(i);
  return i;
}

/////////////////////////////////////////////////////////////////////////
int InjRea::LoadInjection(const Long64_t aInjIndex){
/////////////////////////////////////////////////////////////////////////
  if((aInjIndex<0)||(aInjIndex>=InjTree->GetEntries())) return 0;

  // load injection
  int nbytes_inj = InjTree->GetEntry(sort_index->GetIndex()[aInjIndex]);

  // current injection index
  if(nbytes_inj>0) inj_index = aInjIndex;
  else return 0;

  // load waveform
  int nbytes_wav = WaveTree->GetEntry(InjTree->GetTreeNumber());
  if(nbytes_inj==0) return 0;
  return nbytes_inj + nbytes_wav;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetSineGaussh0plus(void){
/////////////////////////////////////////////////////////////////////////
  // !!! here, amplitude = hrss !!!
  double q = 2 * TMath::Pi() * inj_sigma * inj_f0;
  
  // semimajor and semiminor axes of waveform ellipsoid
  const double a = 1.0 / sqrt(2.0 - inj_ecc * inj_ecc);
  const double b = a * sqrt(1.0 - inj_ecc * inj_ecc);

  // rss of plus polarization
  const double hplusrss  = inj_amp * (a * cos(inj_psi) - b * sin(inj_psi));
  
  // rss of unit amplitude cosine-gaussian waveforms.  see
  // K. Riles, LIGO-T040055-00.pdf
  const double cgrss = sqrt((q / (4.0 * inj_f0 * sqrt(TMath::Pi()))) * (1.0 + exp(-q * q)));
  
  // "peak" amplitude
  return hplusrss / cgrss;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetSineGaussh0cross(void){
/////////////////////////////////////////////////////////////////////////
  // !!! here, amplitude = hrss !!!
  double q = 2 * TMath::Pi() * inj_sigma * inj_f0;

  // semimajor and semiminor axes of waveform ellipsoid
  const double a = 1.0 / sqrt(2.0 - inj_ecc * inj_ecc);
  const double b = a * sqrt(1.0 - inj_ecc * inj_ecc);
  
  // rss of cross polarizations
  const double hcrossrss = inj_amp * (b * cos(inj_psi) + a * sin(inj_psi));
  
  // rss of unit amplitude sine-gaussian waveforms.  see
  // K. Riles, LIGO-T040055-00.pdf
  const double sgrss = sqrt((q / (4.0 * inj_f0 * sqrt(TMath::Pi()))) * (1.0 - exp(-q * q)));

  // "peak" amplitude
  return hcrossrss / sgrss;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetInjectionTimeStart(void){
/////////////////////////////////////////////////////////////////////////

  // user waveform --> start of h+
  if(GetInjectionType()==injtype_user){
    return wave_hplus->GetX()[0]+inj_time;
  }
  // Sine-Gaussian waveform
  else if(GetInjectionType()==injtype_sinegauss){
    // FIXME: 10 sigma???
    return inj_time-10.0*inj_sigma;
  }
  else{};

  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
double InjRea::GetInjectionTimeEnd(void){
/////////////////////////////////////////////////////////////////////////
  
  // user waveform --> end of h+
  if(GetInjectionType()==injtype_user){
    return wave_hplus->GetX()[wave_hplus->GetN()-1]+inj_time;
  }
  // Sine-Gaussian waveform
  else if(GetInjectionType()==injtype_sinegauss){
    // FIXME: 10 sigma???
    return inj_time+10.0*inj_sigma;
  }
  else{};

  return 0.0;
}

/////////////////////////////////////////////////////////////////////////
TH1D* InjRea::GetInjectionParamDist(const string aParamName,
                                    const unsigned int aNbins, const string aBinType){
/////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"InjRea::"<<__func__<<endl;
  if(mon->GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"InjRea::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  if(!InjTree->GetEntries()) return NULL;
  TH1D *h = NULL;

  // linear bins
  if(!aBinType.compare("UNIFORM")){

    // amplitude
    if(!aParamName.compare("amplitude"))
      h = new TH1D("h_inj_amp","Injection amplitude",
                   aNbins, inj_ampmin[wave_names.size()], inj_ampmax[wave_names.size()]*1.01);

    // f0
    if(!aParamName.compare("f0"))
      h = new TH1D("h_inj_f0","Injection frequency",
                   aNbins, inj_f0min[wave_names.size()], inj_f0max[wave_names.size()]*1.01);
  }

  // log bins
  if(!aBinType.compare("LOG")){
    double *bins = new double [aNbins+1];

    // amplitude
    if(!aParamName.compare("amplitude")){
      for(unsigned int b=0; b<aNbins+1; b++) 
	bins[b] = inj_ampmin[wave_names.size()]*pow(10.0,b*log10(2*inj_ampmax[wave_names.size()]/inj_ampmin[wave_names.size()])/(double)aNbins);
      h = new TH1D("h_inj_amp","Injection amplitude",aNbins,bins);
    }

    // f0
    if(!aParamName.compare("f0")){
      for(unsigned int b=0; b<aNbins+1; b++) 
	bins[b] = inj_f0min[wave_names.size()]*pow(10.0,b*log10(2*inj_f0max[wave_names.size()]/inj_f0min[wave_names.size()])/(double)aNbins);
      h = new TH1D("h_inj_f0","Injection frequency",aNbins,bins);
    }

    delete [] bins;
  }

  // fill histo
  if(h!=NULL){
    h->GetYaxis()->SetTitle("Number of injections / bin");

    // amplitude
    if(!aParamName.compare("amplitude")){
      h->GetXaxis()->SetTitle("Injection amplitude");
      for(Long64_t i=0; i<GetN(); i++){
	LoadInjection(i);
	if(GetInjectionTag()) h->Fill(GetInjectionAmplitude());
      }
    }

    // f0
    if(!aParamName.compare("f0")){
      h->GetXaxis()->SetTitle("Injection frequency [Hz]");
      for(Long64_t i=0; i<GetN(); i++){
	LoadInjection(i);
	if(GetInjectionTag()) h->Fill(GetInjectionFrequency());
      }
    }
  } 
  
  return h;
}

